<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_sitio extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('M_usuario');
		$this->load->model('M_cuestionario');
		session_start();
	}

	public function index()
	{
		if(isset($_SESSION['usuario']))
		{ 
		     $t_usuario = $_SESSION['usuario']['tipo'];
		     
		     if($t_usuario != 1){
		        $this->cuestionario(); //$this->load->view('index');
		     }else{
		        $this->inicial();
		     }
		}
		else $this->load->view('login');		
	}

	public function iniciar()
	{
		//	Datos del captcha
    	$secret = '6Ld5O-MUAAAAAJHww-U391PZUrxLo5DU8vxyM0rN';
    	$response = $_POST["g-recaptcha-response"];
    	$remoteip =  $_SERVER['REMOTE_ADDR'];

    	$url = 'https://www.google.com/recaptcha/api/siteverify';

    	$captcha = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response&remoteip=$remoteip");
		$json = json_decode($captcha);

		if($json->success)
    	{
    		$usuario = $this->input->post('usuario');
			$contra = $this->input->post('contrasenia');

			$model = new M_usuario();
			$resp = $model->inicia_sesion($usuario);
			if($resp!=false)
			{
			    if($resp[0]->iActivo == 1) {
    				if(sha1($contra) == $resp[0]->vContrasenia)
    				{
    					echo 'success';
    					$_SESSION['usuario']['usid'] = $resp[0]->iIdUsuario;
    					$_SESSION['usuario']['nom'] = $usuario;
    					$_SESSION['usuario']['tipo'] = $resp[0]->iTipoUsuario;
    					$_SESSION['usuario']['tipo_proced'] = $resp[0]->iIdCentro;
    					$_SESSION['usuario']['otro_centro'] = $resp[0]->vOtroCentro;
    
    
    				}
    				else echo 'error1';
			    }
				else echo 'error4';
			}
			else echo 'error2';
		} 
		else echo 'error3';
		
	}

	public function salir()
	{		
		unset($_SESSION);
        session_destroy();
        if(!isset($_SESSION['usuario'])) echo 1;
	}

	public function usuarios()
	{
		if(!isset($_SESSION['usuario'])) header('Location: '.base_url());
		else
		{
			if($_SESSION['usuario']['tipo'] == 4) {
				$c = $_SESSION['usuario']['tipo_proced'];
				$oc = $_SESSION['usuario']['otro_centro'];
			}
			else {
				$c = 0;
				$oc = '';
			}


			$model = new M_usuario();
			$datos['usuarios'] = $model->carga_usuarios($c, $oc);
			$this->load->view('usuarios',$datos);
		}
	}

	public function preguntas()
	{
		$model = new M_cuestionario();		

		$datos['preg']  = $model->carga_preguntas(1);

		if(!isset($_SESSION['usuario'])) header('Location: '.base_url());
		else $this->load->view('preguntas',$datos);
	}

	public function respuestas()
	{
		$model = new M_cuestionario();
		$datos['opciones'] = $model->carga_resp(-1);

		if(!isset($_SESSION['usuario'])) header('Location: '.base_url());
		else $this->load->view('respuestas',$datos);
	}

	public function agregar_pregunta()
	{
		if(!isset($_SESSION['usuario'])) header('Location: '.base_url());
		else $this->load->view('form_preguntas');
	}

	public function modificar_pregunta()
	{
		if(!isset($_SESSION['usuario'])) header('Location: '.base_url());
		else {
			$model = new M_cuestionario();
			$pregid = $this->input->get('pregid', TRUE);
			$preguntas = $model->carga_preguntas(1,$pregid);
			$tipo = $preguntas[0]->iTipoPregunta;
			$datos['datos_preg'] = $preguntas;
			$datos['tot_resp'] = $model->carga_resp($tipo);
			$datos['resp_preg'] = $model->carga_respuestas($pregid);
			$this->load->view('form_preguntas',$datos);
		}
	}

	public function agregar_usuario()
	{
		$model = new M_usuario();
		$modelc = new M_cuestionario();
		$datos['centros'] = $model->carga_centros();	
		$datos['zonas'] = $modelc->carga_zonas();	
		if(!isset($_SESSION['usuario'])) header('Location: '.base_url());
		else $this->load->view('form_usuarios', $datos);
	}

	public function modificar_usuario()
	{
		if(!isset($_SESSION['usuario'])) header('Location: '.base_url());
		else
		{							
			$usid = $this->input->get('usid');
			$model = new M_usuario();
			$modelc = new M_cuestionario();
			$datos['centros'] = $model->carga_centros();
			$datos['zonas'] = $modelc->carga_zonas();	
			$datos['resp'] = $model->datos_usuario($usid);
			$this->load->view('form_usuarios',$datos);	
		}
	}
	
	public function cuestionario_public()
	{
		$model = new M_cuestionario();
		$hoy = date("Y-m-d");
		$datos['cuest'] = $model->carga_cuestionarios(0,0,0,0,0,$hoy);

		$this->load->view('tabla_cuest_public',$datos);
	}
	
	public function inicial()
	{
		$model = new M_cuestionario();
		$this->load->view('inicial');
	}


	public function cuestionario()
	{
		$usid = $_SESSION['usuario']['usid'];
		$tipo_us = $_SESSION['usuario']['tipo'];
		
		if($tipo_us==1) $tipo_proced = 0;
		else $tipo_proced = $_SESSION['usuario']['tipo_proced'];

		if(!isset($_SESSION['usuario'])) header('Location: '.base_url());
		else
		{	
			$model = new M_cuestionario();
			$modelc = new M_usuario();
			
			$hoy = date("Y-m-d");
			$usuario = $modelc->datos_usuario($usid);
			
			$esNacional = $usuario[0]->iIdCentro == 0 ? 1 : 0;
			$esEmpresarial = $usuario[0]->iIdCentro == 0 ? 0 : 1;
			
			$cuestionarios = $model->carga_cuestionarios($tipo_proced, $usid, $esNacional, $esEmpresarial, $usuario[0]->iIdCentro, $hoy);
		    foreach($cuestionarios as $c){
		        $data = $model->resp_us($usid, $c->iIdCuestionario);
		        
		        $c->dFechaEnvio = count($data) > 0 ? $data[0]->dFechaEnvio : "";
		    }
		    
		    $datos["cuest"] = $cuestionarios;

			/*$preguntas = $model->carga_preguntas();
			if($preguntas!=false && count($preguntas) > 0)
			{
				$datos['preguntas'] = $preguntas;
				foreach ($preguntas as $vpreg) {
					$pregid = $vpreg->iIdPregunta;
					$resp_cal = $model->obtener_resp($pregid, $usid, 1);
					if($resp_cal!=false && count($resp_cal) > 0)
					{
						$vpreg->iCalificacion = $resp_cal[0]->iCalificacion;
						$vpreg->vArchivo = $resp_cal[0]->vArchivo;						
					}
					else 
					{
						$vpreg->iCalificacion = 0;
						$vpreg->vArchivo = '';
					}
				}

			}
			else $datos['preguntas'] = false;




			$resp = $model->resp_usuario($usid);
			if($resp!=false && count($resp) > 0) $datos['respuestas'] = $resp;
			else $datos['respuestas'] = false;

			$this->load->view('cuestionario',$datos);		
			*/
			$this->load->view('tabla_cuest',$datos);
		}

	}

	public function resp_cuestionario()
	{
		$cuestid = $this->input->get('cuestid', TRUE);
		$datos['cuestid'] = $cuestid;
		$usid = 0;
		
		if(isset($_SESSION['usuario'])) { $usid = $_SESSION['usuario']['usid']; }

		$hoy = date("Y-m-d");		

		$where = array('iIdCuestionario' => $cuestid, 'dVigencia >=' => $hoy);
		$model = new M_cuestionario();
		
		$vigencia = $model->cuestionario_vigencia($where);
		$datos['vigencia'] = $vigencia;
		$datos['aviso'] = $vigencia[0]->vAvisoPriv;
		$datos['infoGeneral'] = $vigencia[0]->vInfoGeneral;

		if($cuestid > 0 && !empty($vigencia))
		{
			//if(!isset($_SESSION['usuario'])) header('Location: '.base_url());
			//else
			//{	
				$model = new M_cuestionario();

				$preguntas = $model->carga_preguntas(0,0,$cuestid);
				if($preguntas!=false && count($preguntas) > 0)
				{
					$hijos = array();
					foreach ($preguntas as $vpreg) {
						$pregid = $vpreg->iIdPregunta;

						$preguntaHijo = $model->carga_pregunta_hijo($vpreg->iIdOpcion);
						if(count($preguntaHijo) > 0){
						    //log_message("error", "PH: ".print_r($preguntaHijo));
						    $hijos = array_merge($hijos, $preguntaHijo);
						}
						
						if($usid > 0)
						{
    						$resp_cal = $model->obtener_resp($pregid, $usid, 1);
    						if($resp_cal!=false && count($resp_cal) > 0)
    						{
    							$vpreg->vArchivo = $resp_cal[0]->vArchivo;
    
    
    							$cal = $model->obtener_calif($pregid,$usid);
    							if($cal!=false && count($cal) > 0) $vpreg->iCalificacion = $cal[0]->vCalificacion;
    							else $vpreg->iCalificacion = 0;
    						}
    						else 
    						{				
    							$vpreg->iCalificacion = -1;
    							$vpreg->vArchivo = '';
    						}
						}
					}
					
					$datos['preguntas'] = $preguntas;
					$datos['hijos'] = $hijos;

					//print_r($hijos);

				}
				else $datos['preguntas'] = false;

                if($usid > 0)
				{
				    $resp = $model->resp_usuario($usid, $cuestid);
				    if($resp!=false && count($resp) > 0) $datos['respuestas'] = $resp;
				    else $datos['respuestas'] = false;
			    }
			    else{
			        $datos['respuestas'] = false;
			    }

				$this->load->view('cuestionario',$datos);
			//}			
		}
		else header('Location: '.base_url().'cuestionario');
	}

	public function calificar()
	{
		if(!isset($_SESSION['usuario'])) header('Location: '.base_url());
		else
		{	
			$cuest_us = array();

			$model = new M_cuestionario();
			$usuarios = $model->cuest_usuario();
			if($usuarios!=false && count($usuarios) > 0)
			{
				foreach ($usuarios as $vus) {
					
					
					$resp_us = $model->resp_usold($vus->iIdUsuario);
					if($resp_us!=false && count($resp_us) > 0) $r = $resp_us[0]->iIdUsuario;
					else $r = 0;

					$cal_us = $model->calif_us($vus->iIdUsuario);
					if($cal_us!=false && count($cal_us) > 0)
					{						
						foreach ($cal_us as $vcal) {
							
							$d = array(
								'iIdUsuario'=> $vus->iIdUsuario,
								'vNombreUsuario'=> $vus->vNombreUsuario,
								'vCorreo'=> $vus->vCorreo,
								'vEntidad'=> $vus->vEntidad,
								'vMunicipio'=> $vus->vMunicipio,
								'iTipo'=> $vus->iTipo,
								'resp' => $r,
								'calif' => $vcal->calif,
								'cuestid' => $vcal->iIdCuestionario,
								'nom_cuest' => $vcal->vCuestionario
							);

							array_push($cuest_us, $d);
						}					
					}
					else
					{
						$d = array(
								'iIdUsuario'=> $vus->iIdUsuario,
								'vNombreUsuario'=> $vus->vNombreUsuario,
								'vCorreo'=> $vus->vCorreo,
								'vEntidad'=> $vus->vEntidad,
								'vMunicipio'=> $vus->vMunicipio,
								'iTipo'=> $vus->iTipo,
								'resp' => $r,
								'calif' => 0,
								'cuestid' => 0,
								'nom_cuest' => ''
							);

							array_push($cuest_us, $d);
					}
				}
			}
			$datos['usuarios'] = $cuest_us;
			$this->load->view('calificar',$datos);
		}
	}

	public function calificar_usuario()
	{
		$usid = $this->input->get('usid', TRUE);
		$cuestid = $this->input->get('cuestid', TRUE);

		if($usid > 0 && $cuestid > 0)
		{
			if(!isset($_SESSION['usuario'])) header('Location: '.base_url());
			else
			{
				$model = new M_cuestionario();
				$preguntas = $model->carga_preguntas(0,0,$cuestid);

				if($preguntas!=false && count($preguntas) > 0)
				{
					$datos['preguntas'] = $preguntas;
					foreach ($preguntas as $vpreg) {
						$pregid = $vpreg->iIdPregunta;



						$cal = $model->obtener_calif($pregid,$usid);
						if($cal!=false && count($cal) > 0) $vpreg->iCalificacion = $cal[0]->vCalificacion;
						else $vpreg->iCalificacion = 0;


						$resp_cal = $model->obtener_resp($pregid, $usid, 1);
						if($resp_cal!=false && count($resp_cal) > 0)
						{						
							$vpreg->vArchivo = $resp_cal[0]->vArchivo;
						}
						else 
						{
							$vpreg->vArchivo = '';
						}
					}				

				}
				else $datos['preguntas'] = false;

				$datos['usid'] = $usid;
				$resp = $model->resp_usuario($usid, $cuestid);
				if($resp!=false && count($resp) > 0) $datos['respuestas'] = $resp;
				else $datos['respuestas'] = false;
				$this->load->view('cuestionario_calif', $datos);
			}
		}
		else header('Location: '.base_url().'calificar');
		
	}


	public function dashboard() 
	{
		$model = new M_cuestionario();
		//$cuestionarios = $model->cuestionarios();
		$datos['cuest'] = (isset($_GET['cuestid'])  && !empty($_GET['cuestid'])) ? $this->input->get('cuestid', TRUE) : 0;
		$datos['centros'] = $model->carga_centros();
		$datos['zonas'] = $model->carga_zonas();
		
		
		$datos['centrosSelected'] = (isset($_GET['cuestid'])  && !empty($_GET['cuestid'])) ? $model->carga_centrosbycuestionario($this->input->get('cuestid', TRUE)) : null;
		$datos['zonasSelected'] = (isset($_GET['cuestid'])  && !empty($_GET['cuestid'])) ? $model->carga_zonasbycuestionario($this->input->get('cuestid', TRUE)) : null;
		
		if(isset($_GET['cuestid'])){
    		$query = $model->cuestionarios($this->input->get('cuestid', TRUE));
    		$query = $query->row();
    		foreach ($query as $campo => $valor)
            {
                $datos[$campo] = $valor;
            }
		}
		
		$cuest = $model->datos_cuest($this->input->get('cuestid', TRUE));
		$centros = $model->carga_centrosbycuestionario($this->input->get('cuestid', TRUE));
		
		$success = 0; 
		$error = 0;
		
		$centro = $cuest[0]->iTipo;
		$todos = $cuest[0]->iTodos;
		$soloNacional = $cuest[0]->bEsNacional;
		$soloEmpresarial = $cuest[0]->bEsEmpresarial;
		$nombrecuest = $cuest[0]->vCuestionario;
		
		$datos['nombre'] = $cuest[0]->vCuestionario;
		
		$time = strtotime($cuest[0]->dVigencia);
        $newformat = date('d/m/Y',$time);
                
		$fechalimite = $newformat;
        $infoExtra = "";

		if($todos > 0)
		{	
		    $infoExtra = "y Público en General.";
			$usuarios = $model->carga_usuarios($centros, 0);
		}
		else if($soloNacional > 0)
		{	
			$usuarios = $model->carga_usuarios(null, 1);
		}
		else if($soloNacional > 0 && $soloEmpresarial > 0){
		    $usuarios = $model->carga_usuarios($centros, 2);
		}
		else if($soloEmpresarial > 0)
		{
			$usuarios = $model->carga_usuarios($centros, 3);
		}
		else{
		    $infoExtra = "y Público en General.";
		    $usuarios = $model->carga_usuarios(null, 0);
		}

        $answered = array();
        $noanswered=array();
        $nacional=array();
        $empresarial=array();
        $counterusu = 0;
        
		foreach ($usuarios as $vus) {
		    $usuarioss = array();
		    
			$usuarioss["vCorreo"] = $vus->vCorreo;
			$usuarioss["vNombre"]  = $vus->vNombre;
			
			$resp = $model->resp_us($vus->iIdUsuario, $this->input->get('cuestid', TRUE));
			if(count($resp) == 0) {
                array_push($noanswered, $usuarioss);
			}else{
			    
			    if($vus->iIdCentro == 0){
    			    array_push($nacional, $usuarioss);
    			}
    			else{
    			    array_push($empresarial, $usuarioss);
    			}
			    array_push($answered, $usuarioss);
			}
			
			$counterusu++;
		}
		
		$datos["TotalUsersSent"] = $counterusu;
		$datos["TotalNacional"] = count($nacional);;
		$datos["TotalEmpresarial"] = count($empresarial);;
		
		$datos["TotalUsersNotTake"] = count($noanswered);
		
		$totalpublic = $model->total_resp_cuest($this->input->get('cuestid', TRUE));
		
		$datos["TotalUsersTake"] = count($answered) + (count($totalpublic) > 0 ? $totalpublic[0]->total : 0);
		$datos["TotalPublicUsers"] = count($totalpublic) > 0 ? $totalpublic[0]->total : 0;
		$datos["infoExtra"] = $infoExtra;

		if(!isset($_SESSION['usuario'])) header('Location: '.base_url());
		else $this->load->view('dashboard', $datos);
		
	}

	public function datos_generales() 
	{
		$model = new M_cuestionario();
		//$cuestionarios = $model->cuestionarios();
		$todos = $this->input->post('check_todos', TRUE);
		$publico = $this->input->post('check_publico', TRUE);
		$soloNacional = $this->input->post('check_nacional', TRUE);
		$soloEmpresarial = $this->input->post('check_empresarial', TRUE);
		$fromcuestionario = $this->input->post('fromC', TRUE);
	    $zonas = $this->input->post('zonasarray', TRUE);
		$centros = $this->input->post('centrosarray', TRUE);
		

    		$query = $model->cuestionarios($fromcuestionario);
    		$query = $query->row();
    		foreach ($query as $campo => $valor)
            {
                $datos[$campo] = $valor;
            }
		
		
		$cuest = $model->datos_cuest($fromcuestionario);
		//$centros = $model->carga_centrosbycuestionario($fromcuestionario);
		
		$success = 0; 
		$error = 0;
		
		$centro = $cuest[0]->iTipo;
		//$todos = $cuest[0]->iTodos;
		//$soloNacional = $cuest[0]->bEsNacional;
		//$soloEmpresarial = $cuest[0]->bEsEmpresarial;
		$nombrecuest = $cuest[0]->vCuestionario;
		
		$datos['nombre'] = $cuest[0]->vCuestionario;
		
		$time = strtotime($cuest[0]->dVigencia);
        $newformat = date('d/m/Y',$time);
                
		$fechalimite = $newformat;
        $infoExtra = "";

		if($todos > 0)
		{	
		    $infoExtra = "y Público en General.";
			$usuarios = $model->carga_usuariosReload($centros, 0);
		}
		else if($soloNacional > 0)
		{	
			$usuarios = $model->carga_usuariosReload(null, 1);
		}
		else if($soloNacional > 0 && $soloEmpresarial > 0){
		    $usuarios = $model->carga_usuariosReload($centros, 2);
		}
		else if($soloEmpresarial > 0)
		{
			$usuarios = $model->carga_usuariosReload($centros, 3);
		}
		else{
		    $infoExtra = "y Público en General.";
		    $usuarios = $model->carga_usuariosReload(null, 0);
		}

        $answered = array();
        $noanswered=array();
        $nacional=array();
        $empresarial=array();
        $counterusu = 0;
        
		foreach ($usuarios as $vus) {
		    $usuarioss = array();
		    
			$usuarioss["vCorreo"] = $vus->vCorreo;
			$usuarioss["vNombre"]  = $vus->vNombre;
			
			$resp = $model->resp_us($vus->iIdUsuario, $fromcuestionario);
			if(count($resp) == 0) {
                array_push($noanswered, $usuarioss);
			}else{
			    
			    if($vus->iIdCentro == 0){
    			    array_push($nacional, $usuarioss);
    			}
    			else{
    			    array_push($empresarial, $usuarioss);
    			}
			    array_push($answered, $usuarioss);
			}
			
			$counterusu++;
		}
		
		$datos["TotalUsersSent"] = $counterusu;
		$datos["TotalNacional"] = count($nacional);;
		$datos["TotalEmpresarial"] = count($empresarial);;
		
		$datos["TotalUsersNotTake"] = count($noanswered);
		
		$totalpublic = $publico > 0 ? $model->total_resp_cuest($fromcuestionario) : [];
		
		$datos["TotalUsersTake"] = count($answered) + (count($totalpublic) > 0 ? $totalpublic[0]->total : 0);
		$datos["TotalPublicUsers"] = count($totalpublic) > 0 ? $totalpublic[0]->total : 0;
		$datos["infoExtra"] = $infoExtra;

		echo json_encode($datos);		
	}
	
	public function registro()
	{
		$model = new M_usuario();
		$modelc = new M_cuestionario();
		$datos['centros'] = $model->carga_centros();
		$datos['zonas'] = $modelc->carga_zonas();
		
		$this->load->view('registro', $datos);
		
	}

	public function centros()
	{
		$model = new M_cuestionario();
		$datos['centros'] = $model->carga_centros();
		$datos['entidades'] = $model->carga_entidades();
		$datos['zonas'] = $model->carga_zonas();

		if(!isset($_SESSION['usuario'])) header('Location: '.base_url());
		else $this->load->view('centros', $datos);
	}
	
	public function zonas()
	{
		$model = new M_cuestionario();
		$datos['zonas'] = $model->carga_zonas();
		$datos['centros'] = $model->carga_centros();

		if(!isset($_SESSION['usuario'])) header('Location: '.base_url());
		else $this->load->view('zonas', $datos);
	}

	
}
