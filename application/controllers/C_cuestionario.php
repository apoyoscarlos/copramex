<?php
require APPPATH . 'libraries/PHPExcel.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class C_cuestionario extends CI_Controller {
	

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('M_cuestionario','mc');
		$this->load->model('M_usuario','mu');
		$this->load->library('upload');
		date_default_timezone_set('Mexico/General');
		session_start();
	}

	public function guarda_respuestas()
	{		
		$model = new M_cuestionario();
		$usid = 0;
		
		if(isset($_SESSION['usuario'])) { $usid = $_SESSION['usuario']['usid']; }
		
		$cuestid = $this->input->post('cuestid', TRUE);

		$datos = array('iRevisado' => 0, 'iCalificacion' => 0);
		$uiD = uniqid();
		
		if(is_array($_POST) && !empty($_POST)) 		
		{
			$archivo = array();
			$res = array();
			$ids = array();
			$idpreg = array();
			//var_dump($_POST);
			foreach ($_POST as $key => $value) {
				echo $key.': '.$value;
				
				$c = substr($key, 0, 3);			

				if($c=='ev_') 
				{
					$archivo[substr($key, 3)] = $value;
				}
				elseif($c=='re_')
				{
					$res[substr($key, 3)] = $value;
				}				
		
				else
				{
					if($key!='cuestid')
					{
						if(!in_array(substr($key, 5), $idpreg)) array_push($idpreg, substr($key, 5));

						if(is_array($value)) 
						{
							$calif_r = 0;
							$t_resp = count($value);
							for ($i=0; $i < count($value); $i++) {

								
								$datos['iIdRespuesta'] = $value[$i];
								$datos['iIdUsuario'] = $usid;
								if($usid == 0){
								    $datos['iIdAnonimo'] = $uiD;
								}
								if(isset($archivo[substr($key, 5)])) $datos['vArchivo'] = $archivo[substr($key, 5)];
								else $datos['vArchivo'] = "";
								//var_dump($datos);
								$inserta = $model->guarda_respuestas($datos);
							}

							
							$rangos = $model->rangos_respuestas(substr($key, 5));										

							


							if($t_resp >= $rangos[3]->iLimiteMin)
							{
								$calif_r = $rangos[3]->vValor;
							}
							elseif($t_resp >= $rangos[2]->iLimiteMin)
							{
								$calif_r = $rangos[2]->vValor;
							}
							elseif($t_resp >= $rangos[1]->iLimiteMin)
							{
								$calif_r = $rangos[1]->vValor;
							}
							else 
							{
								$calif_r = $rangos[0]->vValor;
							}
							
							try{
    							$datos_cal['iIdUsuario'] = $usid;
    							$datos_cal['iIdPregunta'] = substr($key, 5);
    							$datos_cal['vCalificacion'] = $calif_r;
    
    							//$calif = $model->guarda_calif($datos_cal);			
							}
							catch(Exception $e){
							       //Nothing
							 }
						}
						else 
						{
                            log_message("error","ID: ".$value);
                            
							array_push($ids, $value);		
							$datos['iIdRespuesta'] = $value;
							$datos['iIdUsuario'] = $usid;
							if($usid == 0){
								    $datos['iIdAnonimo'] = $uiD;
								}
							if(isset($archivo[substr($key, 5)])) $datos['vArchivo'] = $archivo[substr($key, 5)];
							else $datos['vArchivo'] = "";
							//var_dump($datos);
							$inserta = $model->guarda_respuestas($datos);


							$val_resp = $model->valor_opcion($value);
							
							if($val_resp[0]->iIdPregunta != NULL && $val_resp[0]->vValor != NULL){
							    
							    try{
        							$datos_cal['iIdUsuario'] = $usid;
        							$datos_cal['iIdPregunta'] = $val_resp[0]->iIdPregunta;
        							$datos_cal['vCalificacion'] = $val_resp[0]->vValor;
        							
        							//$calif = $model->guarda_calif($datos_cal);
							    }
							    catch(Exception $e){
							       //Nothing
							    }
							}

							
						}						
					}
					
				}
			
			}
			
			//var_dump($ids);
			//var_dump($res);
			//if($usid > 0){
    			foreach ($res as $key => $value) {
    				if(in_array($key, $ids)) $act = $model->act_resp($value,$usid,$key,$uiD);
    			}
			//}
			
			$tpreg = $model->preguntas_cuestionario($cuestid);
			//var_dump($idpreg);
			//var_dump($tpreg);
			if($tpreg!=false) 
			{
				foreach ($tpreg as $valpreg) {
					//echo 'preguntaid: '.$valpreg->iIdPregunta;
					if(!in_array($valpreg->iIdPregunta, $idpreg))
					{
					      try{
        						$d_cal['iIdUsuario'] = $usid;
        						$d_cal['iIdPregunta'] = $valpreg->iIdPregunta;
        						$d_cal['vCalificacion'] = 0;						
        
        						//$calif = $model->guarda_calif($d_cal);
					      }
    					  catch(Exception $e){
    						       //Nothing
    						}
					}
				}
			}
			
			//Save historic
			$datos_cuesth['iIdUsuario'] = $usid;
    		$datos_cuesth['iIdCuestionario'] = $cuestid;
    		$datos_cuesth['dFechaEnvio'] = date("Y-m-d");
    		if($usid == 0){
			    $datos_cuesth['iIdAnonimo'] = $uiD;
			}
            $model->guarda_cuestionariohistorial($datos_cuesth);
		
		}
		echo $inserta;
	}

	public function archivo()
	{		
		//print_r($_FILES);

		$ruta = 'uploads/';
		$model = new M_cuestionario();
		$usid = $_SESSION['usuario']['usid'];
		//$tArchivos = 0;
		//$dFecha = date("Y-m-d H:i:s");
					
		//$tArchivos = count($_FILES["input_1"]["name"]);
		foreach ($_FILES as $key => $value) {
			//echo 'key: '.$key;

			$vNombreAdjunto = $_FILES[$key]["name"];
			$nombreTemp = $_FILES[$key]["tmp_name"];			
			$pregid = substr($key, 6);

			$respid = $model->obtener_resp($pregid, $usid, 0);
			$nombreArch = 'Evidencia_'.$pregid.'_'.$usid;			

			$resto = explode(".", $vNombreAdjunto); 
			$extension = end($resto);
			$nombreArch.='.'.$extension;

			$vRutaAdjunto = $ruta.$nombreArch;			

			move_uploaded_file($nombreTemp, $vRutaAdjunto);
			$datos = array('vArchivo' => $nombreArch);
			$resp = $model->inserta_archivo($datos,$respid[0]->iIdRespuesta,$usid);
		}		
		echo $resp;
	}

	public function eliminar_preg()
	{
		$pregid = $this->input->post('pregid');
		$model = new M_cuestionario();
		$resp = $model->elimina_preg($pregid);
		echo $resp;
	}

	public function pegar_pregunta(){
		$cuestid = $this->input->post('cuestId');
		$pregid = $this->input->post('pregId');
		$model = new M_cuestionario();

		$pregunta = $model->pregunta($pregid);
		$datosp = array(
			'vPregunta' => $pregunta->vPregunta,
			'iPonderacion' => 1,
			'iEvidencia' => $pregunta->iEvidencia,
			'iTipoPregunta' => $pregunta->iTipoPregunta,
			'iActivo' => 1,
			'vLink' => $pregunta->vLink,
			'iOrden' => $pregunta->iOrden,
			'iObligatorio' => $pregunta->iObligatorio,
			'iNumero' => $pregunta->iNumero,
			'iIdCuestionario' => $cuestid
			);

		$iIdPregunta = $this->mc->insertar_registro('iplan_preguntas', $datosp);

		$opciones = $model->opciones($pregid);
					$opciones = $opciones->result();
					foreach($opciones as $op){
						$datosop = array(
							'vOpcion' => $op->vOpcion,
							'iOtro' => $op->iOtro,
							'iTipoR' => $op->iTipoR,
							'vValor' => $op->vValor,
							'iActivo' => 1,
							'iIdPregunta' => $iIdPregunta
							);
			
							$iIdOpcion = $this->mc->insertar_registro('iplan_opciones', $datosop);

							log_message("error", "IdOPCION: ".$iIdOpcion);

							$datospreg = array(
								'iIdOpcion' => $iIdOpcion,
								'iActivo' => 1,
								'iIdPregunta' => $iIdPregunta
								);
	
							$insert_r = $this->mc->guarda_resp($datospreg);

							log_message("error", "INSERT: ".$insert_r);
					}

					echo $iIdPregunta;
	}

	public function duplicar_cuestionario(){
		$cuestid = $this->input->post('cuestId');
		$model = new M_cuestionario();

		$result = $model->cuestionarios($cuestid);
		$cuestionario = $result->result();

		$datos = array(	'vCuestionario' => $cuestionario[0]->vCuestionario,
						'iTipo' => $cuestionario[0]->iTipo,
						'iTodos' => $cuestionario[0]->iTodos,
						'bEsPublico' => $cuestionario[0]->bEsPublico,
						'bEsNacional' => $cuestionario[0]->bEsNacional,
						'bEsEmpresarial' => $cuestionario[0]->bEsEmpresarial,
						'vDescripcion' => $cuestionario[0]->vDescripcion,
						'vInfoGeneral' => $cuestionario[0]->vInfoGeneral,
						'vAvisoPriv' => $cuestionario[0]->vAvisoPriv,
						'vTextFinal' =>$cuestionario[0]->vTextFinal,
						'vLinkFinal' => $cuestionario[0]->vLinkFinal,
						'dFechaCreacion' => $cuestionario[0]->dFechaCreacion,
						'dVigencia' => $cuestionario[0]->dVigencia,
						'iActivo' => 1 );

		$iIdCuestionario = $this->mc->insertar_registro('iplan_cuestionarios', $datos);

		$preguntas = $model->preguntas_cuestionario($cuestid);
		$preguntasadd = array();
		if($preguntas != false){
			foreach($preguntas as $preg){
				$datosp = array(
					'vPregunta' => $preg->vPregunta,
					'iPonderacion' => $preg->iPonderacion,
					'iEvidencia' => $preg->iEvidencia,
					'iTipoPregunta' => $preg->iTipoPregunta,
					'iActivo' => 1,
					'vLink' => $preg->vLink,
					'iOrden' => $preg->iOrden,
					//'iObligatorio' => $preg->iObligatorio,
					'iNumero' => $preg->iNumero,
					'iIdCuestionario' => $iIdCuestionario
					);

					$iIdPregunta = $this->mc->insertar_registro('iplan_preguntas', $datosp);

					//Opciones
					$opciones = $model->opciones($preg->iIdPregunta);
					$opciones = $opciones->result();
					foreach($opciones as $op){
						$datosop = array(
							'vOpcion' => $op->vOpcion,
							'iOtro' => $op->iOtro,
							'iTipoR' => $op->iTipoR,
							'vValor' => $op->vValor,
							'iActivo' => 1,
							'iIdPregunta' => $iIdPregunta
							);
			
							$iIdOpcion = $this->mc->insertar_registro('iplan_opciones', $datosop);

							log_message("error", "IdOPCION: ".$iIdOpcion);

							$datospreg = array(
								'iIdOpcion' => $iIdOpcion,
								'iActivo' => 1,
								'iIdPregunta' => $iIdPregunta
								);
	
							$insert_r = $this->mc->guarda_resp($datospreg);

							log_message("error", "INSERT: ".$insert_r);
					}
			}
		}

		/*$preguntashijas = $model->preguntas_hijascuestionario($cuestid);
		foreach($preguntashijas as $preg){
			$datosp = array(
				'iIdPreguntaPadre' => $preg->vPregunta,
				'iPonderacion' => $preg->iPonderacion,
				'iEvidencia' => $preg->$iEvidencia,
				'iTipoPregunta' => $preg->$iTipoPregunta,
				'iActivo' => 1,
				'vLink' => $preg->$vLink,
				'iOrden' => $preg->$iOrden,
				'iNumero' => $preg->$iNumero,
				'iIdCuestionario' => $iIdCuestionario
				);

				$iIdPregunta = $this->mc->insertar_registro('iplan_preguntas', $datosp);
		}*/
	}

	public function carga_respuestas()
	{
		$op = $this->input->post('op', TRUE);
		$model = new M_cuestionario();
		$resp = $model->carga_resp($op);
		if($resp!=false && count($resp) > 0)
		{
			foreach ($resp as $vresp) {
				if($op==1 || $op==2) { $check = 'checked'; $col = ''; }
				else { $check = ''; $col = 'col-md-2'; }

				echo '<div class="'.$col.' custom-control custom-checkbox custom-control-inline">
	                    <input '.$check.' type="checkbox" id="op_'.$vresp->iIdOpcion.'" name="resp[]" class="custom-control-input" value="'.$vresp->iIdOpcion.'">
	                    <label class="custom-control-label" for="op_'.$vresp->iIdOpcion.'">'.$vresp->vOpcion.'</label>
	                </div>';
			}			
		}
		else echo '<a href="'.base_url().'listado-respuestas">Agregar respuestas</a>';


	}

	public function guardar_pregunta()
	{
		$nombre = $this->input->post('nombre', TRUE);
		$correo = $this->input->post('correo', TRUE);
		$sel_tipo = $this->input->post('sel_tipo', TRUE);
		$sel_preg = $this->input->post('sel_preg', TRUE);
		$resp = $this->input->post('resp', TRUE);
		$link = $this->input->post('link', TRUE);
		$iOrden = $this->input->post('iOrden', TRUE);
		$iObligatorio = $this->input->post('iObligatorio', TRUE);
		
		$model = new M_cuestionario();

		$datos = array(
				'vPregunta' => $nombre,
				'iPonderacion' => $correo,
				'iEvidencia' => $sel_tipo,
				'iTipoPregunta' => $sel_preg,
				'iActivo' => 1,
				'vLink' => $link,
				'iOrden' => $iOrden,
				'iObligatorio' => $iObligatorio
				);

		$insert = $model->guarda_pregunta($datos);

		if($insert>0) 
		{
			for ($i=0; $i < count($resp); $i++) {
				$datos_r = array(
					'iIdPregunta' => $insert,
					'iIdOpcion' => $resp[$i],
					'iActivo' => 1
				);

				$insert_r = $model->guarda_resp($datos_r);
			}
		}
		echo $insert_r;
	}		

	public function actualizar_pregunta()
	{		
		$pregid = $this->input->post('pregid');
		$preg = $this->input->post('nombre');
		$pond = $this->input->post('correo');
		$ev = $this->input->post('sel_tipo');
		$tipo = $this->input->post('sel_preg');
		$resp = $this->input->post('resp');
		$datos = array(
				'vPregunta' => $preg,
				'iPonderacion' => $pond,
				'iEvidencia' => $ev,
				'iTipoPregunta' => $tipo
			);

		$model = new M_cuestionario();
		$act = $model->actualiza_pregunta($pregid,$datos);
		echo $act;
		if($act==1)
		{
			for ($i=0; $i < count($resp); $i++) {
				$datos_r = array(
					'iIdPregunta' => $pregid,
					'iIdOpcion' => $resp[$i],
					'iActivo' => 1
				);

				$insert_r = $model->guarda_resp($datos_r);
				echo $insert_r;
			}
		}
	}

	public function eliminar_resp()
	{
		$respid = $this->input->post('respid', TRUE);
		$model = new M_cuestionario();
		$resp = $model->elimina_resp($respid);
		echo $resp;
	}

	public function elimina_centro()
	{
		$cuestid = $this->input->post('cuestid', TRUE);
		$model = new M_cuestionario();
		$resp = $model->elimina_cent($cuestid);
		echo $resp;
	}
	
	public function elimina_zona()
	{
		$zonaid = $this->input->post('zonaid', TRUE);
		$model = new M_cuestionario();
		$resp = $model->elimina_zona($zonaid);
		echo $resp;
	}

	public function guarda_opcion()
	{
		$respid = $this->input->post('respid', TRUE);
		$resp = $this->input->post('resp', TRUE);
		$tipo = $this->input->post('tipo_resp', TRUE);
		$otro = $this->input->post('otro_resp', TRUE);

		$model = new M_cuestionario();
		$datos = array('vOpcion' => $resp, 'iOtro' => $otro, 'iTipoR' => $tipo, 'iActivo' => 1);
		$resp = $model->guarda_opcion($datos,$respid);	
		echo $resp;
	}

	public function envia_calif()
	{
		$model = new M_cuestionario();
		$usid = $this->input->post('usid', TRUE);

		if(is_array($_POST) && !empty($_POST)) 		
		{
			$calif = array();
			foreach ($_POST as $key => $value) {
				//echo 'key: '.$key.'<br>--> value: '.$value;
				
				$c = substr($key, 0, 6);			

				if($c=='calif_') 
				{
					$d = substr($key, 6);
					$calif[$d] = $value;
					$datos = array('iCalificacion' => $value);

					$resp_id = $model->obtener_resp($d, $usid);
					$resp = $resp_id[0]->iIdRespuesta;
					$resp_calif = $model->act_calif($resp, $usid, $datos);
					//echo 'usuarioid: '.$usid.'<br> preguntaid: '.$d.'<br> respuestaid: '.$resp_id[0]->iIdRespuesta.'----<br>';

				}
			}
			echo $resp_calif;			
		}
	}
	

	function listar_cuestionarios()
	{
		if(!isset($_SESSION['usuario'])) header('Location: '.base_url());
		else
		{

			$hoy = date("Y-m-d");
			$vigencia = date("Y-m-d",strtotime($hoy."+ 2 days"));
			$cuestionarios = $this->mc->cuestionarios(0, $hoy, $vigencia);
			$datos['vigencia'] = $cuestionarios->result();
			$datos['tabla'] = $this->tabla_cuestionarios($cuestionarios->result());
			$this->load->view('listado_cuestionarios',$datos);
		}
		
	}

	public function guarda_centro()
	{
		$cuestid = $this->input->post('cuestid', TRUE);
		$nom_cent = $this->input->post('nom_cent', TRUE);
		$ent_cent = $this->input->post('ent_cent', TRUE);	
		$zona_cent = $this->input->post('zona_cent', TRUE);	

		$model = new M_cuestionario();

		$datos = array('vNombre' => $nom_cent, 'iIdEntidad' => $ent_cent, 'nLatitud' => 20.967778 , 'nLongitud' => -89.621667, 'iActivo' => 1, 'iIdCategoria' => $zona_cent);
		$resp = $model->guardar_centro($datos,$cuestid);	
		echo $resp;
	}
	
	public function guarda_zona()
	{
		$nom_zona = $this->input->post('nom_zona', TRUE);
		$zonaid = $this->input->post('zonaid', TRUE);
		$centros = $this->input->post('centrosarray', TRUE);
		$centrosArray = explode(",", $centros);
		
		log_message("error", $centros);
		
		$model = new M_cuestionario();

      
        $datos = array('vNombre' => $nom_zona, 'iActivo' => 1);
        $resp = $model->guarda_zona($datos, $zonaid, $centrosArray);	
        echo $resp;
        
	}

	function tabla_cuestionarios($vigencia)
	{

		$cuestionarios = $this->mc->cuestionarios();


		$html = ' <div class="table-responsive">                                 
                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Nombre del cuestionario</th>
                                <th>Centro Específico</th>
                                <th>Fecha Inicio</th>
                                <th>Fecha Fin</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>';
         if($cuestionarios->num_rows() > 0)
         {
         	$cuestionarios = $cuestionarios->result();

         	foreach ($cuestionarios as $cuestionario)
         	{
         		//$cuestionario->vNombre = ($cuestionario->iTipo == 1) ? 'Todos': $cuestionario->vNombre;
         		$time = strtotime($cuestionario->dVigencia);
                $newformat = date('d/m/Y',$time);
                
                $creacion = strtotime($cuestionario->dFechaCreacion);
                $newformatc = date('d/m/Y',$creacion);

         		  $html.= '</td>
                        <td>'.$cuestionario->vCuestionario.'</td>
                        <td>'.$cuestionario->vNombre.'</td>
                        <td>'.$newformatc.'</td>
                        <td>'.$newformat.'</td>
                        <td>';
                            
                        $html.='<a href="javascript:" title="Editar" onclick="capturarCuestionario('.$cuestionario->iIdCuestionario.');"><i class="fas fa-pencil-alt"></i></a>&nbsp&nbsp&nbsp';

                        $html.='<a href="javascript:" title="Eliminar" onclick="eliminarCuestionario('.$cuestionario->iIdCuestionario.');"><i class="fas fa-times"></i></a>&nbsp&nbsp&nbsp';       
                        
                        $html.='<a href="javascript:" title="Notificar" onclick="cargarPersonasPorFiltro('.$cuestionario->iIdCuestionario.');"><i class="fas fa-envelope"></i></a>&nbsp&nbsp&nbsp';
                        
                         $html.='<a href="javascript:" title="Ver en Dashboard" onclick="goToDashboard('.$cuestionario->iIdCuestionario.');"><i class="fas fa-eye"></i></a>&nbsp&nbsp&nbsp';
                         
						 $html.='<a href="javascript:" title="Contestar el Cuestionario" onclick="goToResponder('.$cuestionario->iIdCuestionario.');"><i class="fas fa-check"></i></a>&nbsp&nbsp&nbsp';
						 $html.='<a href="javascript:" title="Copiar Cuestionario" onclick="duplicar('.$cuestionario->iIdCuestionario.');"><i class="fas fa-copy"></i></a>&nbsp&nbsp&nbsp';

						 $html.='<a href="javascript:" title="Pegar Pregunta" onclick="pegar_pregunta('.$cuestionario->iIdCuestionario.');"><i class="fas fa-paste"></i></a>';

                    $html.= '</td>
                    </tr>';
         	}
         }


        $html .= '		</tbody>
        			</table>
        		</div>';

        return $html;                 
	}

	function capturar_cuestionario()
	{
		$iIdCuestionario = (isset($_GET['cuestid']) && !empty($_GET['cuestid'])) ? $_GET['cuestid']:0;
		$datos['iIdCuestionario'] = $iIdCuestionario;


		if($iIdCuestionario == 0)
		{
			$d_cuestionario = array('vCuestionario' => '', 'vDescripcion' => '');
			$iIdCuestionario = $this->mc->insertar_registro('iplan_cuestionarios',$d_cuestionario);
		}

		$query = $this->mc->cuestionarios($iIdCuestionario);
		$datos['centros'] = $this->mu->carga_centros();
		$datos['zonas'] = $this->mc->carga_zonas();
		
	    $datos['centrosSelected'] = $this->mc->carga_centrosbycuestionario($iIdCuestionario);
		$datos['zonasSelected'] = $this->mc->carga_zonasbycuestionario($iIdCuestionario);
		
		$query = $query->row();
		foreach ($query as $campo => $valor)
        {
            $datos[$campo] = $valor;
        }		

		$qpreguntas = $this->mc->preguntas($iIdCuestionario);
		
		$qpreguntas = $qpreguntas->result();

		$datos['preguntas'] = '';

		foreach ($qpreguntas as $pregunta) {
			$datos['preguntas'].= $this->html_pregunta($pregunta->iIdPregunta, $iIdCuestionario);
		}

		$this->load->view('form_captura_cuestionario',$datos);

	}

	function guardar_cuestionario()
	{
		$todos = (isset($_POST['check_todos'])) ? 1 : 0;
		$publico = (isset($_POST['check_publico'])) ? 1 : 0;
		$esnacional = (isset($_POST['check_nacional'])) ? 1 : 0;
		$esempresarial = (isset($_POST['check_empresarial'])) ? 1 : 0;
		
		$zonas = $this->input->post('zonasarray', TRUE);
		$zonasArray = explode(",", $zonas);
		$centros = $this->input->post('centrosarray', TRUE);
		$centrosArray = explode(",", $centros);
		
		//log_message('error', 'ZONAS: '.$zonas); 
		//log_message('error', 'CENTROS: '.$centros); 
		
		$datos = array(	'vCuestionario' => $this->input->post('vCuestionario'),
						'vDescripcion' => '',
						'iTipo' => $this->input->post('iTipo'),
						'iTodos' => $todos,
						'bEsPublico' => $publico,
						'bEsNacional' => $esnacional,
						'bEsEmpresarial' => $esempresarial,
						'vDescripcion' => $this->input->post('vDescripcion'),
						'vInfoGeneral' => $this->input->post('vInfoGeneral'),
						'vAvisoPriv' => $this->input->post('vAvisoPriv'),
						'vTextFinal' => $this->input->post('vTextFinal'),
						'vLinkFinal' => $this->input->post('vLinkFinal'),
						'dFechaCreacion' => $this->input->post('fecha_ini'),
						'dVigencia' => $this->input->post('fecha_term'),
						'iActivo' => 1 );
		$where['iIdCuestionario'] = $this->input->post('iIdCuestionario');

		$con = $this->mc->iniciar_transaccion();
		$iIdCuestionario = $this->mc->actualizar_registro('iplan_cuestionarios',$where,$datos,$con);
		
		 //Zonas
         $listadoZonas = array();
         $this->mc->delete_filtros_zona($this->input->post('iIdCuestionario'));
         for ($i = 0; $i < count($zonasArray); $i++) {
             $dataE                 = array();
             $dataE['iIdCategoria'] = $zonasArray[$i];
             $dataE['iIdCuestionario'] = $this->input->post('iIdCuestionario');
             array_push($listadoZonas, $dataE);
         }
         $this->mc->guardar_filtros_zona($listadoZonas);
         
         //Centros
         $listadoCentros = array();
         $this->mc->delete_filtros_centros($this->input->post('iIdCuestionario'));
         for ($i = 0; $i < count($centrosArray); $i++) {
             $dataE                 = array();
             $dataE['iIdCentro'] = $centrosArray[$i];
             $dataE['iIdCuestionario'] = $this->input->post('iIdCuestionario');
             array_push($listadoCentros, $dataE);
         }
         $this->mc->guardar_filtros_centros($listadoCentros);

		if($this->mc->terminar_transaccion($con)) echo '1';
		else echo 'Los cambios no pudieron ser guardados';
	}

	public function crear_pregunta()
	{
		if(isset($_POST['iIdCuestionario']) && !empty($_POST['iIdCuestionario']))
		{
			$preg = $this->input->post('preg', TRUE);
			$datos = array(	'vPregunta' => $preg, 'iTipoPregunta' => 0, 'iEvidencia'=> 1,'iIdCuestionario' => $this->input->post('iIdCuestionario'));

			$con = $this->mc->iniciar_transaccion();

			$iIdPregunta = $this->mc->insertar_registro('iplan_preguntas',$datos,$con);

			$opcion1 = array( 'vOpcion' => 'Opcion 1' , 'iIdPregunta' => $iIdPregunta, 'vValor' => 0);
			$opcion2 = array( 'vOpcion' => 'Opcion 2' , 'iIdPregunta' => $iIdPregunta, 'vValor' => 1);
			$opcion3 = array( 'vOpcion' => 'Opcion 3' , 'iIdPregunta' => $iIdPregunta, 'vValor' => 2);
			$opcion4 = array( 'vOpcion' => 'Opcion 4' , 'iIdPregunta' => $iIdPregunta, 'vValor' => 3);

			$iIdOpcion1 = $this->mc->insertar_registro('iplan_opciones',$opcion1,$con);
			$iIdOpcion2 = $this->mc->insertar_registro('iplan_opciones',$opcion2,$con);
			$iIdOpcion3 = $this->mc->insertar_registro('iplan_opciones',$opcion3,$con);
			$iIdOpcion4 = $this->mc->insertar_registro('iplan_opciones',$opcion4,$con);

			$rango1 = array( 'vValor' => 0 , 'iIdPregunta' => $iIdPregunta, 'iLimiteMin' => '', 'iLimiteMax' => 0);
			$rango2 = array( 'vValor' => 1 , 'iIdPregunta' => $iIdPregunta, 'iLimiteMin' => '', 'iLimiteMax' => 0);
			$rango3 = array( 'vValor' => 2 , 'iIdPregunta' => $iIdPregunta, 'iLimiteMin' => '', 'iLimiteMax' => 0);
			$rango4 = array( 'vValor' => 3 , 'iIdPregunta' => $iIdPregunta, 'iLimiteMin' => '', 'iLimiteMax' => 0);
			

			$iIdRango1 = $this->mc->insertar_registro('iplan_rangos',$rango1,$con);
			$iIdRango2 = $this->mc->insertar_registro('iplan_rangos',$rango2,$con);
			$iIdRango3 = $this->mc->insertar_registro('iplan_rangos',$rango3,$con);
			$iIdRango4 = $this->mc->insertar_registro('iplan_rangos',$rango4,$con);

			//	-- 
			//	Insertamos la configuración
			$respuesta1 = array('iIdPregunta' => $iIdPregunta, 'iIdOpcion' => $iIdOpcion1);
			$respuesta2 = array('iIdPregunta' => $iIdPregunta, 'iIdOpcion' => $iIdOpcion2);
			$respuesta3 = array('iIdPregunta' => $iIdPregunta, 'iIdOpcion' => $iIdOpcion3);
			$respuesta4 = array('iIdPregunta' => $iIdPregunta, 'iIdOpcion' => $iIdOpcion4);

			$iIdRespuesta1 = $this->mc->insertar_registro('iplan_respuestas',$respuesta1,$con);
			$iIdRespuesta2 = $this->mc->insertar_registro('iplan_respuestas',$respuesta2,$con);
			$iIdRespuesta3 = $this->mc->insertar_registro('iplan_respuestas',$respuesta3,$con);
			$iIdRespuesta4 = $this->mc->insertar_registro('iplan_respuestas',$respuesta4,$con);

			if($this->mc->terminar_transaccion($con) > 0) echo '1-'.$iIdPregunta;
			else echo '0-La pregunta no pudo ser creada'; 
		}
		
	}

	public function agregar_opcion()
	{
		if(isset($_POST['iIdPregunta']) && !empty($_POST['iIdPregunta']))
		{
			$iIdPregunta = $this->input->post('iIdPregunta');
			
			//	Iniciamos la transacción
			$con = $this->mc->iniciar_transaccion();

			//	Insertamos la opción
			$opcion = array( 'vOpcion' => '' , 'iIdPregunta' => $iIdPregunta, 'vValor' => -1);
			$iIdOpcion = $this->mc->insertar_registro('iplan_opciones',$opcion,$con);

			//	Insertamos la configuración
			$respuesta = array('iIdPregunta' => $this->input->post('iIdPregunta'), 'iIdOpcion' => $iIdOpcion);
			$iIdRespuesta = $this->mc->insertar_registro('iplan_respuestas',$respuesta,$con);

			//	Terminar transacción
			if($this->mc->terminar_transaccion($con)) echo '1';
			else echo 'La pregunta no pudo ser creada'; 
		}
		
	}

	function guardar_texto_pregunta(){
		if(isset($_POST['iIdPregunta']) && !empty($_POST['iIdPregunta']))
		{
			$datos['vPregunta'] = $this->input->post('vPregunta');
			$datos['vLink'] = $this->input->post('link');
			$datos['iOrden'] = $this->input->post('iOrden');
			$datos['iObligatorio'] = $this->input->post('iObligatorio');
			$where['iIdPregunta'] = $this->input->post('iIdPregunta');
			$this->mc->actualizar_registro('iplan_preguntas',$where,$datos);
		}
	}

	function guardar_requiere_evidencia(){
		if(isset($_POST['iIdPregunta']) && !empty($_POST['iIdPregunta']))
		{
			$datos['iEvidencia'] = $this->input->post('iEvidencia');
			$where['iIdPregunta'] = $this->input->post('iIdPregunta');
			$this->mc->actualizar_registro('iplan_preguntas',$where,$datos);
		}
	}
	
	
	function guardar_relacion_pregunta(){
		if(isset($_POST['iPreguntaHija']) && !empty($_POST['iPreguntaPadre']))
		{
			$datos['iIdPreguntaHijo'] = $this->input->post('iPreguntaHija');
			$datos['iIdPreguntaOpcion'] = $this->input->post('iPreguntaOpcion');
			$datos['iIdPreguntaPadre'] = $this->input->post('iPreguntaPadre');
			$datos['iIdCuestionario'] = $this->input->post('iIdCuestionario');
			
			$existe = $this->mc->verificarPregunta($this->input->post('iPreguntaHija'), $this->input->post('iPreguntaPadre'));
			if(count($existe)){
			    echo 2;
			    return 2;
			}
			
			
			$result = $this->mc->delete_pregunta_matriz($this->input->post('iIdCuestionario'), $this->input->post('iPreguntaPadre'), $this->input->post('iPreguntaOpcion'));
			
			if($this->input->post('iPreguntaHija') > 0){
			    $this->mc->guarda_pregunta_matriz($datos);
			    echo 1;
			}
		}
		
		echo 0;
	}

	function guardar_texto_opcion(){
		if(isset($_POST['iIdOpcion']) && !empty($_POST['iIdOpcion']))
		{
			$datos['vOpcion'] = $this->input->post('vOpcion');
			$where['iIdOpcion'] = $this->input->post('iIdOpcion');
			$this->mc->actualizar_registro('iplan_opciones',$where,$datos);
		}
	}

	function guardar_rango(){
		if(isset($_POST['iIdPregunta']) && !empty($_POST['iIdPregunta']))
		{
			$datos = array( 'iLimiteMin' => $this->input->post('iLimiteMin'));
			$where['iIdPregunta'] = $this->input->post('iIdPregunta');
			$where['vValor'] = $this->input->post('vValor');
			if($this->mc->actualizar_registro('iplan_rangos',$where,$datos)) echo 1;
			else echo 'El registro no pudo ser actualizado';
		}
	}

	public function mostrar_pregunta($iIdPregunta,$iIdCuestionario)
	{
		echo $this->html_pregunta($iIdPregunta,$iIdCuestionario);
	}

	public function mostrar_opciones($iIdPregunta,$iIdCuestionario)
	{
		echo $this->html_opciones($iIdPregunta,$iIdCuestionario);
	}

	public function html_pregunta($iIdPregunta,$iIdCuestionario)
	{
		$p = $this->mc->pregunta($iIdPregunta);
		$sel1 = ($p->iTipoPregunta == 0) ? 'selected':'';
		$sel2 = ($p->iTipoPregunta == 1) ? 'selected':'';
		$sel3 = ($p->iTipoPregunta == 2) ? 'selected':'';
		$sel4 = ($p->iTipoPregunta == 3) ? 'selected':'';
		$sel5 = ($p->iTipoPregunta == 4) ? 'selected':'';

		$sel1_e = ($p->iEvidencia == 0) ? 'selected':'';
		$sel2_e = ($p->iEvidencia == 1) ? 'selected':'';


		$html = '<div class="card" id="div-pregunta-'.$p->iIdPregunta.'"> 
					<div class="card-body">
					<form name="form-preg'.$p->iIdPregunta.'" id="form-preg'.$p->iIdPregunta.'">
						<div class="row">
							<div class="col-md-12 text-right"><i style="cursor:pointer;" title="Elimnar pregunta" onclick="eliminarPregunta('.$p->iIdPregunta.');" class="fas fa-times"></i></div>
						</div>
	                    <div class="row">
	                        <div class="col-md-7">
	                            <div class="form-group">
	                                <input type="hidden" name="iIdPregunta" id="iIdPregunta" value="'.$p->iIdPregunta.'">
	                                <input type="text" class="form-control required" id="vPregunta" name="vPregunta" value="'.$p->vPregunta.'" onblur="guardarTextoPregunta('.$p->iIdPregunta.');" placeholder="Escriba aquí su pregunta">
	                            </div>
							</div>
							<div class="col-md-2">
	                            <div class="form-group">
									<label><input type="checkbox" id="iObligatorio" name="iObligatorio" '.($p->iObligatorio > 0 ? "checked" : "").' onchange="guardarTextoPregunta('.$p->iIdPregunta.');"> Es Obligatorio</label>
	                            </div>
	                        </div>
							<div class="col-md-2">
	                            <div class="form-group">
	                                <input type="number" class="form-control " id="iOrden" name="iOrden" value="'.$p->iOrden.'" onblur="guardarTextoPregunta('.$p->iIdPregunta.');" placeholder="Orden">
	                            </div>
							</div>
							<div class="col-md-1">
	                            <a href="javascript:copiarPregunta('.$p->iIdPregunta.')" title="Copiar Pregunta"><i class="fas fa-copy"></i></a>
	                        </div>
	                        <div class="col-md-12">
	                            <div class="form-group">
	                                <input type="hidden" name="vLink" id="vLink" value="'.$p->vLink.'">
	                                <input type="text" class="form-control required" id="link" name="link" value="'.$p->vLink.'" onblur="guardarTextoPregunta('.$p->iIdPregunta.');" placeholder="Link">
	                            </div>
	                        </div>
	                    </div>
	                    <div class="row">
                            <div class="col-md-12">
                                 <div class="form-group">
                                    <label for="iTipo"> Tipo de pregunta: <span class="text-danger">*</span> </label>
                                    <select name="iTipo" id="iTipo" class="form-control" onchange="cambiarTipoPregunta('.$p->iIdPregunta.');">
                                        <option value="0" '.$sel1.'>Opción multiple</option>
                                        <option value="1" '.$sel2.'>Dicotómica</option>
                                        <option value="2" '.$sel3.'>Abierta</option>
                                        <option value="3" '.$sel4.'>Selección multiple</option>
                                        <option value="4" '.$sel5.'>Priorización</option>
                                    </select>
                                </div>
                            </div>
                        </div>
	                </form>';

       

	    $html.= '<div class="row">
	    			<div id="div-opciones'.$p->iIdPregunta.'" class="col-md-12">'.$this->html_opciones($p->iIdPregunta,$iIdCuestionario).'</div>';

	   	/* if($p->iTipoPregunta == 3)
	    {
	    	$html.= '<div id="div-rangos'.$p->iIdPregunta.'" class="col-md-5">'.$this->html_rangos($iIdPregunta).'</div>';
	    }*/

	    $html.=	'</div>
	    		</div>
	    	</div>';

	    return $html;
	}

	public function html_rangos($iIdPregunta)
	{
		$html = '';
		$query = $this->mc->rangos($iIdPregunta);
		$query = $query->result();
		$html .=  '<div class="row">
						<div class="col-md-12"><small>Indique el el número de opciones que deben seleccionarse para cada puntaje</small></b></div>
					</div>';
		foreach ($query as $p)
		{
			$html.= '<form name="form-rango-'.$iIdPregunta.'-'.$p->vValor.'" id="form-rango-'.$iIdPregunta.'-'.$p->vValor.'">
					<div class="row">
						<div class="col-md-12">
							<input type ="hidden" name="iIdPregunta" value="'.$iIdPregunta.'">
							<input type ="hidden" name="vValor" value="'.$p->vValor.'">
							<input name="iLimiteMin" id="iLimiteMin" type="text" class="form-control" value="'.$p->iLimiteMin.'"placeholder="Ej: 0, 1-2" onblur="guardarRango('.$iIdPregunta.','.$p->vValor.');">
							<small>Puntaje: '.$p->vValor.' </small>
						</div>
					</div>
					</form>';
		}

		return $html;
	}


	public function html_opciones($iIdPregunta,$iIdCuestionario)
	{
		$html = '';
		$query = $this->mc->opciones($iIdPregunta);
		$listadoP = $this->mc->preguntasDropdown($iIdPregunta,$iIdCuestionario);
		$query = $query->result();
		$bandera = false;

		$tipo = 0;
		$html.= '<div class="row">
					<div class="col-md-12">';

		foreach ($query as $p)
		{
			$tipo = $p->iTipoPregunta;
			$html.= '';
			$PreguntaAnidada = '';
			
			if(count($listadoP) > 0){
			    
			    $PreguntaAnidada = '<div class"col-md-2"><label>¿Pregunta Anidada? </label></div><div class="col-md-6"><select class="form-control" name="iPreguntaHija" id="iPreguntaHija" onfocus="preguntaSelected(this)" onchange="guardaRelacion(this,'.$iIdCuestionario.','.$iIdPregunta.','.$p->iIdOpcion.')"><option value="0" >--Ninguna--</option>';
			    foreach($listadoP as $preg){
			        if($preg->iIdPregunta == $p->iIdPreguntaHijo){
			            $PreguntaAnidada .= '<option value="'.$preg->iIdPregunta.'" selected >'.$preg->vPregunta.'</option>';
			        }else{
			            $PreguntaAnidada .= '<option value="'.$preg->iIdPregunta.'" >'.$preg->vPregunta.'</option>';
			        }
			        
			    }
			    $PreguntaAnidada .= '</select></div>';
			}
			
			
			if($p->iTipoPregunta == 0)	// Opcion múltiple (4 radio)
			{
				$bandera = true;
				$html.= '<form name="form-op'.$p->iIdOpcion.'" id="form-op'.$p->iIdOpcion.'">
						<div class="form-group"><div class="row"><div class="col-md-4">
						<div class="custom-control custom-radio">
							<input type="hidden" name="iIdOpcion" id="iIdOpcion" value="'.$p->iIdOpcion.'">
		                    <input type="radio" class="custom-control-input">
		                    <label class="custom-control-label" for="customRadio1"><input type="text" class="form-control" name="vOpcion" id="vOpcion" value="'.$p->vOpcion.'" onblur="guardarTextoOpcion('.$p->iIdOpcion.');"></label>
							<i style="cursor:pointer;" class="fas fa-times" title="Eliminar opción" onclick="eliminarOpcion('.$p->iIdOpcion.','.$iIdPregunta.');"></i>
							</div></div>'.$PreguntaAnidada.'
		                </div></div>
		                </form>'; 
            }
			

            if($p->iTipoPregunta == 1)	// Dicotómica (2 radio)
			{
				$html.= '<form name="form-op'.$p->iIdOpcion.'" id="form-op'.$p->iIdOpcion.'">
						<div class="form-group"><div class="row"><div class="col-md-4">
						<div class="custom-control custom-radio">
							<input type="hidden" name="iIdOpcion" id="iIdOpcion" value="'.$p->iIdOpcion.'">
		                    <input type="radio" class="custom-control-input">
		                    <label class="custom-control-label" for="customRadio1"><input type="text" class="form-control" name="vOpcion" id="vOpcion" value="'.$p->vOpcion.'" onblur="guardarTextoOpcion('.$p->iIdOpcion.');"></label>
		                </div></div>'.$PreguntaAnidada.'
		                </div></div>
		                </form>'; 
            }

            if($p->iTipoPregunta == 2)	// Abierta (text)
			{
				$html.= '<form name="form-op'.$p->iIdOpcion.'" id="form-op'.$p->iIdOpcion.'">
						<div class="form-group">
						<div class="custom-control custom-radio">
							<input type="hidden" name="iIdOpcion" id="iIdOpcion" value="'.$p->iIdOpcion.'">
		                    <textarea class="form-control" name="" id="" disabled></textarea>
		                </div>
		                </div>
		                </form>'; 
            }

            if($p->iTipoPregunta == 3)	// Selección múltiple (check)
			{
				$checked = ($p->iOtro == 1) ? 'checked':'';
				$bandera = true;
            	$html.= '<form name="form-op'.$p->iIdOpcion.'" id="form-op'.$p->iIdOpcion.'">
            			<div class="form-group"><div class="row"><div class="col-md-4">
        				<div class="custom-control custom-checkbox">
        					<input type="hidden" name="iIdOpcion" id="iIdOpcion" value="'.$p->iIdOpcion.'">
                            <input type="checkbox" class="custom-control-input">
                            <label class="custom-control-label" for="customCheck3"><input type="text" class="form-control" name="vOpcion" id="vOpcion" value="'.$p->vOpcion.'" onblur="guardarTextoOpcion('.$p->iIdOpcion.');"></label> <i style="cursor:pointer;" class="fas fa-times" title="Eliminar opción" onclick="eliminarOpcion('.$p->iIdOpcion.','.$iIdPregunta.');"></i>
                            <br><small><input onchange="guardarOtro('.$p->iIdOpcion.');" type="checkbox" name="iOtro" '.$checked.'> Requiere un campo de texto</small>
                        </div></div>'.$PreguntaAnidada.'
                        </div></div>
                        </form>';
          	}

          	if($p->iTipoPregunta == 4)	// Priorización (campos de texto)
			{
				$bandera = true;
            	$html.= '<form name="form-op'.$p->iIdOpcion.'" id="form-op'.$p->iIdOpcion.'">
            			<div class="form-group"><div class="row"><div class="col-md-4">
        				<div class="custom-control custom-checkbox">
        					<input type="hidden" name="iIdOpcion" id="iIdOpcion" value="'.$p->iIdOpcion.'">
                            <input type="checkbox" class="custom-control-input">
                            <label class="custom-control-label" for="customCheck3"><input type="text" class="form-control" name="vOpcion" id="vOpcion" value="'.$p->vOpcion.'" onblur="guardarTextoOpcion('.$p->iIdOpcion.');"></label> <i style="cursor:pointer;" class="fas fa-times" title="Eliminar opción" onclick="eliminarOpcion('.$p->iIdOpcion.','.$iIdPregunta.');"></i>                            
                        </div></div>'.$PreguntaAnidada.'
                        </div></div>
                        </form>';
            }
        
		}


        if($bandera)
		{
			$bandera = false;
			$html.= '<div class="row">
                <div class="col-md-12"><button type="button" class="btn btn-success" onclick="agregarOpcion('.$iIdPregunta.');" ><i class="fas fa-plus"></i>&nbsp;Agregar opción</button></div>
    		</div> <br>';
    	}

		$html.= '</div>';

		/*if($tipo == 3)
		{
			$html.= '<div id="div-rangos'.$iIdPregunta.'" class="col-md-5">'.$this->html_rangos($iIdPregunta).'</div>';
		}*/

		$html.= '</div>';

		return $html;
	}

	public function cambiar_tipo_pregunta()
	{
		if(isset($_POST['iIdPregunta']) && !empty($_POST['iIdPregunta']))
		{
			$dTipo['iTipoPregunta'] = $iTipoPregunta = $this->input->post('iTipo');
			$datos = array('iActivo' => 0);
			$where = 'iIdPregunta = '.$this->input->post('iIdPregunta');

			$con = $this->mc->iniciar_transaccion();
			//	Actualizamos el tipo de pregunta
			$this->mc->actualizar_registro('iplan_preguntas',$where,$dTipo,$con);

			//Borramos todas las opciones
			$this->mc->actualizar_registro('iplan_opciones',$where,$datos,$con);

			//Activamos las respuestas en base al tipo
			if($iTipoPregunta == 0)
			{
				$datos['iActivo'] = 1;
				$where.= ' AND vValor IN(0,1,2,3)';
				$this->mc->actualizar_registro('iplan_opciones',$where,$datos,$con);
			} 

			if($iTipoPregunta == 1)
			{
				$datos['iActivo'] = 1;
				$where.= ' AND vValor IN(0,3)';
				$this->mc->actualizar_registro('iplan_opciones',$where,$datos,$con);
			}

			if($iTipoPregunta == 2)
			{
				$datos['iActivo'] = 1;
				$where.= ' AND vValor = 3';
				$this->mc->actualizar_registro('iplan_opciones',$where,$datos,$con);
			} 

			if($iTipoPregunta == 3)
			{
				$datos['iActivo'] = 1;
				$this->mc->actualizar_registro('iplan_opciones',$where,$datos,$con);
			} 

			if($iTipoPregunta == 4)
			{
				$datos['iActivo'] = 1;
				$this->mc->actualizar_registro('iplan_opciones',$where,$datos,$con);
			}

			if($this->mc->terminar_transaccion($con)) echo '1';
			else echo 'Los datos no puedieron actualizarse';
		}
	}

	public function guardar_otro()
	{
		if(isset($_POST['iIdOpcion']) && !empty($_POST['iIdOpcion']))
		{
			$datos['iOtro'] = (isset($_POST['iOtro'])) ? 1:0;
			$where['iIdOpcion'] = $this->input->post('iIdOpcion');

			$this->mc->actualizar_registro('iplan_opciones',$where,$datos);
		}
	}

	public function eliminar_cuestionario()
	{
		if(isset($_POST['id']) && !empty($_POST['id']))
		{
			
			$where['iIdCuestionario'] = $this->input->post('id');

			if($this->mc->desactivar_registro('iplan_cuestionarios',$where) > 0) echo '1';
			else echo 'El registro no pudo ser eliminado';
		}
	}

	public function eliminar_pregunta()
	{
		if(isset($_POST['iIdPregunta']) && !empty($_POST['iIdPregunta']))
		{
			
			$where['iIdPregunta'] = $this->input->post('iIdPregunta');

			if($this->mc->desactivar_registro('iplan_preguntas',$where) > 0) echo '1';
			else echo 'El registro no pudo ser eliminado';
		}
	}

	public function eliminar_opcion()
	{
		if(isset($_POST['iIdOpcion']) && !empty($_POST['iIdOpcion']))
		{			
			$where['iIdOpcion'] = $this->input->post('iIdOpcion');

			$con = $this->mc->iniciar_transaccion();

			// Eliminamos la opción del catálogo
			$this->mc->desactivar_registro('iplan_opciones',$where,$con);
			//	Eliminas de la configuración
			$this->mc->desactivar_registro('iplan_respuestas',$where,$con);
			if( $this->mc->terminar_transaccion($con) ) echo '1';
			else echo 'El registro no pudo ser eliminado';
		} else {echo 'noentro';}
	}

	public function carga_resp() 
	{
		$cuestid = $this->input->post('cuestid', TRUE);
		$centro = $this->input->post('centro', TRUE);
		$centro = $centro == "" ? [] : $centro;
		//log_message("error", "CENTROS: ".$centro);
		$publico = $this->input->post('check_publico', TRUE);
		$nacional = $this->input->post('check_nacional', TRUE);
		$empresarial = $this->input->post('check_empresarial', TRUE);
		

		$result = array();
		$cont = 0;
		$contop = 0;
		$preg_ant = 0;
		$band = 0;	
		$model = new M_cuestionario();
		$resp = $model->resp_dash($cuestid);
		if($resp!=false) {
			foreach ($resp as $vresp) {
				if($preg_ant != $vresp->iIdPregunta) {
					if($band > 0) {
						$cont++;
						$contop = 0;
					}
					else 
						$band = 1;

					$preg_ant = $vresp->iIdPregunta;
				}
				
				$total = $model->total_resp($vresp->iIdPregunta, $vresp->iIdOpcion, $publico, $nacional, $empresarial, $centro);
				if($total != false) 
					$total_op = $total[0]->total;
				else
					$total_op = 0;


				$result[$cont]['counter'] = $cont+1;
				$result[$cont]['preg_id'] = $vresp->iIdPregunta;
				$result[$cont]['preg'] = $vresp->vPregunta;
				$result[$cont]['tipo'] = $vresp->iTipoPregunta;
				$result[$cont]['op'][$contop]['opcounter'] = $contop+1;
				$result[$cont]['op'][$contop]['opid'] = $vresp->iIdOpcion;
				$result[$cont]['op'][$contop]['nomop'] = $vresp->vOpcion;
				$result[$cont]['op'][$contop]['total'] = $total_op;

				if($vresp->iTipoPregunta == 2){
					$resptable = $model->respuestastable($vresp->iIdPregunta, $vresp->iIdOpcion);
					$respuestasabiertas = array();
					
					if($resptable != false) {
						$contresp = 0;
						foreach ($resptable as $respt) {
						    
						    if($respt->vRespuesta != ''){
    							$respuestasabiertas[$contresp]['resp_id'] = $respt->iIdRespuesta;
    							$respuestasabiertas[$contresp]['respuesta'] = $respt->vRespuesta;
    
    							$contresp++;
						    }
						}
					}

					$result[$cont]['respuestas'] = $respuestasabiertas;
				}

				$contop++;
			}
		}

		echo json_encode($result);

	}

	public function carga_cuest() 
	{
	    $todos = $this->input->post('check_todos', TRUE);
		$publico = $this->input->post('check_publico', TRUE);
		$esnacional = $this->input->post('check_nacional', TRUE);
		$esempresarial = $this->input->post('check_empresarial', TRUE);
		$fromcuestionario = $this->input->post('fromC', TRUE);
		
	    $zonas = $this->input->post('zonasarray', TRUE);
		$centros = $this->input->post('centrosarray', TRUE);

		$cuest = $this->mc->carga_cuest($todos, $publico, $esnacional, $esempresarial, $zonas, $centros);

		if($cuest!=false) {
			echo '<option value="">-- Cuestionario --</option>';
			foreach ($cuest as $vcuest) {
			    if($fromcuestionario > 0 && $fromcuestionario == $vcuest->iIdCuestionario){
				    echo '<option value="'.$vcuest->iIdCuestionario.'">'.$vcuest->vCuestionario.'</option>';
			    }else if($fromcuestionario == 0) {
			        echo '<option value="'.$vcuest->iIdCuestionario.'">'.$vcuest->vCuestionario.'</option>';
			    }
			}
		}
	}

	public function notificar()
	{
		$cuestid = $this->input->post('cuestid', TRUE);
		$messageInitial = $this->input->post('message', TRUE);
		$resp = $this->input->post('resp', TRUE);
		$noresp = $this->input->post('noresp', TRUE);
		$cuest = $this->mc->datos_cuest($cuestid);
		
		$centros = $this->mc->carga_centrosbycuestionario($cuestid);

		$success = 0; 
		$error = 0;
		
		$centro = $cuest[0]->iTipo;
		$todos = $cuest[0]->iTodos;
		$soloNacional = $cuest[0]->bEsNacional;
		$soloEmpresarial = $cuest[0]->bEsEmpresarial;
		$nombrecuest = $cuest[0]->vCuestionario;
		
		$time = strtotime($cuest[0]->dVigencia);
        $newformat = date('d/m/Y',$time);
                
		$fechalimite = $newformat;

		if($todos > 0)
		{	
			$usuarios = $this->mc->carga_usuarios($centros, 0);
		}
		else if($soloNacional > 0)
		{	
			$usuarios = $this->mc->carga_usuarios(null, 1);
		}
		else if($soloNacional > 0 && $soloEmpresarial > 0){
		    $usuarios = $this->mc->carga_usuarios($centros, 2);
		}
		else if($soloEmpresarial > 0)
		{
			$usuarios = $this->mc->carga_usuarios($centros, 3);
		}
		else{
		    $usuarios = $this->mc->carga_usuarios(null, 0);
		}

		foreach ($usuarios as $vus) {
		    
		    //log_message('error', $correo);

			$correo = $vus->vCorreo;
			$nom_us = $vus->vNombre;
			$appat = $vus->vApPat;
			$apmat = $vus->vApMat;

			$resp = $this->mc->resp_us($vus->iIdUsuario, $cuestid);
			if(count($resp) == 0 && $noresp > 0) {
				$this->load->library('Class_mail');
				$mail = new Class_mail();

				$template = 'templates/notificacion.html';
				$mensaje = file_get_contents($template);
				$nombre = htmlentities($nom_us.' '.$appat.' '.$apmat, ENT_QUOTES, "UTF-8");
				$url = base_url();
				
				$mensaje = str_replace('{{BODY}}', $messageInitial, $mensaje);
				$mensaje = str_replace('{{var_nombre_dest}}', $nombre, $mensaje);
				$mensaje = str_replace('{{nombre_cuestionario}}', $nombrecuest, $mensaje);
				$mensaje = str_replace('{{fecha_limite}}', $fechalimite, $mensaje);
				$mensaje = str_replace('{{var_url}}', $url, $mensaje);
				
				$asunto = utf8_decode('Notificación');

                //log_message('error', $correo);
    			if($mail->enviar_correo($correo,$asunto,$mensaje)) $success++;
    			else $error++;
			}
			
			if(count($resp) > 0 && $resp > 0) {
				$this->load->library('Class_mail');
				$mail = new Class_mail();

				$template = 'templates/notificacion.html';
				$mensaje = file_get_contents($template);
				$nombre = htmlentities($nom_us.' '.$appat.' '.$apmat, ENT_QUOTES, "UTF-8");
				$url = base_url();
				
				$mensaje = str_replace('{{BODY}}', $messageInitial, $mensaje);
				$mensaje = str_replace('{{var_nombre_dest}}', $nombre, $mensaje);
				$mensaje = str_replace('{{nombre_cuestionario}}', $nombrecuest, $mensaje);
				$mensaje = str_replace('{{fecha_limite}}', $fechalimite, $mensaje);
				$mensaje = str_replace('{{var_url}}', $url, $mensaje);
				
				$asunto = utf8_decode('Notificación');

                //log_message('error', $correo);
    			if($mail->enviar_correo($correo,$asunto,$mensaje)) $success++;
    			else $error++;
			}
		}

		$envios = array('success' => $success, 'error' => $error);

		echo json_encode($envios);

	}
	
	public function centrosbyzona(){
	    $zonas = $this->input->post('zonasarray', TRUE);
		$zonasArray = explode(",", $zonas);
		
		$model = new M_cuestionario();
		$resp = $model->carga_centrosbyzonas($zonasArray);
		
		echo json_encode($resp);
	}
	
	public function cargarusuariosemail(){
	    
	    $cuestid = $this->input->post('id', TRUE);
		$cuest = $this->mc->datos_cuest($cuestid);

		log_message("error", "Cuest> ".$cuestid);
		$centros = $this->mc->carga_centrosbycuestionario($cuestid);
		
		$success = 0; 
		$error = 0;
		
		$centro = $cuest[0]->iTipo;
		$todos = $cuest[0]->iTodos;
		$soloNacional = $cuest[0]->bEsNacional;
		$soloEmpresarial = $cuest[0]->bEsEmpresarial;
		$nombrecuest = $cuest[0]->vCuestionario;
		
		$time = strtotime($cuest[0]->dVigencia);
        $newformat = date('d/m/Y',$time);
                
		$fechalimite = $newformat;


		if($todos > 0)
		{	
			$usuarios = $this->mc->carga_usuarios($centros, 0);
		}
		else if($soloNacional > 0)
		{	
			$usuarios = $this->mc->carga_usuarios(null, 1);
		}
		else if($soloNacional > 0 && $soloEmpresarial > 0){
		    $usuarios = $this->mc->carga_usuarios($centros, 2);
		}
		else if($soloEmpresarial > 0)
		{
			$usuarios = $this->mc->carga_usuarios($centros, 3);
		}
		else{
		    $usuarios = $this->mc->carga_usuarios(null, 0);
		}

        $answered = array();
        $noanswered=array();
        
		foreach ($usuarios as $vus) {
		    $usuarioss = array();
		    
			$usuarioss["vCorreo"] = $vus->vCorreo;
			$usuarioss["vNombre"]  = $vus->vNombre;

			$resp = $this->mc->resp_us($vus->iIdUsuario, $cuestid);
			if(count($resp) == 0) {
                array_push($noanswered, $usuarioss);
			}else{
			    array_push($answered, $usuarioss);
			}
		}

		$envios = array('personasanswer' => $answered, 'personasnoanswer' => $noanswered);

		echo json_encode($envios);
	}
	
	public function ExportExcel(){
		
		set_time_limit(300);
		
	    $todos = $this->input->post('check_todos', TRUE);
		$publico = $this->input->post('check_publico', TRUE);
		$esnacional = $this->input->post('check_nacional', TRUE);
		$esempresarial = $this->input->post('check_empresarial', TRUE);
		$fromcuestionario = $this->input->post('fromC', TRUE);
	    $zonas = $this->input->post('zonasarray', TRUE);
		$centros = $this->input->post('centrosarray', TRUE);
		
		//	log_message("error", "Todos: ".$todos);
		//		log_message("error", "EMP: ".$esempresarial);
		//log_message("error", $centros);
		
	    if($todos > 0)
		{	
		    if($centros != null && $centros != ""){
		        $usuarios = $this->mc->carga_usuariosreporte($centros, 0);
		    }
		    else{
		        $usuarios = $this->mc->carga_usuariosreporte(null, 0);
		    }
		
		}
		else if($esnacional > 0)
		{	
			$usuarios = $this->mc->carga_usuariosreporte(null, 1);
		}
		else if($esnacional > 0 && $esempresarial > 0){
		    $usuarios = $this->mc->carga_usuariosreporte($centros, 2);
		}
		else if($esempresarial > 0)
		{
			$usuarios = $this->mc->carga_usuariosreporte($centros, 3);
		}
		else{
		    $usuarios = $this->mc->carga_usuariosreporte(null, 0);
		}
		
		 $sheet = array();
		 $initial = array();
		 
		 //Initial Headers en EXCEL
		 	$initial["Correo"] = "Correo";
			$initial["Nombre"]  = "Nombre";
			$initial["Socio"] = "Socio";
			$initial["Centro"]  = "Centro";
			$initial["Completo"]  = "Cuestionario Completo";
			
		//END Initial HEADERS
		
		//PREGUNTAS HEADERS
		$preguntascuest = $this->mc->preguntas_cuestionario($fromcuestionario);
		if($preguntascuest != false){
		    foreach ($preguntascuest as $p) {
		        $initial[$p->vPregunta] = $p->vPregunta;
		    }
		}
		array_push($sheet, $initial);
		//END PREGUNTAS HEADERS
		
		foreach ($usuarios as $vus) {
		    $usuarioss = array();
		    
			$usuarioss["Correo"] = $vus->vCorreo;
			$usuarioss["Nombre"]  = $vus->vNombre;
			
			if($vus->iIdCentro == 0){
			    $usuarioss["Socio"] = "Nacional";
			    $usuarioss["Centro"] = "NA";
			}
			else{
			    $usuarioss["Socio"] = "Empresarial";
			    $usuarioss["Centro"] = $vus->Centro;
			}
			
			$resp = $this->mc->resp_us($vus->iIdUsuario, $fromcuestionario);
			if(count($resp) == 0) {
			    $usuarioss["Completo"] = "NO";
			}else{
			    $usuarioss["Completo"] = "SI";
			    
			}
			
			if($preguntascuest != false){
    		    foreach ($preguntascuest as $p) {
    		        $respusu = $this->mc->repuestas_usuario($p->iIdPregunta, $vus->iIdUsuario);
    		        $resptxt = "";
    		        foreach ($respusu as $r) {
    		            if(!strpos($p->vPregunta, 'centro empresarial')
    		            || (strpos($p->vPregunta, 'centro empresarial') && $vus->Centro !== 0 ))
    		            {
        		            if($resptxt != ""){
        		                $resptxt .= ", ";
        		            }
        		            $resptxt .= $r->iTipoPregunta == 2 ? $r->vRespuesta.' '.$r->vOpcion : $r->vOpcion;
    		            }
    		        }
    		        $usuarioss[$p->vPregunta] = $resptxt;
    		    }
    		}
			
			array_push($sheet, $usuarioss);
		}

	
		if($publico > 0){
			$totalpublic = $this->mc->total_anonimos($fromcuestionario);
			//$valueFor = (count($totalpublic) > 0 ? $totalpublic[0]->total : 0);
			
			foreach ($totalpublic as $pub) {
				$usuarioss = array();
				
				$usuarioss["Correo"] = "Usuario Publico";
				$usuarioss["Nombre"]  = "# Usuario: ".$pub->iIdAnonimo;
				$usuarioss["Socio"] = "NA";
				$usuarioss["Centro"] = "NA";
				$usuarioss["Completo"] = "SI";
				
				$previoustext = "";
				if($preguntascuest != false){
					foreach ($preguntascuest as $p) {
						$respusu = $this->mc->repuestas_anonimo($p->iIdPregunta, $pub->iIdAnonimo);
					
						$resptxt = "";
						foreach ($respusu as $r) {
							if(!strpos($p->vPregunta, 'centro empresarial')
							|| (strpos($p->vPregunta, 'centro empresarial') && !strpos($previoustext, 'Coparmex Nacional') ))
							{
								if($resptxt != ""){
									$resptxt .= ", ";
								}
								$resptxt .= $r->iTipoPregunta == 2 ? $r->vRespuesta : $r->vOpcion;
							}
						}
						
						$previoustext = $resptxt;
						$usuarioss[$p->vPregunta] = $resptxt;
					}
				}
				
				array_push($sheet, $usuarioss);
			}
		}

        
          $doc = new PHPExcel();
          $doc->setActiveSheetIndex(0);
          
          log_message("error", "AFTER PHP EX");
        
          $doc->getActiveSheet()->fromArray($sheet, null, 'A1');
            header('Content-Type: application/vnd.ms-excel. application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="file.xls"');
            header('Cache-Control: max-age=0');
        
          // Do your stuff here
          $writer = PHPExcel_IOFactory::createWriter($doc, 'Excel5');
          ob_start();
            $writer->save("php://output");
            $xlsData = ob_get_contents();
            ob_end_clean();
            
            $response =  array(
                       'op' => 'ok',
                       'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
                   );
            
            die(json_encode($response));
	}
}