<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_usuario extends CI_Controller {
	

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('M_usuario','mu');
		$this->load->model('M_cuestionario','mc');
		session_start();
	}

	public function existe_us() 
	{
		$op = $this->input->post('op', TRUE);
		$nom = $this->input->post('nom', TRUE);
		$ape = $this->input->post('ape', TRUE);
		$nom_us = $nom.'.'.$ape;

		$us = $this->mu->existe_usuario($nom_us);
		echo $us;
	}

	public function guardar()
	{		
		$nombre = $this->input->post('nombre', TRUE);
		$correo = $this->input->post('correo', TRUE);
		$contrasenia = $this->input->post('contrasenia', TRUE);
		$tipo_us = $this->input->post('sel_us', TRUE);
		$tipo = $this->input->post('sel_tipo');
		if($tipo==1) { $ent = $this->input->post('entidad', TRUE); $mun = ''; }
		elseif($tipo==2) { $mun = $this->input->post('municipio', TRUE); $ent = ''; }
		$centro = $this->input->post('sel_centro', TRUE);
		$zona = $this->input->post('sel_zona', TRUE);

		$nom_us = $this->input->post('nom_us', TRUE);
		$appat = $this->input->post('appat', TRUE);
		$apmat = $this->input->post('apmat', TRUE);
		$telefono = $this->input->post('telefono', TRUE);

		$otro_centro = $this->input->post('otro_centro', TRUE);


		$datos = array(
						'vNombreUsuario' => $nombre,
						'vContrasenia' => sha1($contrasenia),
						'vCorreo' => $correo,
						'iTipoUsuario' => $tipo_us,
						'iActivo' => 1,
						'iTipo' => 1,
						'vEntidad' => '',
						'vMunicipio' => '',
						'iIdCentro' => $centro,
						'iIdZona' => $zona,
						'vNombre' => $nom_us,
						'vApPat' => $appat,
						'vApMat' => $apmat,
						'vOtroCentro' => $otro_centro,
						'vTelefono' => $telefono);
		$model = new M_usuario();
		$resp = $model->inserta_usuario($datos);
		echo $resp;		

	}

	public function eliminar()
	{
		$usid = $this->input->post('usid', TRUE);
		$model = new M_usuario();		
		$resp = $model->elimina_usuario($usid);
		echo $resp;
	}

	public function modificar()
	{
		$usid = $this->input->post('usuarioid', TRUE);
		$nombre = $this->input->post('nombre', TRUE);
		$correo = $this->input->post('correo', TRUE);
		$sel_tipo = $this->input->post('sel_tipo', TRUE);
		$ent = $this->input->post('entidad', TRUE);
		$mun = $this->input->post('municipio', TRUE);
		$sel_us = $this->input->post('sel_us', TRUE);
		$centro = $this->input->post('sel_centro', TRUE);
		$zona = $this->input->post('sel_zona', TRUE);


		$otro_centro = $this->input->post('otro_centro', TRUE);
		$nom_us = $this->input->post('nom_us', TRUE);
		$appat = $this->input->post('appat', TRUE);
		$apmat = $this->input->post('apmat', TRUE);
		$telefono = $this->input->post('telefono', TRUE);

		$datos = array(
					'vNombreUsuario' => $nombre,
					'vCorreo' => $correo,
					'iTipoUsuario' => $sel_us,
					'iTipo' => $sel_tipo,
					'vEntidad' => $ent,
					'vMunicipio' => $mun,
					'iIdCentro' => $centro,
					'iIdZona' => $zona,
					'vNombre' => $nom_us,
					'vApPat' => $appat,
					'vApMat' => $apmat,
					'vTelefono' => $telefono,
					'vOtroCentro' => $otro_centro
				);

		$model = new M_usuario();
		$resp = $model->modifca_usuario($datos, $usid);
		echo $resp;
		//$this->load->view('form_usuarios',$datos);	
	}

	public function cambiar_pass()
	{
		$datos['usid'] = $this->input->post('usid', TRUE);
		$datos['nom'] = $this->input->post('nom', TRUE);
		$this->load->view('modal_pass',$datos);
	}

	public function guarda_ncont()
	{
		$usuarioid = $this->input->post('usuarioid', TRUE);
		$contra = $this->input->post('nueva_contra', TRUE);
		$rcontra = $this->input->post('repote_ncontra', TRUE);

		if(sha1($contra) == sha1($rcontra)) {
			$datos = array('vContrasenia' => sha1($contra));
							
			$model = new M_usuario();
			$resp = $model->guarda_ncont($datos, $usuarioid);
			echo $resp;	
		}
		else {
			echo 'error1';
		}

	}	


	//Funcion para leer el archivo Excel
    public function read_excel()
    {
        $response['estatus'] = false;
        $response['mensaje'] = '';
        $response['tipo'] = 'warning';
        $n = 0;
        $model = new M_usuario();

        $this->load->library('PHPExcel');

        $extensiones = array('xlsx');
        
        $file_name = $_FILES['file']['name'];
        $file_campos = explode(".", $file_name);
        $file_extension = strtolower(end($file_campos));
        if($file_name != '')
        {
            //valida que la extension del archivo subido corresponda a un excel
            if (in_array($file_extension, $extensiones)==TRUE)
            {

                
                $ruta = $_FILES['file']['tmp_name'];
                //obtiene la hoja del excel que sera validado
                //$hojaexcel = $this->obtener_hojaexcel($mes);
            
                $inputFileType = PHPExcel_IOFactory::identify($ruta);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($ruta);
                //$sheet = $objPHPExcel->getSheet(0); 
                $sheet = $objPHPExcel->getActiveSheet(); 
                //echo $objPHPExcel->getActiveSheet()->getTitle();
                $highestRow = $sheet->getHighestRow(); 
                $highestColumn = 'M';//$sheet->getHighestColumn();
                

                //Iniciamos la transaccion
                $con = $model->iniciar_transaccion();
                
                //log_message = 

                //recorre la hoja de excel desde el row 2
                for ($row = 2; $row <= $highestRow; $row++)
                {



                    $vNombre = $sheet->getCell("A".$row)->getValue();
                    $vApPat = $sheet->getCell("B".$row)->getValue();
                    $vApMat = $sheet->getCell("C".$row)->getValue();
                    //$vNombreUsuario = $sheet->getCell("D".$row)->getValue();
                    //$vContrasenia = $sheet->getCell("E".$row)->getValue();
                    $vCorreo = $sheet->getCell("D".$row)->getValue();
                    $vOtroCentro = $sheet->getCell("E".$row)->getValue();
                    //$iTipoUsuario = $sheet->getCell("H".$row)->getValue();
                    //$iTipo = $sheet->getCell("I".$row)->getValue();
                    //$vEntidad = $sheet->getCell("J".$row)->getValue();
                    //$vMunicipio = $sheet->getCell("K".$row)->getValue();
                    $vCentro = $sheet->getCell("G".$row)->getValue();
                    


					$data['vNombre'] = $vNombre;
					$data['vApPat'] = $vApPat;
					$data['vApMat'] = $vApMat;
					//$data['vNombreUsuario'] = $vNombreUsuario;
					//$data['vContrasenia'] = $vContrasenia;
					$data['vCorreo'] = $vCorreo;
					$data['iTipoUsuario'] = 3;
					$data['iActivo'] = 1;
					
					//get oIdCentro by Nombre
					$centrofound = $this->mc->getCentroByNombre(strtoupper($vCentro));
				//	log_message("error", "centrofound: ".count($centrofound));
					$iIdCentro = $vCentro != "" && count($centrofound) > 0 ? $centrofound[0]->iIdCentro : 0;
					$iIdZona = $vCentro != "" && count($centrofound) > 0 ? $centrofound[0]->iIdCategoria : 0;
				//	log_message("error", "centroid: ".$iIdCentro);
					$data['iIdCentro'] = $iIdCentro;
					$data['iIdZona'] = $iIdZona;
                   

                    $table = 'iplan_usuarios';                            
                    
                    $result  = $model->inserta_nusuarios($table, $data, $con);
                    $n++;
                    
                }

                 // Finalizar transaccion
                if ($model->terminar_transaccion($con) == true)
                {
                    $response['estatus'] = true;
                    $response['tipo'] = 'success';
                    $response['mensaje'] = "Se ha importado un total $n de registro(s)";
                }
                else
                {
                    $response['tipo'] = 'error';
                    $response['mensaje'] = 'Ha ocurrido un error al intentar guardar los datos';                
                }
                

            }
            else
            {
                $response['mensaje'] = 'El formato del archivo no es válido';
            } 
        }
        else
        {
            $response['mensaje'] = 'Debe subir un archivo válido';
        }
        
        echo json_encode($response);
    }

    public function registra_usuario()
	{
		//	Datos del captcha
		
    	$secret = '6Ld5O-MUAAAAAJHww-U391PZUrxLo5DU8vxyM0rN';
    	$response = $_POST["g-recaptcha-response"];
    	$remoteip =  $_SERVER['REMOTE_ADDR'];

    	$url = 'https://www.google.com/recaptcha/api/siteverify';

    	$captcha = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response&remoteip=$remoteip");
		$json = json_decode($captcha);

		if($json->success)
    	{

			$nom_us = $this->input->post('nom_us', TRUE);
			$appat = $this->input->post('appat', TRUE);
			$apmat = $this->input->post('apmat', TRUE);
			$telefono = " ";
			$nombre = $this->input->post('nombre', TRUE);
			$correo = $this->input->post('correo', TRUE);
			$contrasenia = $this->input->post('contrasenia', TRUE);
			$contrasenia2 = $this->input->post('contrasenia2', TRUE);
			$tipo_us = $this->input->post('sel_us', TRUE);
			$centro = $this->input->post('sel_centro', TRUE);
			$zona = $this->input->post('sel_zona', TRUE);
			$otro_centro = $this->input->post('otro_centro', TRUE);

			
			$us = $this->mu->existe_usuario($nombre, 0);
			$mail = $this->mu->existe_usuario($correo, 1);
			if($us == 0 && $mail == 0) {
				$var = rand(100000, 999999);
				$token = md5($var);

				$datos = array(
							'vNombreUsuario' => $nombre,
							'vContrasenia' => sha1($contrasenia),
							'vCorreo' => $correo,
							'iTipoUsuario' => $tipo_us,
							'iTipo' => 1,
							'vEntidad' => 'Yucatán',
							'vMunicipio' => '',
							'iIdCentro' => $centro,
							'iIdZona' => $zona,
							'iActivo' => 0,
							'vNombre' => $nom_us,
							'vApPat' => $appat,
							'vApMat' => $apmat,
							'vTelefono' => $telefono,
							'vToken' => $token,
							'vOtroCentro' => $otro_centro);

				if(sha1($contrasenia) == sha1($contrasenia2))
				{
					$model = new M_usuario();
					$resp = $model->registro_usuario($datos);
					if($resp > 0) {

						$this->load->library('Class_mail');
						$mail = new Class_mail();

						$template = 'templates/confirmar_correo.html';
						$mensaje = file_get_contents($template);
						$nombre = htmlentities($nom_us.' '.$appat.' '.$apmat, ENT_QUOTES, "UTF-8");
						$url = base_url().'C_usuario/confirmar_correo?id='.$resp.'&token='.$token;
						$mensaje = str_replace('{{var_nombre_dest}}', $nombre, $mensaje);
						$mensaje = str_replace('{{var_url}}', $url, $mensaje);
						
						$asunto = utf8_decode('Confirmación de correo');

		    			if($mail->enviar_correo($correo,$asunto,$mensaje)) echo 'success';		    			
		    			else echo 'error4';
					}
					else echo 'error2';
					
				}
				else echo 'error1';

			}
			else {
				if($us > 0 && $mail == 0) echo 'error5';
				elseif($us == 0 && $mail > 0) echo 'error6';
				else echo 'error7';

			}
			
		} 
		else echo 'error3';
		
	}

	public function confirmar_correo()
	{
		if( isset($_GET['id']) && !empty($_GET['id']) && isset($_GET['token']) && !empty($_GET['token']) )
		{

			$idusuario = $this->input->get('id');
			$token = $this->input->get('token');

			$model = new M_usuario();
			if($model->consultar_usuario_por_token($idusuario,$token))
			{
				$datos = array('iActivo' => 1);
				$result = $model->modifca_usuario($datos, $idusuario);
				if($result!=false) {
					$html = '<p>¡Correo confirmado! Redireccionando...</p>
								<script>
									setTimeout(function(){ window.location.href = "'.base_url().'" }, 3000);
								</script>';
					echo $html;
				}
			}
			else echo 'No se encontró el usuario o ha sido bloquedado por un administrador';
		}
		else
		{
			echo 'Accedo denegado';
		}
	}
	
}
