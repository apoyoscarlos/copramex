<?php

class M_cuestionario extends CI_Model {


	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	public function carga_preguntas($op=0,$id=0,$cuestid=0)
	{
		if($op==0)
		{
			if($cuestid > 0) 
			{
				$this->db->select('p.iIdCuestionario');
				$this->db->where('p.iIdCuestionario',$cuestid);				
			}

			$this->db->select('p.iIdPregunta, p.vPregunta, p.iPonderacion,p.iEvidencia, p.vLink, p.iTipoPregunta, p.iObligatorio, o.iIdOpcion, o.vOpcion, o.iOtro, r.iIdRespuesta, o.iActivo as iIdPreguntaPadre, o.iActivo as OpcionPadre');
			$this->db->from('iplan_preguntas p');
			$this->db->join('iplan_respuestas r','p.iIdPregunta = r.iIdPregunta and r.iActivo = 1', 'INNER');
			$this->db->join('iplan_opciones o','r.iIdOpcion = o.iIdOpcion and o.iActivo = 1', 'INNER');
			$this->db->where('p.iActivo',1);
			$this->db->where('p.iIdPregunta NOT IN (Select pm.iIdPreguntaHijo FROM iplan_pregunta_matriz pm)');
			$this->db->order_by('p.iOrden', 'ASC');
			//$this->db->order_by('p.iIdPregunta', 'ASC');
			$this->db->order_by('o.iIdOpcion', 'ASC');

		}
		elseif($op==1)
		{
			$this->db->select('iIdPregunta, vPregunta, iPonderacion, iEvidencia, iTipoPregunta, vLink');
			$this->db->from('iplan_preguntas');
			$this->db->where('iActivo',1);
			if($id>0) $this->db->where('iIdPregunta',$id);
			$this->db->order_by('iIdPregunta', 'ASC');
		}

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}	
	
	public function carga_pregunta_hijo($opcion)
	{

			$this->db->select('p.iIdPregunta, p.vPregunta, p.iPonderacion,p.iEvidencia, p.vLink, p.iTipoPregunta ,o.iIdOpcion, o.vOpcion, o.iOtro, r.iIdRespuesta, pm.iIdPreguntaPadre, pm.iIdPreguntaOpcion as OpcionPadre, p.iObligatorio');
			$this->db->from('iplan_preguntas p');
			$this->db->join('iplan_respuestas r','p.iIdPregunta = r.iIdPregunta and r.iActivo = 1', 'INNER');
			$this->db->join('iplan_opciones o','r.iIdOpcion = o.iIdOpcion and o.iActivo = 1', 'INNER');
			$this->db->join('iplan_pregunta_matriz pm','p.iIdPregunta = pm.iIdPreguntaHijo', 'INNER');
			$this->db->where('p.iActivo',1);
			$this->db->where('pm.iIdPreguntaOpcion',$opcion);
			//$this->db->where('p.iIdPregunta NOT IN (Select pm.iIdPreguntaHijo FROM iplan_pregunta_matriz pm WHERE pm.iIdPreguntaOpcion = '.$opcion.')');
			$this->db->order_by('p.iIdPregunta', 'ASC');
			$this->db->order_by('o.iIdOpcion', 'ASC');


		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}	

	public function elimina_preg($pregid)
	{
		$this->db->set('iActivo',0);
		$this->db->where('iIdPregunta',$pregid);
		$query = $this->db->update('iplan_preguntas');
		return $query;
	}
	
	public function guarda_pregunta_matriz($datos)
	{
		return $this->db->insert('iplan_pregunta_matriz',$datos);
	}

	public function guarda_respuestas($datos)
	{
		return $this->db->insert('iplan_resp_usuario',$datos);
	}
	
	public function guarda_cuestionariohistorial($datos)
	{
		return $this->db->insert('iplan_cuest_usuario',$datos);
	}
	
	public function guarda_zona($datos, $id, $centros)
	{
	
	    log_message('error', $id); 
		if($id > 0) 
		{ 
			$this->db->where('iIdCategoria',$id);
			$query = $this->db->update('iplan_categorias', $datos);
		}
		else 
		{
		   $this->db->insert('iplan_categorias',$datos);
		   $id = $this->db->insert_id(); 
		   //log_message('error', 'INSERT: '.$id); 
		}
		
		$counterSaving = 1;
		
		if($id > 0){
		    //log_message('error', $centros); 
		    
    		//Remove zonas from previous centros selected

                $this->db->where('iIdCategoria',$id);
                $datosc = array('iIdCategoria' => NULL, 'iActivo' => 1);
                $query = $this->db->update('iplan_centro_esp', $datosc);
    		
    			//Add zonas to centros selected
        		for ($j = 0; $j < count($centros); $j++) {
                    $centroid = $centros[$j];
                    $this->db->where('iIdCentro',$centroid);
                    $datos = array('iIdCategoria' => $id, 'iActivo' => 1);
                    $query = $this->db->update('iplan_centro_esp', $datos);
                    $counterSaving++;
        		}
    		
		}
		
		return $counterSaving;
	}

	public function guarda_calif($datos)
	{
		return $this->db->insert('iplan_calif',$datos);
	}

	public function rangos_respuestas($iIdPregunta)
	{
		$this->db->select('ra.iLimiteMin,ra.vValor');
		$this->db->from('iplan_rangos ra');
		$this->db->where('ra.iIdPregunta',$iIdPregunta);
		$this->db->order_by('ra.iLimiteMin','ASC');

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	public function guarda_pregunta($datos)
	{
		$this->db->insert('iplan_preguntas', $datos);
		$query = $this->db->insert_id();
		return $query;
	}

	public function guarda_resp($datos_r)
	{
		return $this->db->insert('iplan_respuestas', $datos_r);
	}

	public function resp_usuario($usid, $cuestid)
	{
		/*$this->db->select('iIdRespuesta,vRespuesta,vArchivo');
		$this->db->from('iplan_resp_usuario');
		$this->db->where('iIdUsuario',$usid);*/

		$this->db->select('re.iIdRespuesta,re.vRespuesta,re.vArchivo');
		$this->db->from('iplan_resp_usuario re');
		$this->db->join('iplan_respuestas r','re.iIdRespuesta = r.iIdRespuesta','INNER');
		$this->db->join('iplan_preguntas p','r.iIdPregunta = p.iIdPregunta','INNER');
		$this->db->where('re.iIdUsuario',$usid);
		$this->db->where('p.iIdCuestionario',$cuestid);

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	public function act_resp($resp,$usid,$respid,$uiD)
	{
		$this->db->set('vRespuesta', $resp);
		if($usid > 0){
		    $this->db->where('iIdUsuario',$usid);
		}else{
		    $this->db->where('iIdAnonimo',$uiD);
		}
		$this->db->where('iIdRespuesta',$respid);
		$query = $this->db->update('iplan_resp_usuario');
		return $query;
	}

	public function carga_resp($op=0)
	{
		$this->db->select('iIdOpcion, vOpcion, iTipoR, iOtro');
		$this->db->from('iplan_opciones');
		$this->db->where('iActivo',1);
		if($op >= 0) $this->db->where('iTipoR',$op);

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	public function carga_respuestas($pregid = 0)
	{
		$this->db->select('r.iIdPregunta, o.iIdOpcion, o.vOpcion');
		$this->db->from('iplan_respuestas r');
		$this->db->join('iplan_opciones o','r.iIdOpcion = o.iIdOpcion and o.iActivo = 1','INNER');
		if($pregid > 0) $this->db->where('r.iIdPregunta',$pregid);		

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	public function actualiza_pregunta($pregid,$datos)
	{
		$this->db->where('iIdPregunta',$pregid);
		$act = $this->db->update('iplan_preguntas',$datos);

		if($act==1) 
		{
			$resp = $this->reinicia_preguntas($pregid);
			return $resp;
		}
	}

	private function reinicia_preguntas($pregid)
	{
		$this->db->set('iActivo',0);
		$this->db->where('iIdPregunta',$pregid);
		$query = $this->db->update('iplan_respuestas');
		return $query;
	}

	public function elimina_resp($respid)
	{
		$this->db->set('iActivo',0);
		$this->db->where('iIdOpcion',$respid);
		$query = $this->db->update('iplan_opciones');
		return $query;
	}

	public function elimina_cent($cuestid)
	{
		$this->db->set('iActivo',0);
		$this->db->where('iIdCentro',$cuestid);
		$query = $this->db->update('iplan_centro_esp');
		return $query;
	}
	
	public function elimina_zona($cuestid)
	{
		$this->db->set('iActivo',0);
		$this->db->where('iIdCategoria',$cuestid);
		$query = $this->db->update('iplan_categorias');
		return $query;
	}

	public function guarda_opcion($datos,$respid)
	{
		if($respid > 0) 
		{ 
			$this->db->where('iIdOpcion',$respid);
			$query = $this->db->update('iplan_opciones', $datos);
			return $query;
		}
		else return $this->db->insert('iplan_opciones', $datos);
	}

	public function guardar_centro($datos,$cuestid)
	{
		if($cuestid > 0) 
		{ 
			$this->db->where('iIdCentro',$cuestid);
			$query = $this->db->update('iplan_centro_esp', $datos);
			return $query;
		}
		else return $this->db->insert('iplan_centro_esp', $datos);
	}

	public function cuest_usuario()
	{
		$this->db->select('u.iIdUsuario, u.vNombreUsuario, u.vCorreo, vEntidad, vMunicipio, iTipo');
		$this->db->from('iplan_usuarios u');
		$this->db->where('u.iActivo',1);
		$this->db->where('u.iTipoUsuario',3);

		/*$this->db->select('u.iIdUsuario, u.vNombreUsuario, u.vCorreo, vEntidad, vMunicipio, iTipo, ( select r.iIdUsuario from iplan_resp_usuario r where r.iIdUsuario = u.iIdUsuario group by r.iIdUsuario ) as resp, ( select SUM(c.vCalificacion) from iplan_calif c where c.iIdUsuario = u.iIdUsuario group by c.iIdUsuario ) as calif');
		$this->db->from('iplan_usuarios u');
		$this->db->where('u.iActivo',1);
		$this->db->where('u.iTipoUsuario',3);*/

		/*
		$this->db->select('c.iIdUsuario,SUM(c.vCalificacion),p.iIdPregunta, p.iIdCuestionario, u.iIdUsuario, u.vNombreUsuario, u.vCorreo, u.vEntidad, u.vMunicipio, u.iTipo');
		$this->db->from('iplan_usuarios u');
		$this->db->join('iplan_calif c','c.iIdUsuario = u.iIdUsuario','LEFT');
		$this->db->join('iplan_preguntas p','c.iIdPregunta = p.iIdPregunta','LEFT');
		$this->db->group_by('c.iIdUsuario, p.iIdCuestionario'); 
		*/

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}
	
	public function resp_us($usid, $cuestionarioId)
	{
		$this->db->select('r.iIdUsuario, r.dFechaEnvio'); 
		$this->db->from('iplan_cuest_usuario r');
		$this->db->where('r.iIdUsuario',$usid);
		$this->db->where('r.iIdCuestionario',$cuestionarioId);
		$this->db->group_by('r.iIdUsuario ');

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	public function resp_usold($usid)
	{
		$this->db->select('r.iIdUsuario'); 
		$this->db->from('iplan_resp_usuario r');
		$this->db->where('r.iIdUsuario',$usid);
		$this->db->group_by('r.iIdUsuario ');

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	public function calif_us($usid)
	{
		$this->db->select('SUM(c.vCalificacion) as calif, p.iIdCuestionario, cu.vCuestionario');
		$this->db->from('iplan_calif c') ;
		$this->db->join('iplan_preguntas p', 'c.iIdPregunta = p.iIdPregunta', 'INNER');
		$this->db->join('iplan_cuestionarios cu', 'p.iIdCuestionario = cu.iIdCuestionario', 'INNER');
		$this->db->where('c.iIdUsuario', $usid);
		$this->db->group_by('c.iIdUsuario, p.iIdCuestionario');

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	public function obtener_resp($pregid, $usid, $op=0)
	{
		if($op==0) $this->db->select('ru.iIdRespuesta');
		else $this->db->select('ru.iIdRespuesta, ru.vRespuesta, ru.iIdUsuario, ru.iCalificacion, r.iIdRespuesta,r.iIdPregunta,r.iIdOpcion,ru.vArchivo');

		$this->db->from('iplan_resp_usuario ru');
		$this->db->join('iplan_respuestas r ','ru.iIdRespuesta = r.iIdRespuesta','LEFT');
		$this->db->where('ru.iIdUsuario',$usid);
		$this->db->where('r.iIdPregunta',$pregid);

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	public function valor_opcion($op)
	{
		$this->db->select('r.iIdPregunta as iIdPregunta,r.iIdOpcion as iIdOpcion,o.vOpcion as vOpcion,o.vValor as vValor');
		$this->db->from('iplan_respuestas r');
		$this->db->join('iplan_opciones o','r.iIdOpcion = o.iIdOpcion','INNER');
		$this->db->where('r.iIdRespuesta', $op);

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	public function act_calif($respid, $usid, $datos)
	{
		$this->db->where('iIdRespuesta',$respid);
		$this->db->where('iIdUsuario',$usid);

		$query = $this->db->update('iplan_resp_usuario',$datos);
		return $query;
	}

	public function obtener_calif($pregid,$usid)
	{	
		$this->db->select('vCalificacion');
		$this->db->from('iplan_calif');
		$this->db->where('iIdUsuario',$usid);
		$this->db->where('iIdPregunta',$pregid);
		$this->db->group_by('iIdPregunta');
		
		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	public function inserta_archivo($datos,$respid,$usid)
	{
		$this->db->where('iIdRespuesta',$respid);
		$this->db->where('iIdUsuario',$usid);
		$query = $this->db->update('iplan_resp_usuario',$datos);
		return $query;
	}	

	/*	Funciones para usar transacciones
	======================================
	*/
	public function iniciar_transaccion()
	{
	  $con = $this->load->database('default',TRUE);
	  $con->trans_begin();
	  return  $con;
	}

	public function terminar_transaccion($con)
	{
		if ($con->trans_status() === FALSE)
		{
			$con->trans_rollback();
			return false;
		}
		else 
		{
			$con->trans_commit();
			return true;
		}
	}

	public function insertar_registro($tabla,$datos,$con='')
	{
		if($con == '') $con = $this->db;

		if($con->insert($tabla,$datos)) return $con->insert_id();
		else return false;
	}

	public function insertar_registro_no_pk($tabla,$datos,$con='')
	{
		if($con == '') $con = $this->db;

		if($con->insert($tabla,$datos)) return true;
		else return false;
	}

	public function actualizar_registro($tabla,$where,$datos,$con='')
	{
		if($con == '') $con = $this->db;
		$con->where($where);
		return $con->update($tabla, $datos);
	}
	
	public function guardar_filtros_zona($data){
	    $this->db->insert_batch('iplan_filtro_zonas', $data); 
	}
	
	public function delete_filtros_zona($idCuestionario){
	    $this->db->where('iIdCuestionario', $idCuestionario);
	    return $this->db->delete('iplan_filtro_zonas');
	}
	
	public function guardar_filtros_centros($data){
	    $this->db->insert_batch('iplan_filtro_centros', $data); 
	} 
	
	public function delete_filtros_centros($idCuestionario){
	   
	   //log_message('error', 'CUEST'.$idCuestionario);
	   	$this->db->where('iIdCuestionario', $idCuestionario);
	    return $this->db->delete('iplan_filtro_centros');
	    
	}
	
	public function delete_pregunta_matriz($idCuestionario, $idPregunta, $idOpcion){
	   
	   //log_message('error', 'CUEST'.$idCuestionario);
	   	$this->db->where('iIdCuestionario', $idCuestionario);
	   	$this->db->where('iIdPreguntaPadre', $idPregunta);
	   	$this->db->where('iIdPreguntaOpcion', $idOpcion);
	    return $this->db->delete('iplan_pregunta_matriz');
	    
	}

	public function eliminar_registro($tabla,$where,$con)
	{
		return $con->delete($tabla,$where);
	}

	public function desactivar_registro($tabla,$where,$con='')
	{
		if($con == '') $con = $this->db;
		$con->where($where);
		return $con->update($tabla, array('iActivo' => 0));

		return ($con->affected_rows() > 0);
	}

	public function activar_registro($tabla,$where,$con='')
	{
		if($con == '') $con = $this->db;
		$con->where($where);
		$con->update($tabla, array('iActivo' => 1));

		return ($con->affected_rows() > 0);
	}

	public function carga_cuestionarios($tipo_proced, $userid = 0, $nacional = 0, $empresarial = 0, $centro = 0, $fini = '')
	{	
		$this->db->select('c.iIdCuestionario,c.vCuestionario,c.vDescripcion, c.dVigencia,c.dFechaCreacion,c.vAvisoPriv,c.bEsNacional,c.bEsEmpresarial,c.bEsPublico');
		$this->db->from('iplan_cuestionarios c');
		if($userid > 0) { 
		    $this->db->join('iplan_filtro_centros fc', 'c.iIdCuestionario = fc.iIdCuestionario', 'LEFT');
		}
		$this->db->where('c.iActivo',1);

        if($userid > 0) { 
			if($nacional > 0){
			     $this->db->where('c.bEsNacional',1);
			}
			if($empresarial > 0){
			     $this->db->where('((c.bEsEmpresarial = 1 AND fc.iIdCentro = '.$centro.') OR (c.bEsEmpresarial = 1 AND fc.iIdCentro = 0))');
			}
		}
		else{
		    $this->db->where('c.bEsPublico',1);
		}

        if($fini != '')
		{
			$this->db->where('c.dFechaCreacion <=',$fini);
		}

		$query = $this->db->get();
		
		log_message("error",$this->db->last_query());
		
		if($query!=false) return $query->result();
		else return false;
	}

	public function preguntas_cuestionario($cuestid)
	{
		$this->db->select('iIdPregunta, vPregunta, iPonderacion, iEvidencia, iTipoPregunta, iActivo, iNumero, vLink, iOrden');
		$this->db->from('iplan_preguntas');
		$this->db->where('iIdCuestionario', $cuestid);
		$this->db->where('iActivo', 1);

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	public function preguntas_hijascuestionario($cuestid)
	{
		$this->db->select();
		$this->db->from('iplan_pregunta_matriz');
		$this->db->where('iIdCuestionario', $cuestid);

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	/*	Funciones para usar transacciones
	======================================
	*/

	public function cuestionarios($id=0, $fini = '', $ffin = '')
	{
		$this->db->select('c.iIdCuestionario, c.vCuestionario, c.vDescripcion, c.vInfoGeneral, vLinkFinal, vTextFinal, c.iTipo, c.iTodos, ce.vNombre, c.vAvisoPriv, c.dVigencia,c.dFechaCreacion,c.bEsPublico,c.bEsNacional,c.bEsEmpresarial');
		$this->db->from('iplan_cuestionarios c');
		$this->db->join('iplan_centro_esp ce', 'c.iTipo = ce.iIdCentro', 'LEFT');
		$this->db->where('c.iActivo',1);
		if($id > 0) $this->db->where('c.iIdCuestionario',$id);
		if($fini != '' && $ffin != '')
		{
			$this->db->where('c.dVigencia >=',$fini);
			$this->db->where('c.dVigencia <=',$ffin);
		}
		return $this->db->get();
	}

	public function preguntas($iIdCuestionario)
	{
		$this->db->select('iIdPregunta');
		$this->db->from('iplan_preguntas');
		$this->db->where('iActivo',1);
		$this->db->where('iIdCuestionario',$iIdCuestionario);
		
		return $this->db->get();
	}

	public function campos_tabla_cuestionario()
	{
		$sql = "SHOW COLUMNS FROM iplan_cuestionarios FROM {$this->db->database};";
		return $this->db->query($sql); 
	}

	public function pregunta($iIdPregunta)
	{
		$this->db->select('iIdPregunta, vPregunta, iEvidencia, iTipoPregunta, iNumero, vLink, iOrden, iObligatorio');
		$this->db->from('iplan_preguntas');
		$this->db->where('iActivo',1);
		$this->db->where('iIdPregunta',$iIdPregunta);
		
		return $this->db->get()->row();
	}
	
	public function preguntasDropdown($iIdPregunta, $iIdCuestionario)
	{
		$this->db->select('p.iIdPregunta, p.vPregunta');
		$this->db->from('iplan_preguntas p');
		//$this->db->join('iplan_pregunta_matriz pm','p.iIdPregunta = pm.iIdPreguntaHijo','LEFT');
		$this->db->where('p.iActivo',1);
		$this->db->where('p.iIdCuestionario',$iIdCuestionario);
		$this->db->where('p.iIdPregunta <>',$iIdPregunta);
		//$this->db->where('pm.iIdPreguntaHijo <> '.$iIdPregunta.'');
		
		$query = $this->db->get();
		
		log_message("error",$this->db->last_query());
		
		if($query!=false) return $query->result();
		else return false;
	}
	
	public function verificarPregunta($iIdPreguntaPadre, $iIdPreguntaHijo)
	{
		$this->db->select('m.iIdPreguntaPadre, m.iIdPreguntaHijo');
		$this->db->from('iplan_pregunta_matriz m');
		$this->db->where('m.iIdPreguntaHijo', $iIdPreguntaHijo);
		$this->db->where('m.iIdPreguntaPadre', $iIdPreguntaPadre);

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	public function opciones($iIdPregunta)
	{
		$this->db->select('o.iIdOpcion, o.vOpcion, o.iTipoR, o.vValor, p.iTipoPregunta, o.iOtro, pm.iIdPreguntaHijo');
		$this->db->from('iplan_opciones o');
		$this->db->join('iplan_preguntas p','p.iIdPregunta = o.iIdPregunta','INNER');
		$this->db->join('iplan_pregunta_matriz pm','o.iIdOpcion = pm.iIdPreguntaOpcion','LEFT');
		$this->db->where('o.iIdPregunta',$iIdPregunta);
		$this->db->where('o.iActivo',1);

		$this->db->order_by('o.iIdOpcion');

		return $this->db->get();
	}

	public function rangos($iIdPregunta)
	{
		$this->db->select('o.vValor, o.iLimiteMin, o.iLimiteMax');
		$this->db->from('iplan_rangos o');
		$this->db->where('o.iIdPregunta',$iIdPregunta);

		$this->db->order_by('o.vValor');

		return $this->db->get();
	}

	public function resp_dash($cuestid) {	
	    
	    //$this->db->distinct();
		$this->db->select('preg.iIdPregunta, preg.vPregunta, preg.iTipoPregunta, preg.iObligatorio, op.iIdOpcion, op.vOpcion');
		$this->db->from('iplan_cuestionarios c');
		$this->db->join('iplan_preguntas preg', 'c.iIdCuestionario = preg.iIdCuestionario', 'INNER');
		$this->db->join('iplan_opciones op', 'ON preg.iIdPregunta = op.iIdPregunta and op.iActivo = 1', 'INNER');
		$this->db->where('preg.iIdCuestionario', $cuestid);
		$this->db->order_by('preg.iIdPregunta', 'ASC');
        $this->db->order_by('op.iIdOpcion', 'ASC');
		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	
	}
	
	public function respuestastable($pregid, $opid) {
		
		$this->db->select('ru.iIdRespuesta, ru.vRespuesta');
		$this->db->from('iplan_resp_usuario ru');
		$this->db->join('iplan_respuestas r', 'ru.iIdRespuesta = r.iIdRespuesta','INNER');
		$this->db->where('r.iIdPregunta', $pregid);
		$this->db->where('r.iActivo', 1);
		$this->db->where('r.iIdOpcion', $opid);

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}
	
	public function total_resp_cuest($cuest){
	    
	    $this->db->select('COUNT(ru.iIdAnonimo) AS total');
		$this->db->from('iplan_cuest_usuario ru');
		
		$this->db->where('ru.iIdCuestionario', $cuest);
		$this->db->where('ru.iIdUsuario', 0);
		$this->db->where('ru.iIdAnonimo <> ""');
		//$this->db->group_by('ru.iIdAnonimo');

		$query = $this->db->get();
		
		log_message("error",$this->db->last_query());
		
		if($query!=false) return $query->result();
		else return false;
	}
	
	public function total_anonimos($cuest){
	    
	    $this->db->select('ru.iIdAnonimo');
		$this->db->from('iplan_cuest_usuario ru');
		
		$this->db->where('ru.iIdCuestionario', $cuest);
		$this->db->where('ru.iIdUsuario', 0);
		$this->db->where('ru.iIdAnonimo <> ""');
		//$this->db->group_by('ru.iIdAnonimo');

		$query = $this->db->get();
		
		//log_message("error",$this->db->last_query());
		
		if($query!=false) return $query->result();
		else return false;
	}
	
	public function repuestas_anonimo($pregid, $usuarioid) {
		
		$this->db->select('ru.vRespuesta, o.vOpcion, p.iTipoPregunta');
		$this->db->from('iplan_resp_usuario ru');
		$this->db->join('iplan_respuestas r', 'ru.iIdRespuesta = r.iIdRespuesta','INNER');
		$this->db->join('iplan_opciones o', 'r.iIdOpcion = o.iIdOpcion','INNER');
	    $this->db->join('iplan_preguntas p', 'r.iIdPregunta = p.iIdPregunta','INNER');
		
		$this->db->where('r.iIdPregunta', $pregid);
		$this->db->where('ru.iIdAnonimo', $usuarioid);
		
		$query = $this->db->get();
		
		log_message("error",$this->db->last_query());
		
		if($query!=false) return $query->result();
		else return false;
	}
	
	public function repuestas_usuario($pregid, $usuarioid) {
		
		$this->db->select('ru.vRespuesta, o.vOpcion, p.iTipoPregunta, p.vPregunta, p.iObligatorio');
		$this->db->from('iplan_resp_usuario ru');
		$this->db->join('iplan_respuestas r', 'ru.iIdRespuesta = r.iIdRespuesta','INNER');
		$this->db->join('iplan_opciones o', 'r.iIdOpcion = o.iIdOpcion','INNER');
		$this->db->join('iplan_preguntas p', 'r.iIdPregunta = p.iIdPregunta','INNER');
		
		$this->db->where('r.iIdPregunta', $pregid);
		$this->db->where('ru.iIdUsuario', $usuarioid);
		
		$query = $this->db->get();
		
		log_message("error",$this->db->last_query());
		
		if($query!=false) return $query->result();
		else return false;
	}

	public function total_resp($pregid, $opid, $esPublico, $esNacional, $esEmpresarial, $centros) {
		
		$this->db->select('COUNT(ru.iIdRespuesta) AS total');
		$this->db->from('iplan_resp_usuario ru');
		$this->db->join('iplan_respuestas r', 'ru.iIdRespuesta = r.iIdRespuesta','LEFT');
		$this->db->join('iplan_preguntas p', 'r.iIdPregunta = p.iIdPregunta','INNER');
		$this->db->join('iplan_cuestionarios c', 'p.iIdCuestionario = c.iIdCuestionario','INNER');
		$this->db->join('iplan_usuarios u','ru.iIdUsuario = u.iIdUsuario','LEFT');

		
		$this->db->where('r.iIdPregunta', $pregid);
		$this->db->where('r.iActivo', 1);
		$this->db->where('r.iIdOpcion', $opid);
		
		$condition = "";
		
		    if($esPublico == 1 && $esNacional == 1 && $esEmpresarial == 1){
		        $this->db->where('((ru.iIdUsuario = 0 AND ru.iIdAnonimo <> "") OR ru.iIdUsuario > 0)');
		    }
		    else if($esPublico == 1 && $esNacional == 1 && $esEmpresarial == 0){
		        $this->db->where('((ru.iIdUsuario = 0 AND ru.iIdAnonimo <> "") OR (ru.iIdUsuario > 0 AND u.iIdCentro = 0))');
		    }
		    else if($esPublico == 1 && $esNacional == 0 && $esEmpresarial == 1){
		        $this->db->where('((ru.iIdUsuario = 0 AND ru.iIdAnonimo <> "") OR (ru.iIdUsuario > 0 AND u.iIdCentro > 0))');
		    }
		    else if($esPublico == 1 && ($esNacional == 0 && $esEmpresarial == 0)){
		       $this->db->where('(ru.iIdUsuario = 0 AND ru.iIdAnonimo <> "")');
		    }
		    else if($esPublico == 0){
		        
    		    if($esEmpresarial > 0 && $esNacional == 0){
    		       // $this->db->where('u.iIdCentro >', 0);
    		        $condition = "(u.iIdCentro > 0";
    		    }
    		    else if($esEmpresarial > 0 && $esNacional == 1){
    		        //$this->db->where('u.iIdCentro >=', 0);
    		        $condition = "(u.iIdCentro >= 0";
    		    } else if($esNacional > 0) {
    		        //$this->db->where('u.iIdCentro', 0);
    		        $condition = "(u.iIdCentro = 0";
    		    }
		    } 
		    
		    if($condition == "" && $centros != null && count($centros)>0 && $esNacional > 0 && $esPublico > 0){
		        $condition = "(u.iIdCentro IS NULL OR u.iIdCentro >= 0";
		    }
		    else if($condition == "" && $centros != null  && count($centros)>0 && $esNacional == 0 && $esPublico > 0){
		        $condition = "(u.iIdCentro IS NULL OR u.iIdCentro > 0";
		    }
		    else if($condition == "" && $centros != null  && count($centros)>0 && $esNacional > 0 && $esPublico == 0){
		        $condition = "(u.iIdCentro >= 0";
		    }
		    else if($condition == "" && $centros != null  && count($centros)>0 && $esNacional == 0 && $esPublico == 0){
		        $condition = "(u.iIdCentro > 0";
		    }else{
		        $condition =$condition;
		    }
		   
		    //$condition =  && count($centros)>0  ? "(u.iIdCentro > 0" : $condition;
		    //log_message("error", "C: ".count($centros));
		        if(count($centros)>0 && $centros[0] != "0")
            	{
            	    //$condition .= " OR (";
                		for ($i = 0; $i < count($centros); $i++) {
                		    /*if($i == 0){
                		        $this->db->where('u.iIdCentro', $centros[$i]);
                		    }else{*/
                		        
                		    //}
                		    $condition .= " OR u.iIdCentro = ".$centros[$i];
                		}
                	
            	}
            	
            if($condition != ""){
            	$condition .= ")";
            	$this->db->where($condition);
            }


		

		$this->db->group_by('ru.iIdRespuesta');

		$query = $this->db->get();
		
		//log_message("error",$this->db->last_query());
		
		if($query!=false) return $query->result();
		else return false;
	}

    public function getCentroByNombre($nombre) 
	{
		$this->db->select('ce.iIdCentro, ce.vNombre, ce.iIdEntidad, ce.iIdCategoria');
		$this->db->from('iplan_centro_esp ce');
		$this->db->where('UPPER(ce.vNombre) = "'.$nombre.'"');
		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}
	
	public function carga_centros() 
	{
		$this->db->select('ce.iIdCentro, ce.vNombre, ce.iIdEntidad, ce.iIdCategoria');
		$this->db->from('iplan_centro_esp ce');
		$this->db->join('iplan_entidades en','ce.iIdEntidad = en.entidadid','INNER');
		$this->db->where('ce.iActivo', 1);
        $this->db->order_by('ce.vNombre');
		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	public function carga_centrosbycuestionario($cuestionarioid) 
	{
		$this->db->select('ce.iIdCentro, ce.vNombre');
		$this->db->from('iplan_centro_esp ce');
		$this->db->join('iplan_filtro_centros en','ce.iIdCentro = en.iIdCentro','INNER');
		$this->db->where('ce.iActivo', 1);
		$this->db->where('en.iIdCuestionario',$cuestionarioid);
        $this->db->order_by('ce.vNombre');
		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}
	
	public function carga_centrosbyzonas($zonas) 
	{
		$this->db->select('ce.iIdCentro, ce.vNombre, ce.iIdEntidad, en.entidad, ce.iIdCategoria');
		$this->db->from('iplan_centro_esp ce');
		$this->db->join('iplan_entidades en','ce.iIdEntidad = en.entidadid','INNER');
		$this->db->where('ce.iActivo', 1);
		
		if(count($zonas)>0 && $zonas[0] != "0")
		{
    		for ($i = 0; $i < count($zonas); $i++) {
    		    if($i == 0){
    		        $this->db->where('ce.iIdCategoria', $zonas[$i]);
    		    }else{
    		        $this->db->or_where('ce.iIdCategoria', $zonas[$i]);
    		    }
    		}
		}
		
        $this->db->order_by('ce.vNombre');
		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	public function carga_cuest($todos, $publico, $esnacional, $esempresarial, $zonas, $centros)
	{
	    $this->db->distinct();
		$this->db->select('c.iIdCuestionario, c.vCuestionario');
		$this->db->from('iplan_cuestionarios c');		
		if($esempresarial > 0){
    		$this->db->join('iplan_filtro_zonas z','z.iIdCuestionario = c.iIdCuestionario','INNER');
    		$this->db->join('iplan_filtro_centros ce','ce.iIdCuestionario = c.iIdCuestionario','INNER');
		}
		$this->db->where('c.iActivo',1);
		
		if($todos == 0){
		    if($publico > 0){
		        $this->db->where('c.bEsPublico',1);
		    }
		    if($esnacional > 0 && $publico == 0){
		        $this->db->where('c.bEsNacional',1);
		    }
		    else if($esnacional > 0 && $publico == 1){
		        $this->db->or_where('c.bEsNacional',1);
		    }
		    if($esempresarial > 0 && $publico == 0 && $esnacional == 0){
		        $this->db->where('c.bEsEmpresarial',1);
		    }
		    else if($esempresarial > 0 && ($publico == 1 || $esnacional == 1)){
		        $this->db->or_where('c.bEsEmpresarial',1);
		    }
		}
		
		if($esempresarial > 0){
		    if(count($zonas)>0 && $zonas[0] != "0")
    		{
        		for ($i = 0; $i < count($zonas); $i++) {
        		    if($i == 0){
        		        $this->db->where('z.iIdCategoria', $zonas[$i]);
        		    }else{
        		        $this->db->or_where('z.iIdCategoria', $zonas[$i]);
        		    }
        		}
    		}
    		if(count($centros)>0 && $centros[0] != "0")
    		{
        		for ($i = 0; $i < count($centros); $i++) {
        		    if($i == 0){
        		        $this->db->where('ce.iIdCentro', $centros[$i]);
        		    }else{
        		        $this->db->or_where('ce.iIdCentro', $centros[$i]);
        		    }
        		}
    		}
		}

		$query = $this->db->get();
		
	    log_message("error",$this->db->last_query());

	 	if($query!=false) return $query->result();
		else return false;

	}

	public function cuestionario_vigencia($where)
	{
		$this->db->select('iIdCuestionario, vCuestionario, dVigencia, vAvisoPriv, vInfoGeneral, vLinkFinal, vTextFinal');
		$this->db->from('iplan_cuestionarios');
		$this->db->where($where);
		$query = $this->db->get();

	 	if($query!=false) return $query->result();
		else return false;
	}

	public function carga_entidades()
	{
		$this->db->select('entidadid, entidad');
		$this->db->from('iplan_entidades');

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}
	
	public function carga_zonas()
	{
		$this->db->select('iIdCategoria, vNombre');
		$this->db->from('iplan_categorias');
        $this->db->where('iActivo',1);
		$query = $this->db->get();
		
		if($query!=false) return $query->result();
		else return false;
	}
	
	public function carga_zonasbycuestionario($idcuestionario)
	{
		$this->db->select('ce.iIdCategoria, ce.vNombre');
		$this->db->from('iplan_categorias ce');
		$this->db->join('iplan_filtro_zonas en','ce.iIdCategoria = en.iIdCategoria','INNER');
        $this->db->where('ce.iActivo',1);
        $this->db->where('en.iIdCuestionario',$idcuestionario);
		$query = $this->db->get();
		
		if($query!=false) return $query->result();
		else return false;
	}

	public function datos_cuest($cuestid)
	{
		$this->db->selecT('c.iTipo, c.iTodos, c.vCuestionario, c.dVigencia, c.bEsNacional, c.bEsEmpresarial, c.bEsPublico');
		$this->db->from('iplan_cuestionarios c');
		$this->db->where('iIdCuestionario', $cuestid);

		$query = $this->db->get();
		if($query != false) return $query->result();
		else return false;
	}

	public function carga_usuarios($centros, $tipo = 0)
	{
		$this->db->select('iIdUsuario, vCorreo, vNombre, vApPat, vApMat, iIdCentro');
		$this->db->from('iplan_usuarios');
		
		if($tipo == 1) //SOLO NACIONAL
		{
		    $this->db->where('iIdCentro', 0);
		}
		else if($tipo == 2) //NACIONAL Y EMPRESARIAL
		{
		    if(count($centros)>0 && $centros[0]->iIdCentro != 0)
    		{
    		    $this->db->where('iIdCentro', 0);
    		    
        		for ($i = 0; $i < count($centros); $i++) {
                    $this->db->or_where('iIdCentro', $centros[$i]->iIdCentro );
        		}
    		}
		}
		else if($tipo == 3) // SOLO EMPRESARIAL
		{
		    if(count($centros)>0 && $centros[0]->iIdCentro != 0)
    		{
    		    //log_message("error", $centros[0]);
        		for ($i = 0; $i < count($centros); $i++) {
        		    if($i == 0){
        		        //log_message('error',$centros[$i]);
        		        $this->db->where('iIdCentro', $centros[$i]->iIdCentro);
        		    }else{
        		        $this->db->or_where('iIdCentro', $centros[$i]->iIdCentro);
        		    }
        		}
    		}
    		else{
    		    $this->db->where('iIdCentro >', 0); //TODOS los EMPRESARIALES
    		}
		} 
		else if($tipo == 0) // TODOS pero talvez solo algunos empresariales
		{
		     
		    if(count($centros)>0 && $centros[0]->iIdCentro != 0)
    		{
    		    $this->db->where('iIdCentro', 0);
        		for ($i = 0; $i < count($centros); $i++) {
        		   /* if($i == 0){
        		        $this->db->where('iIdCentro', $centros[$i]->iIdCentro);
        		    }else{*/
        		        $this->db->or_where('iIdCentro', $centros[$i]->iIdCentro);
        		    //}
        		}
    		}
    		else{
    		    //TODOS
    		    $this->db->where('iIdUsuario >=', 0);
    		}
		} 
		
		

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}
	
	public function carga_usuariosReporte($centros, $tipo = 0)
    	{
    		$this->db->select('u.iIdUsuario, u.vCorreo, u.vNombre, u.vApPat, u.vApMat, u.iIdCentro, c.vNombre as Centro');
    		$this->db->from('iplan_usuarios u');
    		$this->db->join('iplan_centro_esp c','c.iIdCentro = u.iIdCentro','LEFT');
    		
    		
    		if($tipo == 1) //SOLO NACIONAL
    		{
    		    $this->db->where('u.iIdCentro', 0);
    		}
    		else if($tipo == 2) //NACIONAL Y EMPRESARIAL
    		{
    		    if($centros != null && count($centros)>0 && $centros[0] != "0")
        		{
        		    $this->db->where('u.iIdCentro', 0);
        		    
            		for ($i = 0; $i < count($centros); $i++) {
                        $this->db->or_where('u.iIdCentro', $centros[$i]);
            		}
        		}
    		}
    		else if($tipo == 3) // SOLO EMPRESARIAL
    		{
    		    if($centros != null && count($centros)>0 && $centros[0] != "0")
        		{
        		    //log_message("error", $centros[0]);
            		for ($i = 0; $i < count($centros); $i++) {
            		    if($i == 0){
            		        //log_message('error',$centros[$i]);
            		        $this->db->where('u.iIdCentro', $centros[$i]);
            		    }else{
            		        $this->db->or_where('u.iIdCentro', $centros[$i]);
            		    }
            		}
        		}
        		else{
        		    $this->db->where('u.iIdCentro >', 0); //TODOS los EMPRESARIALES
        		}
    		} 
    		else if($tipo == 0) // TODOS pero talvez solo algunos empresariales
    		{
    		    if($centros != null && count($centros)>0 && $centros[0] != "0")
        		{
                    $this->db->where('u.iIdCentro', 0);
            		for ($i = 0; $i < count($centros); $i++) {
            		   /* if($i == 0){
            		        $this->db->where('iIdCentro', $centros[$i]->iIdCentro);
            		    }else{*/
            		        $this->db->or_where('u.iIdCentro', $centros[$i]);
            		    //}
            		}
        		}
        		else{
        		    //TODOS
        		    $this->db->where('u.iIdUsuario >=', 0);
        		}
    		} 
    		
    		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
		}
		
		public function carga_usuariosReload($centros, $tipo = 0)
    	{
    		$this->db->select('u.iIdUsuario, u.vCorreo, u.vNombre, u.vApPat, u.vApMat, u.iIdCentro');
    		$this->db->from('iplan_usuarios u');
    		//$this->db->join('iplan_centro_esp c','c.iIdCentro = u.iIdCentro','LEFT');
    		
    		
    		if($tipo == 1) //SOLO NACIONAL
    		{
    		    $this->db->where('u.iIdCentro', 0);
    		}
    		else if($tipo == 2) //NACIONAL Y EMPRESARIAL
    		{
    		    if($centros != null && count($centros)>0 && $centros[0] != "0")
        		{
        		    $this->db->where('u.iIdCentro', 0);
        		    
            		for ($i = 0; $i < count($centros); $i++) {
                        $this->db->or_where('u.iIdCentro', $centros[$i]);
            		}
        		}
    		}
    		else if($tipo == 3) // SOLO EMPRESARIAL
    		{
    		    if($centros != null && count($centros)>0 && $centros[0] != "0")
        		{
        		    //log_message("error", $centros[0]);
            		for ($i = 0; $i < count($centros); $i++) {
            		    if($i == 0){
            		        //log_message('error',$centros[$i]);
            		        $this->db->where('u.iIdCentro', $centros[$i]);
            		    }else{
            		        $this->db->or_where('u.iIdCentro', $centros[$i]);
            		    }
            		}
        		}
        		else{
        		    $this->db->where('u.iIdCentro >', 0); //TODOS los EMPRESARIALES
        		}
    		} 
    		else if($tipo == 0) // TODOS pero talvez solo algunos empresariales
    		{
    		    if($centros != null && count($centros)>0 && $centros[0] != "0")
        		{
                    $this->db->where('u.iIdCentro', 0);
            		for ($i = 0; $i < count($centros); $i++) {
            		   /* if($i == 0){
            		        $this->db->where('iIdCentro', $centros[$i]->iIdCentro);
            		    }else{*/
            		        $this->db->or_where('u.iIdCentro', $centros[$i]);
            		    //}
            		}
        		}
        		else{
        		    //TODOS
        		    $this->db->where('u.iIdUsuario >=', 0);
        		}
    		} 
    		
    		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
    	}

}