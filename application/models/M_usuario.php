<?php

class M_usuario extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	public function inicia_sesion($usuario)
	{	
		$this->db->select('vContrasenia,iTipoUsuario,iIdUsuario,iTipo,iIdCentro,iActivo,vOtroCentro');
		$this->db->from('iplan_usuarios');
		$this->db->where('vCorreo',$usuario);
		$query = $this->db->get();

		if($query!=false) return $query->result();
		else return false;
	}

	public function existe_usuario($val, $op = 0)
	{
		$this->db->select('iIdUsuario');
		$this->db->from('iplan_usuarios');
		
		if($op == 0) $this->db->where('vCorreo', $val);
		else $this->db->where('vCorreo', $val);


		$query = $this->db->get();

		if($query!=false) return $query->num_rows();
		else return false;
	}

	public function carga_usuarios($c = 0, $oc = '')
	{
		$this->db->select('us.iIdUsuario,us.vNombreUsuario,us.vCorreo,us.iTipoUsuario,us.iActivo, ce.vNombre as cNombre, us.vApPat, us.vNombre, us.vApMat, us.vTelefono, us.vOtroCentro, ca.vNombre as zNombre');
		$this->db->from('iplan_usuarios us');
		$this->db->join('iplan_centro_esp ce','us.iIdCentro = ce.iIdCentro and ce.iActivo = 1', 'LEFT');
		$this->db->join('iplan_categorias ca','us.iIdZona = ca.iIdCategoria', 'LEFT');
		if($c > 0) $this->db->where('us.iIdCentro', $c);
		elseif($oc != '') {
			$this->db->where('us.vOtroCentro', $oc);
		}
		$this->db->where('us.iActivo',1);
		$query = $this->db->get();

		if($query!=false) return $query->result();
		else return false;
	}

	public function inserta_usuario($datos)
	{
		$query = $this->db->insert('iplan_usuarios', $datos);
		return $query;
	}

	public function registro_usuario($datos)
	{
		$this->db->insert('iplan_usuarios', $datos);
		return $this->db->insert_id();		
	}

	public function elimina_usuario($usid)
	{
		$this->db->set('iActivo', 0);
		$this->db->where('iIdUsuario',$usid);
		$query = $this->db->update('iplan_usuarios');
		return $query;
	}


	public function modifca_usuario($datos, $usid)
	{		
		$this->db->where('iIdUsuario',$usid);
		$query = $this->db->update('iplan_usuarios',$datos);
		return $query;
	}

	public function datos_usuario($usid)
	{
		$this->db->select('us.iIdUsuario,us.vNombreUsuario,us.vCorreo,us.iTipoUsuario,us.iTipo,us.vEntidad,us.vMunicipio, us.iIdCentro, us.iIdZona, us.vApPat, us.vNombre, us.vApMat, us.vTelefono, us.vOtroCentro');
		$this->db->from('iplan_usuarios us');		
		$this->db->where('us.iActivo',1);
		$this->db->where('us.iIdUsuario', $usid);
		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	public function carga_centros() 
	{
		$this->db->select('iIdCentro, vNombre, iIdCategoria');
		$this->db->from('iplan_centro_esp');
		$this->db->where('iActivo', 1);
        $this->db->order_by('vNombre');
		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}


	public function guarda_ncont($datos, $usuarioid)
	{
		$this->db->where('iIdUsuario',$usuarioid);
		$query = $this->db->update('iplan_usuarios',$datos);
		return $query;
	}

	public function iniciar_transaccion()
	{
	  $con = $this->load->database('default',TRUE);
	  $con->trans_begin();
	  return  $con;
	}

	public function terminar_transaccion($con)
	{
		if ($con->trans_status() === FALSE)
		{
			$con->trans_rollback();
			return false;
		}
		else 
		{
			$con->trans_commit();
			return true;
		}
	}


	public function inserta_nusuarios($tabla,$datos,$con='')
	{
		if($con == '') $con = $this->db;

		if($con->insert($tabla,$datos)) return true;
		else return false;
	}


	public function consultar_usuario_por_token($idusuario, $token)
	{
		$this->db->select('u.iIdUsuario');
		$this->db->from('iplan_usuarios u');
		$this->db->where('u.iIdUsuario',$idusuario);
		$this->db->where('u.vToken',$token);
		$response = false;

		$query =  $this->db->get();
		if($query)
		{
			if($query->num_rows() == 1) $response = true;
		}

		return $response;
	}
}