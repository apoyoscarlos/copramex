<!DOCTYPE html>
<html dir="ltr" lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url();?>assets/images/favicon.png">
    <title>Pulso Coparmex</title>
    <!-- This page plugin CSS -->
    <link href="<?=base_url();?>assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?=base_url();?>dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include('nav-bar.php'); ?>

        
        <!-- ============================================================== -->
        <div id="contenedor" class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Administrar Usuarios</h4>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- basic table -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">                                
                                <h6 class="card-subtitle" align="right"><a href="<?=base_url();?>agregar_usuario" class="btn waves-effect waves-light btn-info">Agregar usuario</a></h6>

                                <h6 class="card-subtitle" align="right"><button data-toggle="modal" data-target="#cargausModal" class="btn waves-effect waves-light btn-info">Cargar usuarios</button></h6>

                                
                                <div class="table-responsive">                                 
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <!--<th>ID</th>-->
                                                <th>Nombre</th>
                                                <th>Primer Apellido</th>
                                                <th>Segundo Apellido</th>
                                                <th>Correo</th>
                                                <th>Empresa</th>
                                                
                                                <th>Tipo de Socio Coparmex</th>
                                                <th>Centro Empresarial</th>
                                                <th>Federación</th>
                                                
                                                <th>Tipo</th>
                                                <th>Activo</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if($usuarios!=false)
                                            {
                                                foreach ($usuarios as $vus) {
                                                    switch ($vus->iTipoUsuario) {
                                                        case 1:
                                                            $tipo = 'Admin';
                                                            break;
                                                        case 2:
                                                            $tipo = 'Moderador';
                                                            break;
                                                        case 3:
                                                            $tipo = 'Usuario';
                                                            break;
                                                        case 4:
                                                            $tipo = 'Administrador local';
                                                            break;
                                                    }

                                                    $activo = ($vus->iActivo==1) ? 'Si' : 'No';

                                                    $centro = ($vus->cNombre != "") ? "Empresarial" : "Nacional";
                                                    
                                                    
                                                    
                                                    
                                                    echo '<tr id="us_'.$vus->iIdUsuario.'">
                                                        <td>'.$vus->vNombre.'</td>
                                                        <td>'.$vus->vApPat.'</td>
                                                        <td>'.$vus->vApMat.'</td>
                                                        <td>'.$vus->vCorreo.'</td>
                                                        <td>'.$vus->vOtroCentro.'</td>
                                                        
                                                        <td>'.$centro.'</td>
                                                        <td>'.$vus->cNombre.'</td>
                                                        <td>'.$vus->zNombre.'</td>
                                                        
                                                        
                                                        <td>'.$tipo.'</td>
                                                        <td>'.$activo.'</td>
                                                        <td>';
                                                            if($_SESSION['usuario']['tipo']==1) 
                                                            {
                                                                echo '<a href="javascript:" onclick="mod_us('.$vus->iIdUsuario.');"><i class="fas fa-pencil-alt"></i></a>&nbsp&nbsp&nbsp';
                                                                if($_SESSION['usuario']['nom']!=$vus->vNombreUsuario) 
                                                                    echo '<a href="javascript:" onclick="elim_us('.$vus->iIdUsuario.');"><i class="fas fa-times"></i></a>&nbsp&nbsp&nbsp';

                                                                echo "<a href=\"javascript:\" onclick=\"cambiar_pass(".$vus->iIdUsuario.", '".$vus->vNombreUsuario."');\"><i class=\"fas fa-key\"></i></a>
                                                                    ";
                                                            }                                                            
                                                            
                                                        echo '</td>
                                                    </tr>';
                                                }
                                            }
                                            ?>
                                        </tbody>                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- order table -->
                <div id="modal"></div>

                <div id="modal_cargaus">
                    <div id="cargausModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">Cargar usuarios</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body">
                                        <form id="formuploadajax" enctype="multipart/form-data" method="POST">
                                            <div class="row">
                                                <div class="col-md-12">
                                                     <a class="btn btn-rounded btn-block btn-light" style="cursor:pointer; color:blue;"  href="<?=base_url();?>docs/FormatoUsuarios.xlsx" id="descarga"><i class="m-r-10 mdi mdi-download"></i>Descargar formato de carga de usuarios</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12" style="margin-top: 30px;">
                                                    <input type="file" id="idFiles" name="file" value="seleccione un archivo" class="">
                                                </div>
                                                <div class="col-md-12" style="margin-top: 30px;">
                                                    
                                                    <button id="importarAvnc" class="btn waves-effect waves-light btn-info" type="submit" onclick="importarUsuarios()"><i class="mdi mdi-upload"></i>&nbsp;Importar Usuarios</button>
                                                </div> 
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                    
                </div>

                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
              <footer class="footer text-center">
                Encuestas 2020 
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->


    <script src="<?=base_url();?>assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?=base_url();?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?=base_url();?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="<?=base_url();?>dist/js/app.min.js"></script>
    <script src="<?=base_url();?>dist/js/app.init.horizontal.js"></script>
    <script src="<?=base_url();?>dist/js/app-style-switcher.horizontal.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?=base_url();?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="<?=base_url();?>dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?=base_url();?>dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?=base_url();?>dist/js/custom.js"></script>

    <!--Wave Effects -->
    <!--Menu sidebar -->
    <!--Custom JavaScript -->
    <!--This page plugins -->
    <script src="<?=base_url();?>assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="<?=base_url();?>dist/js/pages/datatable/datatable-basic.init.js"></script>

    <script src="<?=base_url();?>assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="<?=base_url();?>assets/libs/sweetalert2/sweet-alert.init.js"></script>

    <script type="text/javascript">

        function mod_us(usid) {            
            location.href = "<?=base_url();?>modificar_usuario?usid="+usid;
        }

        function elim_us(usid) {
            $.post('<?=base_url();?>eliminar', {usid:usid}, function(resp){
                //console.log(resp);
                if(resp==1) { 
                    Swal.fire({
                      type: 'success',
                      title: 'Correcto',
                      text: 'Usuario eliminado'
                    });
                    $('#us_'+usid).remove(); 
                }
            });
        }

        function cambiar_pass(usid, nom) {
            $.post('<?=base_url();?>C_usuario/cambiar_pass', {usid: usid, nom: nom}, function(resp) {
                $('#modal').html(resp);

            });
        }

        function importarUsuarios(){
        event.preventDefault();
       
        var formData = new FormData(document.getElementById("formuploadajax"));

        $.ajax({
        type: "POST",
        url: "<?=base_url()?>C_usuario/read_excel",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(resp) {
            resp = JSON.parse(resp);
            if(resp.estatus) {
                
                Swal.fire({
                  type: 'success',
                  title: 'Correcto',
                  text: resp.mensaje
                }).then((result) => {
                      location.reload();
                    });
                $('#cargausModal').modal('hide');
                
            } else {                
                Swal.fire({
                      type: 'error',
                      title: 'Error',
                      text: resp.mensaje
                    });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {

        }
    });

    }



    </script>

</body>

</html>