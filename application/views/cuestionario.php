<?php count($vigencia); ?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url();?>assets/images/favicon.png">
    <title>Pulso Coparmex</title>
    <!-- This page plugin CSS -->
    <link href="<?=base_url();?>assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?=base_url();?>dist/css/style.min.css" rel="stylesheet">
    <link href="<?=base_url();?>dist/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
    <style>
        .card{
            border-radius:10px !important;
        }
        .card-header{
            background:gray;
            color:white;
            border-radius:10px 10px 0px 0px !important;
        }
        .card-body{
            background:#F7F7F7;
            color:black;
             border-radius:0px 0px 10px 10px  !important;
        }
        body{
            background:#B6E0EC !important;
        }
        .page-wrapper{
            background:#B6E0EC !important;
        }
        
    </style>

</head>

<body>
    
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    
    <div id="main-wrapper" style="background:#B6E0EC !important;">
        
        <?php include('nav-bar.php'); ?>

        
        <!-- ============================================================== -->
        <div class="page-wrapper">
            
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h2 class="page-title" style="color:black"><?=$vigencia[0]->vCuestionario?></h2>
                    </div>                    
                </div>
            </div>
            
            <div class="container-fluid">
                

                <div class="row">                   
                    <div class="col-12">
                        <form id="form_cuestionario">  
                           <?php if($infoGeneral != ""){ ?>
                                <div class="card"  style="border-radius:0px 0px 0px 0px  !important;">
                                    <div class="card-body" style="border-radius:0px 0px 0px 0px  !important;">
                                        <h4><?=$infoGeneral?></h4>
                                    </div>    
                                </div>
                            <?php } ?>
                                <input type="hidden" id="cuestid" name="cuestid" value="<?=$cuestid;?>">
                                <?php                                     
                                    $resp_us = array();
                                    $resp_us_t = array();
                                    $requiresSubQuestion = 0;
                                    $subQuestionToPrint = 0;
                                    $pregHijo = 0;
                                    //print_r($respuestas);
                                    if($respuestas!=false)
                                    {
                                        foreach ($respuestas as $vresp) {
                                            array_push($resp_us, $vresp->iIdRespuesta);

                                            $resp_us_t[$vresp->iIdRespuesta] = array(
                                                'respuesta' => $vresp->vRespuesta);
                                        }
                                    }
                                
                                    //print_r($resp_us);
                                    $pregid = 0;  
                                    $preghid = 0;
                                    if($preguntas!=false && count($preguntas) > 0)
                                    {
                                        $cont = 1;
                                        if(count($resp_us) > 0) $dis = 'disabled';
                                        else $dis = '';

                                        //print_r($preguntas);
                                        foreach ($preguntas as $vpreg) {
                                            if($vpreg->iObligatorio > 0){
                                                $obligatorio = "required='required'";
                                            }else{
                                                $obligatorio = "";
                                            }

                                            if($vpreg->iIdPregunta!=$pregid)
                                            {
                                                if($pregid>0 && $tipobefore == 0) echo '</select>';
                                                if($pregid>0) echo '</div></div></div></div></div>';
                                            
                                            ?>
                                                <?php include('preguntas-hijas.php'); ?>
                                                
                                            <?php
                                                echo '<div class="card"><div class="card-header"><h2>Pregunta '.$cont.'</h2></div><div class="card-body"><div class="row" >';
                                                echo '<div class="col-md-7">
                                                    <div class="row">
                                                        <div class="col-md-12"><h4 class="card-title">'.$vpreg->vPregunta.'';

                                                if($vpreg->vLink!='')
                                                {
                                                      echo ' (
                                                        <small><a href="'.$vpreg->vLink.'" target="_blank">Ver Link</a></small>)';
                                                }
                                                    echo '</h4></div></div>
                                                    
                                                </div>';
                                                $pregid = $vpreg->iIdPregunta;
                                                $tipobefore = $vpreg->iTipoPregunta;
                                                
                                                
                                               
                                                
                                                echo '<div class="col-md-5"><div class="preg_'.$pregid.'" id="preg_'.$pregid.'">';
                                                if($vpreg->iTipoPregunta == 0) echo '<select class="form-control" onchange="showPregunta('.$vpreg->iIdOpcion.','.$vpreg->iIdPregunta.',1)" data="'.$vpreg->iIdOpcion.'" id="op_'.$vpreg->iIdPregunta.'" name="preg_'.$vpreg->iIdPregunta.'" '.$dis.'  '.$obligatorio.'>';

                                                $cont++;
                                            }

                                            $disp = ($vpreg->iTipoPregunta==2) ? 'style="display: none"' : '';                                        

                                            if($vpreg->iTipoPregunta==3) { $tipo_el = 'checkbox'; $ar = '[]'; }
                                            else { $tipo_el = 'radio'; $ar = ''; }

                                            if(in_array($vpreg->iIdRespuesta, $resp_us)) $check = 'checked';
                                            else 
                                            {
                                                if($vpreg->iTipoPregunta==2) $check = 'checked';
                                                else $check = '';
                                            }

                                            $r_op = ($vpreg->iTipoPregunta==2) ? '' : $vpreg->vOpcion;

                                            if($vpreg->iTipoPregunta == 4)
                                            {
                                                echo '
                                                <div class="custom-control custom-checkbox">
                                                    <input onchange="showPregunta('.$vpreg->iIdOpcion.','.$vpreg->iIdPregunta.')" data-opt="'.$vpreg->iIdOpcion.'" checked style="display: none" type="checkbox" id="op_'.$vpreg->iIdRespuesta.'" name="preg_'.$vpreg->iIdPregunta.'[]" class="custom-control-input" value="'.$vpreg->iIdRespuesta.'">
                                                    <label class="custom-label" for="op_'.$vpreg->iIdRespuesta.'">'.$r_op.'</label>';
                                                    
                                                        if(isset($resp_us_t[$vpreg->iIdRespuesta]['respuesta'])) $r_otro = $resp_us_t[$vpreg->iIdRespuesta]['respuesta'];
                                                        else $r_otro = '';

                                                        echo '<div class="col-md-12">
                                                                <input id="re_'.$vpreg->iIdRespuesta.'" '.$dis.' '.$obligatorio.' name="re_'.$vpreg->iIdRespuesta.'" type="number" class="form-control" value="'.$r_otro.'"/>
                                                            </div>';
                                                                                    
                                                echo '</div>';
                                            }
                                            elseif($vpreg->iTipoPregunta == 0)
                                            {
                                                if(in_array($vpreg->iIdRespuesta, $resp_us)) $check = 'selected';
                                                else 
                                                {
                                                    $check = '';
                                                }
                                            
                                                echo '<option value="'.$vpreg->iIdRespuesta.'" '.$check.'>'.$r_op.'</option>';
                                            }
                                            else {
                                                 echo '
                                                <div class="custom-control custom-'.$tipo_el.'">
                                                    <input onchange="showPregunta('.$vpreg->iIdOpcion.','.$vpreg->iIdPregunta.')" data-opt="'.$vpreg->iIdOpcion.'" '.$check.' '.$dis.' '.$obligatorio.' type="'.$tipo_el.'" id="op_'.$vpreg->iIdRespuesta.'" name="preg_'.$vpreg->iIdPregunta.$ar.'" class="custom-control-input" value="'.$vpreg->iIdRespuesta.'">
                                                    <label class="custom-control-label" '.$disp.' for="op_'.$vpreg->iIdRespuesta.'">'.$r_op.'</label>';
                                                
                                                    if($vpreg->iTipoPregunta==2)
                                                    {

                                                        if(isset($resp_us_t[$vpreg->iIdRespuesta]['respuesta'])) $r_otro = $resp_us_t[$vpreg->iIdRespuesta]['respuesta'];
                                                        else $r_otro = '';

                                                        echo '<div class="col-md-12">
                                                                <input id="re_'.$vpreg->iIdRespuesta.'" '.$dis.' '.$obligatorio.' name="re_'.$vpreg->iIdRespuesta.'" type="text" class="form-control" value="'.$r_otro.'" />
                                                            </div>';
                                                    }                                   
                                                echo '</div>';
                                            }
                                            
                                            
                                            //print_r($preguntas);

                                            foreach ($hijos as $vpregh) {
                                                
                                                if($vpregh->iIdPreguntaPadre == $pregid && $pregHijo != $vpregh->iIdOpcion){
                                                    //print_r($vpregh);
                                                    //print_r($pregid);
                                                    $requiresSubQuestion = 1;
                                                    $subQuestionToPrint = $vpregh->iIdPreguntaPadre;
                                                    $pregHijo = $vpregh->iIdOpcion;
                                                    break;
                                                }
                                            }
                                            
                                            

                                        }
                                        
                                        echo '</div></div></div></div></div>';
                                        
                                        ?>
                                        
                                        <?php include('preguntas-hijas.php'); ?>
                                        
                                        <?php
                                        
                                        
                                       
                                    }
                                    else echo '<h2>El cuestionario aún no se encuentra disponible</h2>';
                                    ?>                                    
                                    
                                
                            
                        </form>
                        
                          <?php
            
                                        if(count($resp_us) == 0) {
                                            if($pregid>0 && $tipobefore == 0) echo '</select>';
                                                
                                            
                                        }
             ?> 

                <!-- order table -->        
                                
          
            
            
                <?php 
                  if(count($resp_us) == 0) {
                  echo '<div class="card"><div class="card-body"><div class="row"><div class="col-md-10">
                                                <label>Aviso de Privacidad: </label><a target="_blank" href="'.$aviso.'">Leer</a><br />
                                                        <input type="checkbox" class="custom-control-input" id="terminos" name="terminos"  >
                                                        <label class="custom-control-label" for="terminos" style="margin-left: 2%;">He leído, doy mi consentimiento y acepto las condiciones del Aviso de Privacidad.</label>
                                            </div>
                                            <div class="col-md-2"><button id="envia_enc" type="submit" form="form_cuestionario" style="margin-top: 4%" class="btn btn-primary">Enviar Cuestionario</button></div></div></div></div>';
                  }
                ?>
            
            </div>
            <footer class="footer text-center">
                Pulso Coparmex. 
                
            </footer>
            
        </div>
        
    </div>
       
    <div class="chat-windows"></div>
    


    <script src="<?=base_url();?>assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?=base_url();?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?=base_url();?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="<?=base_url();?>dist/js/app.min.js"></script>
    <script src="<?=base_url();?>dist/js/app.init.horizontal.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?=base_url();?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="<?=base_url();?>dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?=base_url();?>dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?=base_url();?>dist/js/custom.js"></script>

    
    <!--This page plugins -->
    <script src="<?=base_url();?>assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="<?=base_url();?>dist/js/pages/datatable/datatable-basic.init.js"></script>

    <script src="<?=base_url();?>assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>    
    <script src="<?=base_url();?>assets/libs/jquery-validation/dist/additional-methods.js"></script>  

    <script src="<?=base_url();?>dist/js/fileinput.js" type="text/javascript"></script>
    <script src="<?=base_url();?>dist/js/es.js" type="text/javascript"></script>

    <script src="<?=base_url();?>assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="<?=base_url();?>assets/libs/sweetalert2/sweet-alert.init.js"></script>

    <script type="text/javascript">

     $.extend($.validator.messages, { required: "Campo obligatorio, debe llenarlo para continuar." }); 

        $( "#myform" ).validate({
          rules: {   

            required: true            
          }
        });


        function showPregunta(id, quest, isselect = 0){
            
            var originalid = id;
            var ind = 0;
            if(isselect > 0){
               ind = $("#op_"+quest+" option:selected").index();
               id += ind;
            }

            if($('#question-'+id+'-'+quest) != undefined){
                $('#question-'+id+'-'+quest).show();
            }

            $('#preg_'+quest+' input[type=checkbox]').each(function () {
                if(!this.checked){
                    var data = this.getAttribute("data-opt");
                    if($("#question-"+data+'-'+quest) != undefined){
                        $('#question-'+data+'-'+quest).hide();
                    }
                }
            });
            
            $('#preg_'+quest+' input[type=radio]').each(function () {
                if(!this.checked){
                    var data = this.getAttribute("data-opt");
                    if($("#question-"+data+'-'+quest) != undefined){
                        $('#question-'+data+'-'+quest).hide();
                    }
                }
            });
            
            $('#preg_'+quest+' > select > option').each(function () {
                if(!this.selected){
                    originalid = originalid + this.index;
                    
                    if($("#question-"+originalid+'-'+quest) != undefined){
                        $('#question-'+originalid+'-'+quest).hide();
                    }
                }
            });
        }

        $(".file").fileinput({
            showUpload: false,
            language: 'es',
            layoutTemplates: {
                actions: '<div class="file-actions">\n' +
                '    <div class="file-footer-buttons">\n' +
                '        {delete} {zoom}' +
                '    </div>\n' +
                '    {drag}\n' +
                '    <div class="clearfix"></div>\n' +
                '</div>',
            },
            uploadUrl: '<?=base_url();?>archivo',
            maxFileCount: 1,
            allowedFileExtensions: ["pdf", "xls"],
            maxFileSize: 10240,            
            uploadAsync: false,           
        });    

        
        (function() {
            $("#form_cuestionario").validate({
                ignore: [],
                errorPlacement: function(error, element) {
                    error.appendTo( element.parent("div"));
                },
                submitHandler: function(form) {
                  envia_form();
                }
            });
            var arr = [];
            var arr2 = [];

            /*$("input[name^='preg']").each(function(index){
                if(this.type=='radio' || this.type=='checkbox') {
                    if(!arr.includes(this.name)) {
                        arr.push(this.name);
                        $("[name='"+this.name+"']").rules("add", {
                            required: true,
                            messages: {
                                required: 'Seleccione una opción'
                            }
                        });                        
                    }
                }
            }); 

            $("input[name^='re']").each(function(index){
                if(this.type=='text' || this.type=='number') {
                    if(!arr2.includes(this.name)) {
                        arr2.push(this.name);
                        
                        var id = this.name.split('_');
                        
                        $("[name='"+this.name+"']").rules("add", {
                            required: true,
                            messages: {
                                required: 'Debe llenar este campo'
                            }
                        });
                    }
                }
            });*/

        })();

        function envia_form() {

            if(!$('#terminos').is(':checked')){
               Swal.fire({
                              type: 'error',
                              title: 'Error',
                              text: 'Es necesario aceptar el aviso de privacidad para enviar el cuestionario.'
                            });
               return false;
            }

            var form = $('#form_cuestionario').serialize();
            $.ajax({
                type: 'POST',
                url: '<?=base_url();?>respuestas',
                data: form,
                async: false,                        
                success: function(data) {
                    var footer = '';
                    var dataText = 'Información guardada correctamente.';
                    
                    <?php if($vigencia[0]->vLinkFinal != ""){ ?>
                        footer = '<p style="margin-right:5px"><?=$vigencia[0]->vTextFinal?></p><a href="<?=$vigencia[0]->vLinkFinal?>" > Ir al Link</a>';
                    <?php } ?>

                    Swal.fire({
                      type: 'success',
                      title: 'Correcto',
                      html: dataText,
                      footer: footer
                    }).then((result) => {
                      location.replace('/');
                    });

                    if(data==1) {
                        $("input[type='radio']").each(function(i) {
                            $(this).attr('disabled', 'disabled');
                        });

                        $("input[type='checkbox']").each(function(i) {
                            $(this).attr('disabled', 'disabled');
                        });

                        $("input[type='select']").each(function(i) {
                            $(this).attr('disabled', 'disabled');
                        });

                        $("input[name^='input']").each(function(index){                
                            var filesCount = $('#'+this.name).fileinput('getFilesCount');
                            if(filesCount>0) $('#'+this.name).fileinput('upload');
                        });

                        document.getElementById("envia_enc").remove();
                        //location.reload();
                    }
                    
                    //
                },
                error: function(e) {
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      text: 'No se pudo guardar la información: '+e
                    });
                 }

            });
        }

        function envia_archivo() {
            //var arch = $('#archivo').val();
            var form = $('#form_archivo_dos').serialize();
            console.log(form);
            $.post('<?=base_url();?>archivo', $('#form_archivo_dos').serialize(), function(resp){
                console.log(resp);
            });
        }

    </script>
</body>

</html>