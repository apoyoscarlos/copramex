<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url();?>assets/images/favicon.png">
    <title>Pulso Coparmex</title>
    <!-- This page plugin CSS -->
    <link href="<?=base_url();?>assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?=base_url();?>dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2-selection--multiple {
             height: auto !important;
        }
        
        .select2-container--classic .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            background-color: #dee2e6;
            color: #220fbf;
        }
    </style>
    
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include('nav-bar.php'); ?>

        
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-md-11 text-right"><button class="btn btn-success" type="button" onclick="actualizarCuestionario(1);"><i class="fas fa-disk"></i>&nbsp;Guardar</button></div>
                    <div class="col-md-1 text-right"><button class="btn btn-white" type="button" onclick="regresar(event);"><i class="fas fa-arrow-alt-circle-left"></i>&nbsp;Regresar</button></div>
                </div>
            </div>
       
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <legend>Captura de cuestionario</legend>
                                <!--<h6 class="card-subtitle">You can us the validation like what we did</h6>-->
                                <form id="form-cuestionario" action="#" class="validation-wizard wizard-circle m-t-40">
                                    <!-- Step 1 -->
                                    <section>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="hidden" name="iIdCuestionario" id="iIdCuestionario" value="<?=$iIdCuestionario?>">
                                                    <label for="wfirstName2"> Nombre del cuestionario: <span class="text-danger">*</span> </label>
                                                    <input type="text" class="form-control required" id="vCuestionario" name="vCuestionario" value="<?=$vCuestionario?>" onblur="actualizarCuestionario();" > </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wfirstName2">Información General del cuestionario:  </label>
                                                    <textarea class="form-control " id="vInfoGeneral" name="vInfoGeneral"  onblur="actualizarCuestionario();" ><?=$vInfoGeneral?></textarea> </div>
                                            </div>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wfirstName2"> Descripción del cuestionario:  </label>
                                                    <textarea class="form-control " id="vDescripcion" name="vDescripcion"  onblur="actualizarCuestionario();" ><?=$vDescripcion?></textarea> </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="hidden" name="iIdCuestionario" id="iIdCuestionario" value="<?=$iIdCuestionario?>">
                                                    <label for="wfirstName2"> Aviso de privacidad: <span class="text-danger">*</span> </label>
                                                    <input type="text" class="form-control required" id="vAvisoPriv" name="vAvisoPriv" value="<?=$vAvisoPriv?>" onblur="actualizarCuestionario();" > </div>
                                            </div>
                                        </div>

<hr />
<h5>Filtros Principales</h5><br/>
                                        <div class="row">
                                            
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="iTipoEncuesta"> Tipo Encuesta: <span class="text-danger">*</span> </label>
                                                    <select name="iTipoEncuesta" id="iTipoEncuesta" style="width:100%;" class="js-example-basic-multiple" multiple="multiple" onchange="logicTipo();" required>
                                                        <option value="1">Comparmex NACIONAL</option>
                                                        <option value="2">Centro Empresarial</option>
                                                        <option value="3">Público</option>                                                       
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group" id="zonas" style="display:none">
                                                    <label for="iZona"> Zona: <span class="text-danger">*</span> </label>
                                                    <select name="iZona" id="iZona" style="width:100%;" class="js-example-basic-multiple" multiple="multiple" onchange="logicZona();">
                                                         <option value="0">TODOS</option>
                                                         <?php 
                                                            foreach ($zonas as $vcent) {
                                                               echo '<option value="'.$vcent->iIdCategoria.'" >'.$vcent->vNombre.'</option>';
                                                            }
                                                        ?>                                                       
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group" id="divcentro" style="display:none">
                                                    <label for="iTipo"> Centro Empresarial: <span class="text-danger">*</span> </label>
                                                    <select name="iTipo" id="iTipo" style="width:100%;" class="js-example-basic-multiple" multiple="multiple" onchange="logicCentros()" >
                                                        <option value="0">TODOS</option>
                                                        <?php 
                                                        foreach ($centros as $vcent) {
                                                            if($vcent->iIdCentro == $iTipo)
                                                                echo '<option value="'.$vcent->iIdCentro.'" selected>'.$vcent->vNombre.'</option>';
                                                            else 
                                                                echo '<option value="'.$vcent->iIdCentro.'">'.$vcent->vNombre.'</option>';
                                                        }
                                                        ?>                                                        
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
<hr />
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="fecha_ini"> Fecha de Inicio: </label>
                                                    <input id="fecha_ini" name="fecha_ini" type="date" class="form-control" onchange="actualizarCuestionario();" value="<?=$dFechaCreacion;?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="fecha_term"> Fecha de Vigencia: </label>
                                                    <input id="fecha_term" name="fecha_term" type="date" class="form-control" onchange="actualizarCuestionario();" value="<?=$dVigencia;?>">
                                                </div>
                                            </div>
                                        </div>
                                        <hr />
                                        <h5>Configuración Final del Cuestionario</h5><br/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wfirstName2">  Texto en la confirmación del Cuestionario </label>
                                                    <input type="text" class="form-control required" id="vTextFinal" name="vTextFinal" value="<?=$vTextFinal?>" onblur="actualizarCuestionario();" > </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wfirstName2"> Link de redirección al final del Cuestionario: </label>
                                                    <input type="text" class="form-control required" id="vLinkFinal" name="vLinkFinal" value="<?=$vLinkFinal?>" onblur="actualizarCuestionario();" > </div>
                                            </div>
                                        </div>

                                        <div class="row" style="display:none">
                                            <div class="col-md-3">

                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="check_todos" name="check_todos" onclick="actualizarCuestionario();" <?=($iTodos == 1) ? 'checked' : '';?> >
                                                        <label class="custom-control-label" for="check_todos">Todos</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="check_publico" name="check_publico" onclick="actualizarCuestionario();" <?=($bEsPublico == 1) ? 'checked' : '';?> >
                                                        <label class="custom-control-label" for="check_publico">Publico (Los usuarios sin cuenta pueden verlo)</label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="check_nacional" name="check_nacional" onclick="actualizarCuestionario();" <?=($bEsNacional == 1) ? 'checked' : '';?> >
                                                        <label class="custom-control-label" for="check_nacional">Nacional</label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="check_empresarial" name="check_empresarial" onclick="actualizarCuestionario();" <?=($bEsEmpresarial == 1) ? 'checked' : '';?> >
                                                        <label class="custom-control-label" for="check_empresarial">Empresarial</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>





                                    </section>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <section>

                <div class="row">
                    <div class="col-12" id="div-preguntas">
                        <?=$preguntas?>
                    </div>
                </div>

                 <div class="row">                   
                    <div class="col-md-10 text-right"><button class="btn btn-success" type="button" onclick="actualizarCuestionario(1);"><i class="fas fa-disk"></i>&nbsp;Guardar</button></div>
                    <div class="col-md-2 text-right"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalPreg"><i class="fas fa-plus"></i>&nbsp;Agregar pregunta</button></div>
                </div>

                </section>
            </div>


            <div id="modalPreg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <input type="text" class="form-control required" id="txtnPreg" name="txtnPreg" value="" placeholder="Escriba aquí su pregunta" required>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info waves-effect" onclick="crearPregunta(<?=$iIdCuestionario?>);">Crear pregunta</button>
                            <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
                        
     
             <footer class="footer text-center">
                Encuestas 2020 
            </footer>
     
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->


    <script src="<?=base_url();?>assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?=base_url();?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?=base_url();?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="<?=base_url();?>dist/js/app.min.js"></script>
    <script src="<?=base_url();?>dist/js/app.init.horizontal.js"></script>    
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?=base_url();?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="<?=base_url();?>dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?=base_url();?>dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?=base_url();?>dist/js/custom.js"></script>

    <script src="<?=base_url();?>assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>    
    <script src="<?=base_url();?>assets/libs/jquery-validation/dist/additional-methods.js"></script>

    <script src="<?=base_url();?>assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="<?=base_url();?>assets/libs/sweetalert2/sweet-alert.init.js"></script>
    
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

    <script type="text/javascript">
    
       <?php
          $zonasSelectedOK = array();
          $i=0;
          foreach ($zonasSelected as $vcent) {
             $zonasSelectedOK[$i] = $vcent->iIdCategoria;
             $i++;
          }
          
          $centrosSelectedOK = array();
          $j=0;
          foreach ($centrosSelected as $vcent) {
             $centrosSelectedOK[$j] = $vcent->iIdCentro;
             $j++;
          }
          
          $selectedType = array();
          if($bEsNacional){
              $selectedType[] = "1";
          }
          if($bEsEmpresarial){
              $selectedType[]= "2";
          }
          if($bEsPublico){
              $selectedType[]= "3";
          }
       ?>
    
        var zonasSelected =  <?php echo json_encode($zonasSelectedOK); ?>;
        var centrosSelected = <?php echo json_encode($centrosSelectedOK); ?>;
        var typesSelected = <?php echo json_encode($selectedType); ?>;
    
        $(document).ready(function() {
            setSelect();
            
            $("#iTipoEncuesta").val(typesSelected);
            $('#iTipoEncuesta').trigger('change');
            
            $("#iZona").val(zonasSelected);
            $('#iZona').trigger('change');
        });
    
        function setSelect(){
             $('.js-example-basic-multiple').select2({
                placeholder: 'Selecciona 1 o varios...',
                multiple:true
            });
            
        }
    
        var valida_form = $('#form-cuestionario').validate({
      //ignore: [],
          rules: {
            vCuestionario: "required",            
            iTipo:  "required"            
          },
          messages: {
            vCuestionario: "Este campo es obligatorio",
            iTipo: 'Debe seleccionar una opción'
          },
          submitHandler: function(form) {
            guardarCuestionario();
          }
        }); 

        function regresar(e){
            e.preventDefault();
            window.location.href = "<?=base_url();?>listado-cuestionarios";

        }

        function copiarPregunta(id){
            console.log("Pregunta Copiada...");

            localStorage.setItem('idPregunta', id);

            Swal.fire({
                          type: 'success',
                          title: 'Pregunta Copiada!!!',
                          text: 'Ahora puede navegar al listado de cuestionarios y pegarlo en algun cuestionario.'
                        });
        }

        function actualizarCuestionario(confirmacion){
            var centro = $('#iTipo').val();
            var zonas = $('#iZona').val();
            var vigencia = $('#fecha_term').val();
            
            //console.log($('#iTipo').val());
            //console.log($('#iZona').val());
            
            if(/*centro!='' && */vigencia != '') {
                $.post('<?=base_url();?>C_cuestionario/guardar_cuestionario', $('#form-cuestionario').serialize()+ "&centrosarray="+centro+ "&zonasarray="+zonas, function(resp){
                    if(resp==1) { 
                        
                    }
                    
                    if(confirmacion != undefined && confirmacion == 1){
                         Swal.fire({
                          type: 'success',
                          title: 'Correcto',
                          text: 'Información guardada correctamente'
                        });
                    
                        history.go(-1);
                    }
                });
            }
            else if(vigencia == '') {
                Swal.fire({
                  type: 'error',
                  title: 'Error',
                  text: 'Debe seleccionar la vigencia del cuestionario'
                });
            }
            else {
                Swal.fire({
                  type: 'error',
                  title: 'Error',
                  text: 'Debe seleccionar un centro para guardar los cambios'
                });
            }
        }

        function guardarTextoPregunta(iIdPregunta){
            $.post('<?=base_url();?>C_cuestionario/guardar_texto_pregunta', $('#form-preg'+iIdPregunta).serialize(), function(resp){
                if(resp==1) { 
                    
                }
            });
        }

         function guardarRequiereEvidencia(iIdPregunta){
            $.post('<?=base_url();?>C_cuestionario/guardar_requiere_evidencia', $('#form-preg'+iIdPregunta).serialize(), function(resp){
                if(resp==1) { 
                    
                }
            });
        }

        function guardarTextoOpcion(iIdOpcion){
            $.post('<?=base_url();?>C_cuestionario/guardar_texto_opcion', $('#form-op'+iIdOpcion).serialize(), function(resp){
                if(resp==1) { 
                    
                }
            });
        }
        
        function guardarOtro(iIdOpcion){
            $.post('<?=base_url();?>C_cuestionario/guardar_otro', $('#form-op'+iIdOpcion).serialize(), function(resp){
                if(resp==1) { 
                    
                }
            });
        }
        
        var previousSelected = "";
        
        function guardaRelacion(iIdPreguntaHijo, iIdCuestionario, iIdPreguntaPadre, iIdOpcion){
            
            console.log(iIdPreguntaHijo.value);
            
            $.post('<?=base_url();?>C_cuestionario/guardar_relacion_pregunta', { iPreguntaHija: iIdPreguntaHijo.value, iIdCuestionario: iIdCuestionario, iPreguntaPadre:iIdPreguntaPadre, iPreguntaOpcion:iIdOpcion }, function(resp){
                
                if(resp=="2") { 
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      text: 'Es imposible agregar como pregunta anidada, porque ya es pregunta padre de la pregunta actual.'
                    });
                    
                    iIdPreguntaHijo.value = 0;
                    
                }
                else if(resp!="0") { 
                    //$('#div-pregunta-'+iIdPreguntaHijo.value).remove();
                    //mostrarPregunta(iIdPreguntaHijo.value);
                }
                else{
                    //$('#div-pregunta-'+previousSelected).remove();
                    //mostrarPregunta(previousSelected);
                }
            });
        }
        
        function preguntaSelected(iIdPreguntaHijo){
            previousSelected = iIdPreguntaHijo.value;
        }
        

        function guardarRango(iIdPregunta,vValor){
            $.post('<?=base_url();?>C_cuestionario/guardar_rango',$('#form-rango-'+iIdPregunta+'-'+vValor).serialize(), function(resp){
                if(resp==1) { 
                    
                }
            });
        }

        function cambiarTipoPregunta(iIdPregunta){
            $.post('<?=base_url();?>C_cuestionario/cambiar_tipo_pregunta', $('#form-preg'+iIdPregunta).serialize(), function(resp){
                if(resp==1) { 
                    mostrarOpciones(iIdPregunta);
                    
                    $("#div-rangos"+iIdPregunta).show();
                } else {
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      text: resp
                    });
                }
            });
        }

        function crearPregunta(iIdCuestionario) {
            let preg = $('#txtnPreg').val();
            if(preg != '') {
                $.post('<?=base_url();?>C_cuestionario/crear_pregunta', {iIdCuestionario: iIdCuestionario, preg: preg}, function(resp){
                    var cod = resp.split('-');
                    if(cod[0] == 1){
                        mostrarPregunta(cod[1]);
                        $('#modalPreg').modal('hide');
                        $('#txtnPreg').val('');
                    }else {
                        Swal.fire({
                          type: 'error',
                          title: 'Error',
                          text: cod[1]
                        });
                    }
                });
            }
            else alert('Debe agregar una pregunta para continuar')
        }

        function agregarOpcion(iIdPregunta) {
            $.post('<?=base_url();?>C_cuestionario/agregar_opcion', 'iIdPregunta='+iIdPregunta, function(resp){
                //var cod = resp.split('-');
                if(resp == 1){
                    mostrarOpciones(iIdPregunta);
                }else {
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      text: resp
                    });
                }
            });
        }

        function mostrarPregunta(iIdPregunta){
            $.post('<?=base_url();?>C_cuestionario/mostrar_pregunta/'+iIdPregunta+'/<?=$iIdCuestionario?>', '', function(resp){
                $("#div-preguntas").append(resp);
            });
        }

        function mostrarOpciones(iIdPregunta){
            $.post('<?=base_url();?>C_cuestionario/mostrar_opciones/'+iIdPregunta+'/<?=$iIdCuestionario?>', '', function(resp){
                $("#div-opciones" + iIdPregunta).html(resp);
            });
        }

        function eliminarPregunta(iIdPregunta){
            if(confirm('¿Realmente desea eliminar esta pregunta?')){
                $.post('<?=base_url();?>C_cuestionario/eliminar_pregunta', 'iIdPregunta='+iIdPregunta, function(resp){
                    if(resp == 1){
                        $("#div-pregunta-"+iIdPregunta).remove();    
                    }else {
                        Swal.fire({
                          type: 'error',
                          title: 'Error',
                          text: resp
                        });
                    }
                    
                });
            }
        }

        function eliminarOpcion(iIdOpcion,iIdPregunta){
            $.post('<?=base_url();?>C_cuestionario/eliminar_opcion', 'iIdOpcion='+iIdOpcion, function(resp){
                mostrarOpciones(iIdPregunta);
            });
        }
        
        function cargarCentrosByZona(){
            
            $('#iTipo option').remove();
            $.post('<?=base_url();?>C_cuestionario/centrosbyzona', 'zonasarray='+$('#iZona').val(), function(resp){
                
                var data = $.parseJSON(resp);
                $('#iTipo').append('<option value="0">TODOS</option>');
                $.each(data, function(i, item) {
                     $('#iTipo').append('<option value="'+item.iIdCentro+'">'+item.vNombre+'</option>');
                });
                   
                $("#iTipo").val(centrosSelected);
                $('#iTipo').trigger('change');
            });
        }
        
        function logicTipo(){
            var tipos = $("#iTipoEncuesta").val();
            
            if(tipos.indexOf("2") != -1){
                $("#zonas").show();
                $("#check_empresarial"). prop("checked", true);
            }else{
                $("#zonas").hide();
                $("#check_empresarial"). prop("checked", false);
            }
            
            if(tipos.length == 3){
                $("#check_todos"). prop("checked", true);
            }else{
                $("#check_todos"). prop("checked", false);
            }
            
            if(tipos.indexOf("1") != -1){
                $("#check_nacional"). prop("checked", true);
            }else{
                $("#check_nacional"). prop("checked", false);
            }
            
            if(tipos.indexOf("3") != -1){
                $("#check_publico"). prop("checked", true);
            }else{
                $("#check_publico"). prop("checked", false);
            }
        }
        
        function logicZona(){
            var tipos = $("#iZona").val();
            
            if(tipos.length > 0){
                cargarCentrosByZona();
                $("#divcentro").show();
            }
            else{
                $("#divcentro").hide();
            }
            
            if(tipos.indexOf("0") != -1 && tipos.length > 1){
                $("#iZona").val(["0"]);
                $("#iZona").trigger('change');
            }
        }
        
        function logicCentros(){
            var tipos = $("#iTipo").val();
            
            if(tipos.indexOf("0") != -1 && tipos.length > 1){
                $("#iTipo").val(["0"]);
                $("#iTipo").trigger('change');
            }
        }
        
        
    </script> 

</body>
</html>