<?php

$tipo_admin = $_SESSION['usuario']['tipo'];
$sel1 = '';
$sel2 = '';
$sel3 = '';
$sel4 = '';


if(isset($resp) && count($resp) > 0)
{
   $pagina = 'Editar usuario';
   $usid = $resp[0]->iIdUsuario;
   $nom_us = $resp[0]->vNombreUsuario;
   $correo = $resp[0]->vCorreo;
   $tipo_us = $resp[0]->iTipoUsuario;
   $tipo = $resp[0]->iTipo;
   $ent = $resp[0]->vEntidad;
   $mun = $resp[0]->vMunicipio;

   $otro = $resp[0]->vOtroCentro;


    $appat = $resp[0]->vApPat;
    $nombre = $resp[0]->vNombre;
    $apmat = $resp[0]->vApMat;
    $telefono = $resp[0]->vTelefono;


   $formid = 'form_modus';
   $centro = $resp[0]->iIdCentro;
   $zonaS = $resp[0]->iIdZona;
   $esedit = 1;
}
else
{
    $usid = 0;
    $nom_us = '';
    $correo = '';
    $tipo_us = 0;
    $tipo = 2;
    $ent = '';
    $mun = '';
    $formid = 'form_usuario';
    $pagina = 'Agregar usuario';
    $centro = 0;

    $otro = '';

    $nombre = '';
    $appat = '';
    $apmat = '';
    $telefono = '';
    $zona=0;
    $esedit = 0;
}

if($tipo_admin == 4) 
{
    $tipo_proced = $_SESSION['usuario']['tipo_proced'];
    $otro_centro = $_SESSION['usuario']['otro_centro'];

    if($tipo_proced > 0 && $otro_centro == '')
    {
        $dis_centro = '';
        //$centro = $_SESSION['usuario']['tipo_proced'];
        $disp_sel = '';
        $disp_otro = 'style="display: none"';
        $check = '';
    }
    elseif($tipo_proced == 0 && $otro_centro != '')
    {
        $dis_centro = 'disabled';
        $otro = $_SESSION['usuario']['otro_centro'];
        $disp_sel = 'style="display: none"';
        $disp_otro = '';
        $check = 'checked';
    }
    
}
else
{
    $dis_centro = '';
    if($centro == 0 && $otro == '') {
        $disp_sel = '';
        $disp_otro = 'style="display: none"';
        $check = '';
    } 
    elseif($centro == 0 && $otro != '') {
        $disp_sel = 'style="display: none"';
        $disp_otro = '';
        $check = 'checked';
    }
    elseif($centro > 0) {
        $disp_sel = '';
        $disp_otro = 'style="display: none"';
        $check = '';
    }
}




?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url();?>assets/images/favicon.png">
    <title>Pulso Coparmex</title>
    <!-- This page plugin CSS -->
    <link href="<?=base_url();?>assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?=base_url();?>dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include('nav-bar.php'); ?>

        
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title"><?=$pagina;?></h4>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- basic table -->  
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">                            
                            <form id="<?=$formid;?>">                                
                                <div class="form-body">
                                    <div class="card-body">



                                        <div class="row p-t-20">
                                            <input type="hidden" id="usuarioid" name="usuarioid" value="<?=$usid;?>">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Nombre</label>
                                                    <input type="text" id="nom_us" name="nom_us" class="form-control" placeholder="" value="<?=$nombre;?>">     
                                                </div>                                               
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Primer apellido</label>
                                                    <input type="text" id="appat" name="appat" class="form-control form-control-danger" placeholder="" value="<?=$appat;?>">
                                                </div>                                                    
                                            </div>
                                            <!--/span-->
                                        </div>




                                        <div class="row">
                                            <input type="hidden" id="usuarioid" name="usuarioid" value="<?=$usid;?>">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Segundo apellido</label>
                                                    <input type="text" id="apmat" name="apmat" class="form-control" placeholder="" value="<?=$apmat;?>">     
                                                </div>                                               
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Teléfono</label>
                                                    <input type="number" maxlength="12" id="telefono" name="telefono" class="form-control form-control-danger" placeholder="" value="<?=$telefono;?>">
                                                </div>                                                    
                                            </div>
                                            <!--/span-->
                                        </div>









                                        <div class="row">
                                            <input type="hidden" id="usuarioid" name="usuarioid" value="<?=$usid;?>">
                                            
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Correo</label>
                                                    <input type="email" id="correo" name="correo" class="form-control form-control-danger" placeholder="user@gmail.com" value="<?=$correo;?>">
                                                </div>                                                    
                                            </div>
                                            <div id="cont-entidad" class="col-md-6" style="<?=($tipo==2) ? 'display: none;' : '';?>">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Entidad federativa</label>
                                                    <input type="text" id="entidad" name="entidad" class="form-control form-control-danger" placeholder="Entidad federativa" value="<?=$ent;?>">
                                                </div>                                                    
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group" style="display:none">
                                                    <label class="control-label">Nombre de usuario</label>
                                                    <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre" value="<?=$nom_us;?>">     
                                                </div>                                               
                                            </div>
                                        </div>
                                        <?php 
                                        if($usid == 0)
                                        {?>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group has-danger">
                                                        <label class="control-label">Contraseña</label>
                                                        <input type="password" id="contrasenia" name="contrasenia" class="form-control form-control-danger" placeholder="Contraseña">
                                                    </div>                                                    
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group has-danger">
                                                        <label class="control-label">Repetir contraseña</label>
                                                        <input type="password" id="contrasenia2" name="contrasenia2" class="form-control form-control-danger" placeholder="Repetir contraseña">
                                                    </div>                                                    
                                                </div>                                        
                                                <!--/span-->                                            
                                            </div>

                                        <?php } ?>
                                        <!--/row-->
                                        <div class="row">
                                            <!--<div class="col-md-6">
                                                <div class="form-group has-success">
                                                    <label class="control-label">Procedencia</label>
                                                    <select onchange="proc(this.value);" id="sel_tipo" name="sel_tipo" class="form-control custom-select">
                                                        <option value="1" <?=($tipo==1) ? 'selected' : '';?>>Entidad</option>
                                                        <option value="2" <?=($tipo==2) ? 'selected' : '';?>>Municipio</option>
                                                    </select>   
                                                </div>                                                 
                                            </div>-->

                                            <!--<div id="cont-municipio" class="col-md-6" style="<?=($tipo==1) ? 'display: none;' : '';?>">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Municipio</label>
                                                    <input type="text" id="municipio" name="municipio" class="form-control form-control-danger" placeholder="Municipio" value="<?=$mun;?>">
                                                </div>                                                    
                                            </div>-->                                   
                                            <!--/span-->                                            
                                        </div>

                                        <?php echo 'tipo us: '.$tipo_us;?>
                                <?php echo 'tipo admin: '.$tipo_admin;?>
                                <?php echo 'tipo: '.$tipo;?>
                                        <div class="row">                                            
                                            <div class="col-md-6">
                                                <div class="form-group has-success">
                                                    <label class="control-label">Tipo de usuario</label>
                                                    <select id="sel_us" name="sel_us" class="form-control custom-select">
                                                        <option value="">Seleccionar</option>
                                                        <?php 
                                                            switch ($tipo_us) {
                                                                case 1:
                                                                    $sel1 = 'selected';
                                                                    break;
                                                                case 3:
                                                                    $sel3 = 'selected';
                                                                    break;
                                                                case 4:
                                                                    $sel4 = 'selected';
                                                                    break;
                                                            }

                                                            if($tipo_admin == 4) 
                                                            {
                                                                echo '<option value="3" '.$sel3.'>Usuario</option>';
                                                            }
                                                            else 
                                                            {
                                                                echo '<option value="1" '.$sel1.'>Administrador</option>
                                                                <option value="4" '.$sel4.'>Staff</option>
                                                                <option value="3" '.$sel3.'>Usuario</option>';
                                                            }
                                                        ?>
                                                    </select>   
                                                </div>                                                 
                                            </div>
                                            <!--/span-->  
                                     <div id="div_otro" class="col-md-6" >
                                                <div class="form-group has-success">
                                                    <label class="control-label">Empresa</label>
                                                    <input type="text" id="otro_centro" name="otro_centro" class="form-control form-control-danger" value="<?=$otro?>" placeholder="Especifique empresa"> 
                                                </div>                                                 
                                            </div>




                                                                        
                                        </div>
                                        <!--/row-->
                                        <div class="row">                   
                                        
   
                                        
                                        <div class="col-md-6" id="div_sel1">
                                            <label>Tipo de Socio Coparmex</label>
                                             <div class="row">
                                                 
                                                <div class="col-md-6 custom-control custom-checkbox">
                                                    <input type="radio" class="custom-control-input" id="check_otro" name="check_otro" onclick="otro();" <?php if($centro > 0) { echo ''; } else { echo 'checked=true'; } ?> value="nacional" >
                                                    <label class="custom-control-label" for="check_otro">Coparmex Nacional</label>
                                                </div>
                                                 <div class="col-md-6 custom-control custom-checkbox">
                                                    <input type="radio" class="custom-control-input" id="empresarial" name="check_otro" onclick="otro();" <?php if($centro > 0) { echo 'checked=true'; } else { echo ''; } ?> value="empresarial">
                                                    <label class="custom-control-label" for="empresarial">Centro Empresarial</label>
                                                </div>
                                            </div> 
                                            
                                                                                             
                                            </div>
                                            </div>
                                            <div class="row">  
                                            
                                             <div class="col-md-6" id="div_zona" <?php if($esedit != 0 && ($esedit>0 &&$zonaS>0)) { echo ''; } else { echo 'style="display:none"'; } ?>>
                                                <div class="form-group has-success">
                                                    <label class="control-label">Federación</label>
                                                    <select id="sel_zona" name="sel_zona" class="form-control custom-select" style="background-color: #dddddd;pointer-events: none;" readonly="readonly">
                                                        <option value="">Seleccionar</option>
                                                        <?php 
                                                            foreach ($zonas as $zona) {
                                                                if($zona->iIdCategoria == $zonaS)
                                                                    echo '<option value="'.$zona->iIdCategoria.'" selected>'.$zona->vNombre.'</option>';
                                                                else 
                                                                    echo '<option value="'.$zona->iIdCategoria.'">'.$zona->vNombre.'</option>';
                                                                
                                                            }
                                                        ?>
                                                    </select>   
                                                </div>    
                                                </div>

                                            <div class="col-md-6" id="div_sel" style="display:none">
                                                <div class="form-group has-success">
                                                    <label class="control-label">Centro Empresarial</label>
                                                    <select id="sel_centro" name="sel_centro" class="form-control custom-select" onchange="setZona()">
                                                        <option value="">Seleccionar</option>
                                                        <?php 
                                                            foreach ($centros as $vcent) {
                                                                if($vcent->iIdCentro == $centro)
                                                                    echo '<option value="'.$vcent->iIdCentro.'" selected  data-zona="'.$vcent->iIdCategoria.'">'.$vcent->vNombre.'</option>';
                                                                else 
                                                                    echo '<option value="'.$vcent->iIdCentro.'" data-zona="'.$vcent->iIdCategoria.'">'.$vcent->vNombre.'</option>';
                                                            }
                                                        ?>
                                                    </select>   
                                                </div>    
                                                </div>

                                                                                    
                                        </div>
                                        
                                        <!--/row-->
                                    </div>                                    
                                    
                                    <div class="form-actions">
                                        <div class="card-body">
                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>                                            
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- order table -->
                                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
             <footer class="footer text-center">
                Encuestas 2020 
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->


    <script src="<?=base_url();?>assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?=base_url();?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?=base_url();?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="<?=base_url();?>dist/js/app.min.js"></script>
    <script src="<?=base_url();?>dist/js/app.init.horizontal.js"></script>    
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?=base_url();?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="<?=base_url();?>dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?=base_url();?>dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?=base_url();?>dist/js/custom.js"></script>

    <script src="<?=base_url();?>assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>    
    <script src="<?=base_url();?>assets/libs/jquery-validation/dist/additional-methods.js"></script>

    <script src="<?=base_url();?>assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="<?=base_url();?>assets/libs/sweetalert2/sweet-alert.init.js"></script>

    <!--Custom JavaScript -->
    <!--This page plugins -->   

    <script type="text/javascript">    
    
    
    $( document ).ready(function() {
        <?php if($centro > 0) { ?>
            //cargarCentrosByZona();
             $('#div_sel').css('display','block');
        <?php } ?>
    });
        
    
        var valida_form = $('#form_usuario').validate({
      //ignore: [],
          rules: {
            nombre: "required",            
            contrasenia:  "required",
            contrasenia2: {
              required: true,
              equalTo: '#contrasenia'
            },
            sel_tipo: "required",
            correo: {
              required: true,
              email: true
            }            

          },
          messages: {
            nombre: "Ingrese su nombre",
            contrasenia: 'Ingrese su contraseña',
            contrasenia2: {
                required: 'Repita su contraseña',
                equalTo: "Las contraseñas no coinciden"
            },
            sel_tipo: "Seleccione una opción",
            correo: {
              required: "Ingrese su correo",
              email: "Ingrese un correo válido"
            }

          },
          submitHandler: function(form) {
            //console.log(form);
            guardar();
          }
        }); 

        var mod_form = $('#form_modus').validate({
      //ignore: [],
          rules: {
            nombre: "required",
            sel_tipo: "required",
            correo: {
              required: true,
              email: true
            }            

          },
          messages: {
            nombre: "Ingrese su nombre",            
            sel_tipo: "Seleccione una opción",
            correo: {
              required: "Ingrese su correo",
              email: "Ingrese un correo válido"
            }

          },
          submitHandler: function(form) {
            //console.log(form);
            modificar();
          }
        }); 

        function otro() {
            if($('#check_otro').is(':checked'))
            {
                $('#div_sel').css('display','none');
                $('#div_zona').css('display','none');
                
                $('#sel_centro').val(0);
                $('#sel_zona').val(0);
            }
            else 
            {
                $('#div_sel').css('display','block');
                $('#div_zona').css('display','block');
            }
        }
        
        function setZona(){
            var value = $('#sel_centro').find(':selected').data('zona');
            $('#sel_zona').val(value);
            $('#sel_zona').trigger("change");
        }
        
        function cargarCentrosByZona(){
            
            var zonaarray = [];
            zonaarray[0] = $('#sel_zona').val();
            
            $('#sel_centro option').remove();
            $.post('<?=base_url();?>C_cuestionario/centrosbyzona', 'zonasarray='+zonaarray, function(resp){
                
                var data = $.parseJSON(resp);
                $.each(data, function(i, item) {
                     if(item.iIdCentro == <?=$centro?> ){
                           $('#sel_centro').append('<option value="'+item.iIdCentro+'" selected>'+item.vNombre+'</option>');
                     } else { 
                          $('#sel_centro').append('<option value="'+item.iIdCentro+'">'+item.vNombre+'</option>');
                      } 
                });
                
                $('#div_sel').css('display','block');
            });
        }

        function guardar() {
            $.post('<?=base_url();?>guardar', $('#form_usuario').serialize(), function(resp){
                if(resp==1) { 
                    Swal.fire({
                      type: 'success',
                      title: 'Correcto',
                      text: 'Usuario insertado'
                    }).then((result) => {
                      history.go(-1);
                    });
                    $('#form_usuario').trigger("reset");
                }
                else {
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      text: 'El usuario no pudo ser insertado'
                    });
                }
            });
        }

        function modificar() {
            $.post('<?=base_url();?>modificar', $('#form_modus').serialize(), function(resp){
                if(resp==1) {                     
                    Swal.fire({
                      type: 'success',
                      title: 'Correcto',
                      text: 'Usuario modificado'
                    }).then((result) => {
                      history.go(-1);
                    });
                }
                else {
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      text: 'El usuario no pudo ser modificado'
                    });
                }
            });
        }

        function proc(op) {
            if(op==1) {
                $('#cont-municipio').css('display','none');
                $('#cont-entidad').css('display','block');
            } 
            else if(op==2) {
                $('#cont-municipio').css('display','block');
                $('#cont-entidad').css('display','none');
            }
        }
        
    </script> 

</body>

</html>