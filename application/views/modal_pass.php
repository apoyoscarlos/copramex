
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Cambiar la contraseña de <?=$nom;?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form id="form-nuevacont">
                    <div class="row">
                        <input type="hidden" id="usuarioid" name="usuarioid" value="<?=$usid;?>">
                        <div class="col-md-6">
                            <div class="form-group has-danger">
                                <label class="control-label">Contraseña</label>
                                <input type="password" id="nueva_contra" name="nueva_contra" class="form-control form-control-danger" placeholder="Contraseña">
                            </div>                                                    
                        </div>
                        <div class="col-md-6">
                            <div class="form-group has-danger">
                                <label class="control-label">Repetir contraseña</label>
                                <input type="password" id="repote_ncontra" name="repote_ncontra" class="form-control form-control-danger" placeholder="Repetir contraseña">
                            </div>                                                    
                        </div>                                        
                        <!--/span-->     
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info waves-effect">Guardar</button>
                        </div>                                       
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script src="<?=base_url();?>assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>    
    <script src="<?=base_url();?>assets/libs/jquery-validation/dist/additional-methods.js"></script>
<script type="text/javascript">
    (function(){
        $('#myModal').modal('show');
    })();


    var vform = $('#form-nuevacont').validate({
      //ignore: [],
          rules: {
            nueva_contra:  "required",
            repote_ncontra: {
              required: true,
              equalTo: '#nueva_contra'
            }           

          },
          messages: {            
            nueva_contra: 'Ingrese su contraseña',
            repote_ncontra: {
                required: 'Repita su contraseña',
                equalTo: "Las contraseñas no coinciden"
            }

          },
          submitHandler: function(form) {
            //console.log(form);
            guarda_ncont();
          }
        }); 

    function guarda_ncont() {
        $.post('<?=base_url();?>C_usuario/guarda_ncont', $('#form-nuevacont').serialize(), function(resp) {
            if(resp==1) {
                $('#myModal').modal('hide');
                Swal.fire({
                          type: 'success',
                          title: 'Correcto',
                          text: 'Contraseña modificada correctamente'
                        });

            } else {
                Swal.fire({
                  type: 'error',
                  title: 'Error',
                  text: 'No se pudo modificar la contraseña'
                });

            }
        });
    }
</script>