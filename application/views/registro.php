<!DOCTYPE html>
<html dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <title>Pulso Coparmex</title>
    <!-- Custom CSS -->
    <link href="<?=base_url();?>dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <div class="main-wrapper">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url(../../assets/images/big/auth-bg.jpg) no-repeat center center;">
            <div class="auth-box" style="max-width: 800px;">
                <div>
                    <div class="logo">
                        <span class="db"><img src="<?=base_url();?>assets/images/logo_giz.png" alt="logo" /></span>
                        <!--<h5 class="font-medium m-b-20">Iniciar sesión</h5>-->
                    </div>
                    <!-- Form -->
                    <div class="row">
                        <div class="col-12">
                            <form class="form-horizontal m-t-20" id="form-registro" onsubmit="registrarse(event);">
                                
                                <div class="form-body">
                                    <div class="card-body">


                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Nombre</label>
                                                    <input type="text" id="nom_us" name="nom_us" class="form-control" placeholder="" value="" onblur="genera_usuario();">     
                                                </div>                                               
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Primer apellido</label>
                                                    <input type="text" id="appat" name="appat" class="form-control form-control-danger" placeholder="" value="" onblur="genera_usuario();">
                                                </div>                                                    
                                            </div>
                                            <!--/span-->
                                        </div>




                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Segundo apellido</label>
                                                    <input type="text" id="apmat" name="apmat" class="form-control" placeholder="" value="">     
                                                </div>                                               
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Correo</label>
                                                    <input type="email" id="correo" name="correo" class="form-control form-control-danger" placeholder="user@gmail.com" value="">
                                                </div>                                                    
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group"  style="display:none">
                                                    <label class="control-label">Nombre de usuario</label>
                                                    <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre" value="" readonly>     
                                                </div>                                               
                                            </div>
                                            
                                       

                                        </div>
                                        
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Contraseña</label>
                                                    <input type="password" id="contrasenia" name="contrasenia" class="form-control form-control-danger" placeholder="Contraseña">
                                                </div>                                                    
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Repetir contraseña</label>
                                                    <input type="password" id="contrasenia2" name="contrasenia2" class="form-control form-control-danger" placeholder="Repetir contraseña">
                                                </div>                                                    
                                            </div>                                        
                                            <!--/span-->                                            
                                        </div>

                                        
                                        <!--/row-->
                                        <div class="row">                   
                                        
                                        <div id="div_otro" class="col-md-6" >
                                                <div class="form-group has-success">
                                                    <label class="control-label">Empresa</label>
                                                    <input type="text" id="otro_centro" name="otro_centro" class="form-control form-control-danger" placeholder="Especifique empresa"> 
                                                </div>                                                 
                                            </div>
                                        
                                        <div class="col-md-6" id="div_sel1">
                                            <label>Tipo de Socio Coparmex</label>
                                             <div class="row">
                                                 
                                                <div class="col-md-6 custom-control custom-checkbox">
                                                    <input type="radio" class="custom-control-input" id="check_otro" name="check_otro" onclick="otro();" checked="true" value="nacional" >
                                                    <label class="custom-control-label" for="check_otro">Coparmex Nacional</label>
                                                </div>
                                                 <div class="col-md-6 custom-control custom-checkbox">
                                                    <input type="radio" class="custom-control-input" id="empresarial" name="check_otro" onclick="otro();" value="empresarial">
                                                    <label class="custom-control-label" for="empresarial">Centro Empresarial</label>
                                                </div>

                                                <div class="col-md-6 custom-control custom-checkbox">
                                                   <!-- <input type="radio" class="custom-control-input" id="check_gempresas" name="check_otro" onclick="otro();" value="gempresas">
                                                    <label class="custom-control-label" for="check_gempresas">Grandes Empresas</label> -->
                                                </div>
                                            </div> 
                                            
                                                                                             
                                            </div>
                                            </div>
                                            <div class="row">  
                                            
                                             <div class="col-md-6" id="div_zona" style="display:none">
                                                <div class="form-group has-success">
                                                    <label class="control-label">Federación</label>
                                                    <select id="sel_zona" name="sel_zona" class="form-control custom-select" style="background-color: #dddddd;pointer-events: none;" readonly="readonly">
                                                        <option value="">Seleccionar</option>
                                                        <?php 
                                                            foreach ($zonas as $zona) {
                                                                echo '<option value="'.$zona->iIdCategoria.'">'.$zona->vNombre.'</option>';
                                                            }
                                                        ?>
                                                    </select>   
                                                </div>    
                                                </div>

                                            <div class="col-md-6" id="div_sel" style="display:none">
                                                <div class="form-group has-success">
                                                    <label class="control-label">Centro Empresarial</label>
                                                    <select id="sel_centro" name="sel_centro" class="form-control custom-select" onchange="setZona()">
                                                        <option value="">Seleccionar</option>
                                                        <?php 
                                                            foreach ($centros as $vcent) {
                                                                echo '<option value="'.$vcent->iIdCentro.'" data-zona="'.$vcent->iIdCategoria.'" >'.$vcent->vNombre.'</option>';
                                                            }
                                                        ?>
                                                    </select>   
                                                </div>    
                                                </div>

                                                                                    
                                        </div>
                                        <!--/row-->
                                        
                                        <!--/row-->
                                    </div>                   
                                        
                                        
                                        
                                            <div class="col-md-5">
                                                <div class="form-group has-success">
                                                    <label class="control-label"></label>
                                                    <input type="hidden" id="sel_us" name="sel_us" value="3">
                                                    
                                                </div>                                                 
                                            </div>
                                            <!--/span-->  

                                                             
                                    
                                    <div class="form-actions">
                                        <div class="card-body">
                                            <div class="form-group text-center">

                                                <div class="col-xs-6 p-b-20">
                                                    <div id="div_captcha"></div>
                                                </div>                              
                                                <div class="col-xs-6 p-b-20">
                                                    <button class="btn btn-block btn-lg btn-info" type="submit">Guardar</button>
                                                </div>
                                            </div>                                             
                                        </div>
                                    </div>
                                </div>
                                                               
                            </form>
                        </div>
                    </div>
                </div>
                <!--<div id="recoverform">
                    <div class="logo">
                        <span class="db"><img src="<?=base_url();?>assets/images/logo-icon.png" alt="logo" /></span>
                        <h5 class="font-medium m-b-20">Recover Password</h5>
                        <span>Enter your Email and instructions will be sent to you!</span>
                    </div>
                    <div class="row m-t-20">
                        
                        <form class="col-12" action="index.html">
                            
                            <div class="form-group row">
                                <div class="col-12">
                                    <input class="form-control form-control-lg" type="email" required="" placeholder="Username">
                                </div>
                            </div>
                            
                            <div class="row m-t-20">
                                <div class="col-12">
                                    <button class="btn btn-block btn-lg btn-danger" type="submit" name="action">Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>-->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right Sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->
    <script src="<?=base_url();?>assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?=base_url();?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?=base_url();?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="<?=base_url();?>assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="<?=base_url();?>assets/libs/sweetalert2/sweet-alert.init.js"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"async defer></script>
    <!-- ============================================================== -->
    <!-- This page plugin js -->
    <!-- ============================================================== -->
    <script>
    var div_captcha;
    var onloadCallback = function() {
        div_captcha = grecaptcha.render('div_captcha', {
          'sitekey' : '6Ld5O-MUAAAAAKkBBPxVTkoCfZreTOWlmmQV62ek'
        });
    };

    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();

    function otro() {
        if($('input:radio[name=check_otro]:checked').val() != 'empresarial')
        {
            $('#div_sel').css('display','none');
            $('#div_zona').css('display','none');
        }
        else 
        {
            $('#div_sel').css('display','block');
            $('#div_zona').css('display','block');
        }
    }
    
    function setZona(){
        var value = $('#sel_centro').find(':selected').data('zona');
        $('#sel_zona').val(value);
        $('#sel_zona').trigger("change");
    }
    
    function cargarCentrosByZona(){
            
            var zonaarray = [];
            zonaarray[0] = $('#sel_zona').val();
            
            $('#sel_centro option').remove();
            $.post('<?=base_url();?>C_cuestionario/centrosbyzona', 'zonasarray='+zonaarray, function(resp){
                
                var data = $.parseJSON(resp);
                $.each(data, function(i, item) {
                     $('#sel_centro').append('<option value="'+item.iIdCentro+'">'+item.vNombre+'</option>');
                });
                
                $('#div_sel').css('display','block');
            });
        }

    function genera_usuario() {
        var nom_us = $('#nom_us').val();
        var appat = $('#appat').val();
        let nom = nom_us.split(' ');
        let ape = appat.split(' ');

        $('#nombre').val(nom[0]+'.'+ape[0]);
        /*
        $.post('<?=base_url();?>C_usuario/existe_us', {op: 1, nom: nom[0], ape: ape[0]}, function(resp){
            if(resp==0) {
            }
            else {
                Swal.fire({
                          type: 'error',
                          title: 'Error',
                          text: 'El usuario ya existe'
                        });
            }
        });
        */


    }
    
    function registrarse(e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: "<?=base_url();?>C_usuario/registra_usuario",
            data: $('#form-registro').serialize(),
            success: function(resp) {
                switch(resp) {
                    case 'success':
                        Swal.fire({
                          type: 'success',
                          title: 'Correcto',
                          text: 'Bienvenido'                      
                        });
                        window.location.reload(true); 
                        break;
                    case 'error1':                     
                        Swal.fire({
                          type: 'error',
                          title: 'Error',
                          text: 'Las contrasñeas no coinciden'
                        });
                        break;
                    case 'error2':                     
                        Swal.fire({
                          type: 'error',
                          title: 'Error',
                          text: 'El usuario no existe'
                        });
                        break;
                    case 'error3': 
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: 'Resulva el captcha para continuar'
                        });
                        break;
                    case 'error4': 
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: 'El correo no ha sido enviado'
                        });
                        break;
                    case 'error5': 
                        Swal.fire({
                          type: 'error',
                          title: 'Error',
                          text: 'El usuario ya existe'
                        });
                        break;
                    case 'error6': 
                        Swal.fire({
                          type: 'error',
                          title: 'Error',
                          text: 'El correo ya fue registrado'
                        });
                        break;
                    case 'error7': 
                        Swal.fire({
                          type: 'error',
                          title: 'Error',
                          text: 'El usuario y el correo ya están registrados'
                        });
                        break;
                }
                grecaptcha.reset(div_captcha);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
              Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: errorThrown
                        });
               grecaptcha.reset(div_captcha);
               //console.log(XMLHttpRequest);
            }
        });       
    }

    </script>
    
      <footer class="footer text-center">
                Encuestas 2020 
            </footer>
</body>

</html>