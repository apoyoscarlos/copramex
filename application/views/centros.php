<!DOCTYPE html>
<html dir="ltr" lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url();?>assets/images/favicon.png">
    <title>Pulso Coparmex</title>
    <!-- This page plugin CSS -->
    <link href="<?=base_url();?>assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?=base_url();?>dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include('nav-bar.php'); ?>

        
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">               
                        <h4 class="page-title">Administrar Centros</h4>
                    </div>                    
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- basic table -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h6 class="card-subtitle" align="right"><a href="javascript:" class="btn waves-effect waves-light btn-info" data-toggle="modal" data-target="#modal-centro">Agregar un centro</a></h6>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Centro</th>
                                                
                                                <th>Opción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php

                                                if($centros!=false)
                                                {

                                                    foreach ($centros as $vop) {
                                                        echo '
                                                        <tr id="resp_'.$vop->iIdCentro.'">
                                                            <td>'.$vop->iIdCentro.'</td>
                                                            <td>'.$vop->vNombre.'</td>
                                                         
                                                            <td><a href="javascript:" data-toggle="modal" data-target="#modal-centro" onclick="mod_cent('.$vop->iIdCentro.','.$vop->iIdEntidad.','.$vop->iIdCategoria.');"><i class="fas fa-pencil-alt"></i></a>&nbsp&nbsp&nbsp <a href="javascript:" onclick="elim_cent('.$vop->iIdCentro.');"><i class="fas fa-times"></i></a></td>
                                                        </tr>
                                                        ';                                                    
                                                    }
                                                }                                       
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">                                
                                <!-- sample modal content -->
                                <div id="modal-centro" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Agregar centro</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <form id="form_centro">
                                                    <input type="hidden" id="cuestid" name="cuestid" value="0">
                                                    <div class="form-group">
                                                        <label for="nom_cent" class="control-label">Nombre:</label>
                                                        <input type="text" class="form-control" id="nom_cent" name="nom_cent">
                                                    </div>
                                                    <div class="form-group">                                                        
                                                        <select id="ent_cent" name="ent_cent" class="form-control custom-select">
                                                            <option value="">Entidad</option>
                                                            <?php 
                                                            if($entidades!=false)
                                                            {
                                                                foreach ($entidades as $vent) {
                                                                    echo '<option value="'.$vent->entidadid.'">'.$vent->entidad.'</option>';
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                     <div class="form-group">                                                        
                                                        <select id="zona_cent" name="zona_cent" class="form-control custom-select">
                                                            <option value="">Zona Geogr&aacute;fica</option>
                                                            <?php 
                                                            if($zonas!=false)
                                                            {
                                                                foreach ($zonas as $vent) {
                                                                    echo '<option value="'.$vent->iIdCategoria.'">'.$vent->vNombre.'</option>';
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                                                        <button type="submit" class="btn btn-danger waves-effect waves-light" id="btn-save">Guardar</button>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.modal -->                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- order table -->
                                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                Encuestas 2020 
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <div class="chat-windows"></div>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->


    <script src="<?=base_url();?>assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?=base_url();?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?=base_url();?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="<?=base_url();?>dist/js/app.min.js"></script>
    <script src="<?=base_url();?>dist/js/app.init.horizontal.js"></script>
    <script src="<?=base_url();?>dist/js/app-style-switcher.horizontal.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?=base_url();?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="<?=base_url();?>dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?=base_url();?>dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?=base_url();?>dist/js/custom.js"></script>

    <!--Wave Effects -->
    <!--Menu sidebar -->
    <!--Custom JavaScript -->
    <!--This page plugins -->
    <script src="<?=base_url();?>assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="<?=base_url();?>dist/js/pages/datatable/datatable-basic.init.js"></script>

    <script src="<?=base_url();?>assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>    
    <script src="<?=base_url();?>assets/libs/jquery-validation/dist/additional-methods.js"></script>

    <script src="<?=base_url();?>assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="<?=base_url();?>assets/libs/sweetalert2/sweet-alert.init.js"></script>

    <script type="text/javascript">

    var valida_form = $('#form_centro').validate({
      //ignore: [],
          rules: {
            nom_cent: "required",
            ent_cent: "required"
          },
          messages: {
            nom_cent: "Ingrese el nombre",            
            ent_cent: "Seleccione una opción"
          },
          submitHandler: function(form) {
            guarda_op();
          }
        });

        function mod_cent(cuestid, entid, zonaid) {
            var op  = document.getElementById('resp_'+cuestid).getElementsByTagName('td');

            var cuestid = op[0].textContent; 
            var nom_cent = op[1].textContent;
            
            $('#cuestid').val(cuestid);
            $('#nom_cent').val(nom_cent);
            $('#ent_cent').val(entid);
            $('#zona_cent').val(zonaid);
        }
                

        function guarda_op() {
            
            $('#btn-save').text("Guardando...")
            $('#btn-save').attr('disabled', 'disabled');
            
            $.post('<?=base_url();?>C_cuestionario/guarda_centro', $('#form_centro').serialize(), function(resp){                
                if(resp==1) {                     
                   
                    $('#form_centro').trigger("reset"); $('#cuestid').val(0); 
                    
                    setTimeout(function(){ 
                        $('#modal-centro').modal('hide'); 
                        
                       Swal.fire({
                          type: 'success',
                          title: 'Correcto'
                        });
                        
                        $('#btn-save').text("Guardar")
                        $('#btn-save').removeAttr('disabled');
                        
                        location.reload();
                        
                    }, 5000);
                }
                else {
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      text: 'El centro no pudo ser insertado'
                    });
                    
                    $('#btn-save').text("Guardar")
                    $('#btn-save').removeAttr('disabled');
                }
            });
        }

        function elim_cent(cuestid) {
            $.post('<?=base_url();?>C_cuestionario/elimina_centro', {cuestid:cuestid}, function(resp){
                if(resp==1) {  
                    Swal.fire({
                      type: 'success',
                      title: 'Centro eliminado con éxito'                      
                    });
                    $('#resp_'+cuestid).remove(); 
                }
            });
        }

    </script>
</body>

</html>