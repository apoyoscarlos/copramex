<!DOCTYPE html>
<html dir="ltr" lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url();?>assets/images/favicon.png">
    <title>Pulso Coparmex</title>
    <!-- This page plugin CSS -->
    <link href="<?=base_url();?>assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?=base_url();?>dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2-selection--multiple {
             height: auto !important;
        }
        
        .select2-container--classic .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            background-color: #dee2e6;
            color: #220fbf;
        }
    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>


   

    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include('nav-bar.php'); ?>

        
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
       
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                    
                        <legend><?=$nombre?></legend>
                        <div class="card">
                            <div class="card-body">
                                <label>Datos Generales</label>
                                <div class="row">
                                    <div id="loader" class="loader"  style="width: 100%;
                                        height: 100%;
                                        position: fixed;
                                        z-index: 99999;
                                        font-size: 100px;
                                        color:blue;
                                        font-weight: bold;">
                                            <div style="display: inline-block;width: 55px;
                                            height: 55px;
                                            position: absolute;
                                            top: calc(10% - 3.4px);
                                            left: calc(40% - 3.4px);">
                                                <i class="fas fa-spinner fa-pulse" ></i>
                                            </div>
                                    </div>
                                </div>
                        <div class="row">
                        
                                            <div class="col-md-4">

                                                <div class="card border-success mb-3" style="max-width: 18rem; border:0.5px solid #007bff!important;border-radius:10px;">
                                                  <div class="card-header">Encuesta enviada a</div>
                                                  <div class="card-body text-info">
                                                    <h5 class="card-title"> <label id="totaluserssent"><?=$TotalUsersSent?></label> Usuarios <?=$infoExtra?></h5>
                                                  </div>
                                                </div>
                                                
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card border-success mb-3" style="max-width: 18rem; border:0.5px solid #007bff!important;border-radius:10px;">
                                                  <div class="card-header">Encuesta contestada por</div>
                                                  <div class="card-body text-info">
                                                    <h5 class="card-title"> <label id="totaluserstake"><?=$TotalUsersTake?></label>  Usuarios</h5>
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="card border-success mb-3" style="max-width: 18rem; border:0.5px solid #007bff!important;border-radius:10px;">
                                                  <div class="card-header">Encuesta no contestada por</div>
                                                  <div class="card-body text-info">
                                                    <h5 class="card-title"> <label id="totalusersnottake"><?=$TotalUsersNotTake?></label> Usuarios</h5>
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card border-success mb-3" style="max-width: 18rem; border:0.5px solid #007bff!important;border-radius:10px;">
                                                  <div class="card-header">Encuesta contestada por</div>
                                                  <div class="card-body text-info">
                                                    <h5 class="card-title"> <label id="totalnacional"><?=$TotalNacional?></label> Usuarios Coparmex Nacional</h5>
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card border-success mb-3" style="max-width: 18rem; border:0.5px solid #007bff!important;border-radius:10px;">
                                                  <div class="card-header">Encuesta contestada por</div>
                                                  <div class="card-body text-info">
                                                    <h5 class="card-title"> <label id="totalempresarial"><?=$TotalEmpresarial?></label> Usuarios Centro Empresarial</h5>
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card border-success mb-3" style="max-width: 18rem; border:0.5px solid #007bff!important;border-radius:10px;">
                                                  <div class="card-header">Encuesta contestada por</div>
                                                  <div class="card-body text-info">
                                                    <h5 class="card-title"> <label id="totalpublic"><?=$TotalPublicUsers?></label> Usuarios Publicos</h5>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                        <div class="card">
                            <div class="card-body">
                                <!--<h6 class="card-subtitle">You can us the validation like what we did</h6>-->
                                
                                    <!-- Step 1 -->
                                    <section>
                                        
                                        
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="iTipoEncuesta"> Tipo Encuesta: </label>
                                                    <select name="iTipoEncuesta" id="iTipoEncuesta" style="width:100%;" class="js-example-basic-multiple" multiple="multiple" onchange="logicTipo();" required>
                                                        <option value="1">Comparmex NACIONAL</option>
                                                        <option value="2">Centro Empresarial</option>
                                                        <option value="3">Público</option>                                                       
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="zonas" style="display:none">
                                                <div class="form-group"  >
                                                    <label for="iZona"> Federación:</label>
                                                    <select name="iZona" id="iZona" style="width:100%;" class="js-example-basic-multiple" multiple="multiple" onchange="logicZona();">
                                                         <option value="0">TODOS</option>
                                                         <?php 
                                                            foreach ($zonas as $vcent) {
                                                               echo '<option value="'.$vcent->iIdCategoria.'" >'.$vcent->vNombre.'</option>';
                                                            }
                                                        ?>                                                       
                                                    </select>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6" id="divcentro" style="display:none">
                                                <div class="form-group" >
                                                    <label for="iTipo"> Centro Empresarial: </label>
                                                    <select name="iTipo" id="iTipo" style="width:100%;" class="js-example-basic-multiple" multiple="multiple" onchange="logicCentros()" >
                                                        <option value="0">TODOS</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!--<div class="col-md-5">
                                                <div class="form-group" id="centros-div">                                               
                                                    <select class="form-control" id="centro" onchange="carga_cuest(this.value);">
                                                        <option value="0">-- Centros --</option>
                                                        <?php 
                                                            if(count($centros) > 0) {
                                                                foreach ($centros as $vcent) {
                                                                    echo '<option value="'.$vcent->iIdCentro.'">'.$vcent->vNombre.'</option>';
                                                                }
                                                            }
                                                        ?>
                                                        
                                                    </select>
                                                </div>
                                            </div>-->

                                            <div class="col-md-4">
                                                <div class="form-group">                 
                                                <label for="cuest"> Cuestionario: </label>
                                                    <select class="form-control" class="js-example-basic-multiple" id="cuest" style="height:30px !important">
                                                        <option value="">-- Cuestionario --</option>
                                                    </select>
                                                </div>
                                            </div>                                            
                                            <div class="col-md-2 btn-group" id="btns" style="padding-top:25px;padding-bottom:25px">
                                                <button class="btn btn-info" onclick="filtrar();"><i class="fas fa-search"></i> Buscar</button>
                                               <button class="btn btn-success" onclick="exporting();" title="Exportar usuarios del cuestionario"><i class="fas fa-file-excel"></i> Exportar</button>
                                            </div>
                                            
                                            
                                        </div>
                                        <div class="row" style="display:none">
                                            <div class="col-md-3">

                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="check_todos" name="check_todos"  >
                                                        <label class="custom-control-label" for="check_todos">Todos</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="check_publico" name="check_publico" >
                                                        <label class="custom-control-label" for="check_publico">Publico (Los usuarios sin cuenta pueden verlo)</label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="check_nacional" name="check_nacional" >
                                                        <label class="custom-control-label" for="check_nacional">Nacional</label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="check_empresarial" name="check_empresarial">
                                                        <label class="custom-control-label" for="check_empresarial">Empresarial</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </section>

                                    <section id="cont_graficas"></section>

                                
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
     
             <footer class="footer text-center">
                Encuestas 2020 
            </footer>
     
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->


    <script src="<?=base_url();?>assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?=base_url();?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?=base_url();?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="<?=base_url();?>dist/js/app.min.js"></script>
    <script src="<?=base_url();?>dist/js/app.init.horizontal.js"></script>    
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?=base_url();?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="<?=base_url();?>dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?=base_url();?>dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?=base_url();?>dist/js/custom.js"></script>

    <script src="<?=base_url();?>dist/js/highcharts.js"></script>
    <script src="<?=base_url();?>dist/js/exporting.js"></script>
    <script src="<?=base_url();?>dist/js/export-data.js"></script>
    
    
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

    <script type="text/javascript"> 
    
        <?php
              $zonasSelectedOK = array();
              $centrosSelectedOK = array();
              $selectedType = array();
              
              if($cuest > 0) {
                  $i=0;
                  foreach ($zonasSelected as $vcent) {
                     $zonasSelectedOK[$i] = $vcent->iIdCategoria;
                     $i++;
                  }
                  
                  $j=0;
                  foreach ($centrosSelected as $vcent) {
                     $centrosSelectedOK[$j] = $vcent->iIdCentro;
                     $j++;
                  }
                  
                  
                  if($bEsNacional){
                      $selectedType[] = "1";
                  }
                  if($bEsEmpresarial){
                      $selectedType[]= "2";
                  }
                  if($bEsPublico){
                      $selectedType[]= "3";
                  }
              }
           ?>
        
        var zonasSelected =  <?php echo json_encode($zonasSelectedOK); ?>;
        var centrosSelected = <?php echo json_encode($centrosSelectedOK); ?>;
        var typesSelected = <?php echo json_encode($selectedType); ?>;
    
        $(document).ready(function() {
            setSelect();
            
            if(typesSelected.length > 0){
                $("#iTipoEncuesta").val(typesSelected);
                $('#iTipoEncuesta').trigger('change');
            }
            
            if(zonasSelected.length > 0){
                $("#iZona").val(zonasSelected);
                $('#iZona').trigger('change');
            }
            
            //carga_cuest();
        });
    
        function setSelect(){
             $('.js-example-basic-multiple').select2({
                placeholder: 'Selecciona 1 o varios...',
                multiple:true
            });
            
        }
    
        
        
        function otro() {
            if($('input:radio[name=check_otro]:checked').val() != 'empresarial')
            {
                carga_cuest();
                $('#centros-div').css('display','none');
            }
            else 
            {
                carga_cuest();
                $('#centros-div').css('display','block');
            }
        }

        function filtrar(){
            carga_resp();
            refresh_datos();
        }

        function carga_resp()    {

            $("#loader").show();
            var cuest = $('#cuest').val();
            var centros = $('#iTipo').val()
             var cp = $("#check_publico").is(":checked") ? 1 : 0;
            var cn = $("#check_nacional").is(":checked") ? 1 : 0;
            var ce = $("#check_empresarial").is(":checked") ? 1 : 0;
            
            if(cuest != '') {
                $('#cont_graficas').html('');
                $.post('<?=base_url();?>carga_resp', {cuestid: cuest, centro: centros, check_publico: cp, check_nacional: cn, check_empresarial: ce}, function(resp) {

                    var res = JSON.parse(resp); 
                    console.log(res);
                    res.forEach(preg => {
                        if(preg.tipo == 2){
							genera_table(preg);
						}else{
							genera_grafica(preg);
						}
                    });

                    $("#loader").hide();
                });
            }
            else alert('Debe seleccionar un cuestionario');

        }

        function refresh_datos() {
            
            var ct = $("#check_todos").is(":checked") ? 1 : 0;
            var cp = $("#check_publico").is(":checked") ? 1 : 0;
            var cn = $("#check_nacional").is(":checked") ? 1 : 0;
            var ce = $("#check_empresarial").is(":checked") ? 1 : 0;
            var zonas = $('#iZona').val();
            var centros = $('#iTipo').val()
            var fromCuestionarios = <?=$cuest?>;

            $.post('<?=base_url();?>C_sitio/datos_generales', {zonasarray:zonas, centrosarray:centros, check_todos: ct, check_publico: cp, check_nacional: cn, check_empresarial: ce, fromC: fromCuestionarios }, function(resp) {
                var res = JSON.parse(resp); 
                    console.log(res);

                    $('#totaluserssent').text(res.TotalUsersSent);
                    $('#totaluserstake').text(res.TotalUsersTake);
                    $('#totalusersnottake').text(res.TotalUsersNotTake);
                    $('#totalnacional').text(res.TotalNacional);
                    $('#totalempresarial').text(res.TotalEmpresarial);
                    $('#totalpublic').text(res.TotalPublicUsers);

            });
        }

        function carga_cuest() {
            
            var ct = $("#check_todos").is(":checked") ? 1 : 0;
            var cp = $("#check_publico").is(":checked") ? 1 : 0;
            var cn = $("#check_nacional").is(":checked") ? 1 : 0;
            var ce = $("#check_empresarial").is(":checked") ? 1 : 0;
            var zonas = $('#iZona').val();
            var centros = $('#iTipo').val()
            var fromCuestionarios = <?=$cuest?>;
            
            console.log(zonas);
            
            $.post('<?=base_url();?>C_cuestionario/carga_cuest', {zonasarray:zonas, centrosarray:centros, check_todos: ct, check_publico: cp, check_nacional: cn, check_empresarial: ce, fromC: fromCuestionarios }, function(resp) {
                $('#cuest').html(resp);
                
                 <?php
                   if($cuest > 0) {
                ?>
                     $("#cuest").val("<?=$cuest ?>");
                     $("#cuest").trigger("change");
                     carga_resp();
                     refresh_datos();
                <?php
                   }
                ?>
            });
        }
        
         function exporting() {

            $("#loader").show();
            
            var ct = $("#check_todos").is(":checked") ? 1 : 0;
            var cp = $("#check_publico").is(":checked") ? 1 : 0;
            var cn = $("#check_nacional").is(":checked") ? 1 : 0;
            var ce = $("#check_empresarial").is(":checked") ? 1 : 0;
            var zonas = $('#iZona').val();
            var centros = $('#iTipo').val()
            var fromCuestionarios = <?=$cuest?>;
            
            console.log(zonas);
            
            
            $.ajax({
               type:'POST',
               url:'<?=base_url();?>C_cuestionario/ExportExcel',
               data: {zonasarray:zonas, centrosarray:centros, check_todos: ct, check_publico: cp, check_nacional: cn, check_empresarial: ce, fromC: fromCuestionarios },
               dataType:'json'
            }).done(function(data){
                   var $a = $("<a>");
                   $a.attr("href",data.file);
                   $("body").append($a);
                   $a.attr("download","coparmex-usuarios-cuestionario.xls");
                   $a[0].click();
                   $a.remove();

                   $("#loader").hide();
            }).fail(function(data){
                console.log(data);
              alert("Error al exportar archivo. Intentar mas tarde. ");
            });

        }
		
		function genera_table(p){

            $('#cont_graficas').append('<hr/> <div id="container'+p.preg_id+'" style="min-width: 310px; height: 400px; margin: 0 auto"><center><label>'+ p.counter + '.-' + p.preg+' </label></center> <table id="tb'+p.preg_id+'" class="table table-bordered"><thead><tr><th>Respuestas</th></tr></thead><tbody></tbody></table></div>');

            datos = p.respuestas;
            console.log(datos);

            datos.forEach(op => {
                $('#tb'+p.preg_id+' > tbody:last-child').append('<tr><td>'+op.respuesta+'</td></tr>');
            });
        }

        function genera_grafica(p) {
                var cat = [];
                var result = [];
                var sub = '';
                var colors = ["#2345F3", "#1944A6", "#258EFA",  "#0F49AB", "#1AB7E5", "#0078D4","#75E1A5", "#5D8DD5", "#E81F1F","#EB6767",  "#E6DC3D", "#ED9E7F", , "#D85073", "#0D0909", "#0D0909", "#0D0909", "#0D0909", "#0D0909", "#0D0909", "#0D0909", "#0D0909", "#0D0909", "#0D0909", "#0D0909"],

                datos = p.op;
                
                var strHeight = '400px;'
                if(datos.length > 8){
                    strHeight = '800px;'
                }
                
                $('#cont_graficas').append('<div id="container'+p.preg_id+'" style="min-width: 310px; height: '+strHeight+'; margin: 0 auto"></div>');

                datos.forEach(op => {
                    cat.push(op.opcounter);
                    data = {
                       name: op.nomop,
                       color: colors[op.opcounter - 1],
                       y: Number(op.total)
                    }
                    result.push(data);
                    sub += op.opcounter+'.- '+op.nomop+': '+op.total+' <br>';
                });

                Highcharts.chart('container'+p.preg_id, {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: p.counter + '.-' + p.preg
                    },
                    subtitle: {
                        text: sub,
                        align: 'left'
                    },           
                    xAxis: {
                        categories: cat,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        allowDecimals: false,
                        title: {
                            text: 'Puntaje'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} Puntos</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        name: 'Resultado',
                        data: result

                    }]
                });
        }
        
        function cargarCentrosByZona(){
            
            $('#iTipo option').remove();
            $.post('<?=base_url();?>C_cuestionario/centrosbyzona', 'zonasarray='+$('#iZona').val(), function(resp){
                
                var data = $.parseJSON(resp);
                $('#iTipo').append('<option value="0">TODOS</option>');
                $.each(data, function(i, item) {
                     $('#iTipo').append('<option value="'+item.iIdCentro+'">'+item.vNombre+'</option>');
                });
            });
        }
        
        
        function logicTipo(){
            var tipos = $("#iTipoEncuesta").val();
            
            if(tipos.indexOf("2") != -1){
                $("#zonas").show();
                $("#check_empresarial"). prop("checked", true);
            }else{
                $("#zonas").hide();
                $("#check_empresarial"). prop("checked", false);
            }
            
            if(tipos.length == 3){
                $("#check_todos"). prop("checked", true);
            }else{
                $("#check_todos"). prop("checked", false);
            }
            
            if(tipos.indexOf("1") != -1){
                $("#check_nacional"). prop("checked", true);
            }else{
                $("#check_nacional"). prop("checked", false);
            }
            
            if(tipos.indexOf("3") != -1){
                $("#check_publico"). prop("checked", true);
            }else{
                $("#check_publico"). prop("checked", false);
            }
            
            carga_cuest();
        }
        
        function logicZona(){
            var tipos = $("#iZona").val();
            
            if(tipos.length > 0){
                cargarCentrosByZona();
                $("#divcentro").show();
            }
            else{
                $("#divcentro").hide();
            }
            
            if(tipos.indexOf("0") != -1 && tipos.length > 1){
                $("#iZona").val(["0"]);
                $("#iZona").trigger('change');
            }
            else{
                carga_cuest();
            }
        }
        
        function logicCentros(){
            var tipos = $("#iTipo").val();
            
            if(tipos.indexOf("0") != -1 && tipos.length > 1){
                $("#iTipo").val(["0"]);
                $("#iTipo").trigger('change');
            }
            else{
                carga_cuest();
            }
        }
    </script> 

</body>
</html>