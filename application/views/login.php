<!DOCTYPE html>
<html dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <title>Pulso Coparmex</title>
    <!-- Custom CSS -->
    <link href="<?=base_url();?>dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <div class="main-wrapper">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url(../../assets/images/big/auth-bg.jpg) no-repeat center center;">
            <div class="auth-box">
                <div>
                    <div class="logo">
                        <span class="db"><img src="<?=base_url();?>assets/images/logo_giz.png" alt="logo" /></span>
                        <!--<h5 class="font-medium m-b-20">Iniciar sesión</h5>-->
                    </div>
                    <!-- Form -->
                    <div class="row">
                        <div class="col-12">
                            <form class="form-horizontal m-t-20" id="form-login" onsubmit="login(event);">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class="ti-user"></i></span>
                                    </div>
                                    <input id="usuario" name="usuario" type="text" class="form-control form-control-lg" placeholder="Correo" aria-label="Usuario" aria-describedby="basic-addon1" autofocus="">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon2"><i class="ti-pencil"></i></span>
                                    </div>
                                    <input id="contrasenia" name="contrasenia" type="password" class="form-control form-control-lg" placeholder="Contraseña" aria-label="Contraseña" aria-describedby="basic-addon1">
                                </div> 
                                <div class="input-group mb-3">
                                    <div id="div_captcha"></div>
                                </div>                              
                                <div class="form-group text-center">

                                    <div class="col-xs-12 p-b-20">
                                        <button class="btn btn-block btn-lg btn-info" type="submit">Iniciar sesión</button>
                                    </div>


                                	<div class="col-xs-12 p-b-20">
                                        <a href="<?=base_url();?>C_sitio/registro" class="btn btn-block btn-lg btn-info" type="button" onclick="registro();">Registrarse</a>
                                    </div>


                                    <div class="col-xs-12 p-b-20">
                                        <a href="<?=base_url();?>cuestionarios-publicos" class="" >Ver Cuestionarios Públicos</a>
                                    </div>
                                    
                                </div>     
                                
                                <?php /*<div class="form-group text-center">
                                    <div class="col-xs-12 p-b-20" style="padding-left:15px">
                                        <label>Aviso de Privacidad: </label><a href="https://www.coparmex.org.mx/downloads/AvisoPrivacidad/AvisoPrivacidad_PulsoSemanalCPX.pdf">Leer</a><br />
                                	    <input type="checkbox" class="custom-control-input" id="terminos" name="terminos"  >
                                        <label class="custom-control-label" for="terminos">He leído, doy mi consentimiento y acepto las condiciones del Aviso de Privacidad.</label>
                                    </div>   
                                </div> */?>

                            </form>
                        </div>
                    </div>
                </div>
                <!--<div id="recoverform">
                    <div class="logo">
                        <span class="db"><img src="<?=base_url();?>assets/images/logo-icon.png" alt="logo" /></span>
                        <h5 class="font-medium m-b-20">Recover Password</h5>
                        <span>Enter your Email and instructions will be sent to you!</span>
                    </div>
                    <div class="row m-t-20">
                        
                        <form class="col-12" action="index.html">
                            
                            <div class="form-group row">
                                <div class="col-12">
                                    <input class="form-control form-control-lg" type="email" required="" placeholder="Username">
                                </div>
                            </div>
                            
                            <div class="row m-t-20">
                                <div class="col-12">
                                    <button class="btn btn-block btn-lg btn-danger" type="submit" name="action">Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>-->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right Sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->
    <script src="<?=base_url();?>assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?=base_url();?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?=base_url();?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="<?=base_url();?>assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="<?=base_url();?>assets/libs/sweetalert2/sweet-alert.init.js"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"async defer></script>
    <!-- ============================================================== -->
    <!-- This page plugin js -->
    <!-- ============================================================== -->
    <script>
    var div_captcha;
    var onloadCallback = function() {
        div_captcha = grecaptcha.render('div_captcha', {
          'sitekey' : '6Ld5O-MUAAAAAKkBBPxVTkoCfZreTOWlmmQV62ek'
        });
    };

    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();
    
    function login(e) {

        e.preventDefault();
        

        $.ajax({
            type: "POST",
            url: "<?=base_url();?>iniciar",
            data: $('#form-login').serialize(),
            success: function(resp) {
                switch(resp) {
                    case 'success':
                        Swal.fire({
                          type: 'success',
                          title: 'Correcto',
                          text: 'Bienvenido'                      
                        });
                        window.location.reload(true); 
                        break;
                    case 'error1':                     
                        Swal.fire({
                          type: 'error',
                          title: 'Error',
                          text: 'Contraseña incorrecta'
                        });
                        break;
                    case 'error2':                     
                        Swal.fire({
                          type: 'error',
                          title: 'Error',
                          text: 'El usuario no existe'
                        });
                        break;
                    case 'error3': 
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: 'Resulva el captcha para continuar'
                        });
                        break;
                    case 'error4': 
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: 'La cuenta no ha sido activada'
                        });
                        break;
                }
                grecaptcha.reset(div_captcha);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
               alerta(resp, 'error');
               grecaptcha.reset(div_captcha);
               //console.log(XMLHttpRequest);
            }
        });       
    }

    </script>
      <footer class="footer text-center">
                Encuestas 2020 
            </footer>
</body>

</html>