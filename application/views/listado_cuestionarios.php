<!DOCTYPE html>
<html dir="ltr" lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url();?>assets/images/favicon.png">
    <title>Pulso Coparmex</title>
    <!-- This page plugin CSS -->
    <link href="<?=base_url();?>assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?=base_url();?>dist/css/style.min.css" rel="stylesheet">
    <link href="<?=base_url();?>dist/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
</head>

<body <?=(count($vigencia) > 0) ? 'onload="carga_modal();"' : '' ?>>
    
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    
    <div id="main-wrapper">
        
        <?php include('nav-bar.php'); ?>

        
        <!-- ============================================================== -->
        <div class="page-wrapper">
            
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Listado de cuestionarios</h4>
                    </div>                    
                </div>
            </div>
            
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                        <div class="card-body" id="contenido">

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <a class="btn btn-primary" href="javascript:" title="Editar" onclick="capturarCuestionario(0);"><i class="fas fa-plus"></i></>&nbsp;Nuevo cuestionario</a>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button data-toggle="modal" data-target="#modalNotificacion" class="btn btn-primary" title="Editar">Notificar</button>
                                </div>


                            </div>
                            <br>

                            <?=$tabla?>
                        </div>
                        </div>
                    </div>
                </div>
                    
        </div>

        <?php 
            if(count($vigencia) > 0)
            {
                ?>
             <div id="modalNotificacion" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive">                                 
                                <table id="zero_config" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <!--<th>ID</th> -->
                                            <th>Nombre del cuestionario dd</th>
                                            <th>Centro Específico</th>
                                            <th>Fecha Inicio</th>
                                            <th>Fecha Fin</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        //print_r($vigencia);
                                        foreach ($vigencia as $vig) {
                                            echo '<tr>'; 
                                            //echo '<td>'.$vig->iIdCuestionario.'</td>'; 
                                            echo '<td>'.$vig->vCuestionario.'</td>';
                                            echo '<td></td>';
                                            echo '<td>'.$vig->dVigencia.'</td>';
                                            echo '<td>'.$vig->vNombre.'</td>';
                                            echo '<td><button id="btn'.$vig->iIdCuestionario.'" class="btn btn-primary" title="Editar" onclick="enviaRecordatorio('.$vig->iIdCuestionario.');">Notificar</button></td>';
                                            echo '</tr>';
                                        }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        <?php
            }
        ?>
        
        <div id="modalConfirmaNotificacion" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12">
                                <p class="col-md-12">
                                    <label>Enviar correo a: </label>
                                </p>
                                <div class="row">
                                    <div class="col-md-6">
    
                                                    <div class="form-group">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="check_resp" name="check_resp"  >
                                                            <label class="custom-control-label" for="check_resp">Los que respondieron</label>
                                                        </div>
                                                    </div>
                                    </div>
                                    <div class="col-md-6">
    
                                                    <div class="form-group">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="check_noresp" name="check_noresp" >
                                                            <label class="custom-control-label" for="check_noresp">Los que no han respondido</label>
                                                        </div>
                                                    </div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-md-10">
                                    <div id="editor">
                                        Le recordamos que la fecha l&iacute;mite del cuestionario {{nombre_cuestionario}} est&aacute; pr&oacute;xima, por lo cual no podr&aacute; acceder a responderla posteriormente. Fecha L&iacute;mite: {{fecha_limite}}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    Usa los TAGS siguientes para posicionar ciertos datos
                                    <small style="color:blue">{{nombre_cuestionario}}<br />
                                    {{fecha_limite}}
                                    </small>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" id="cuesthiddenid"  />
                            <button type="button" id="btnrecordatorio" class="btn btn-info waves-effect" onclick="enviaRecordatorio();">Enviar Mensaje</button>
                            <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
                                
            </div>
            
            <footer class="footer text-center">
                Encuestas 2020 
            </footer>
            
        </div>
        
    </div>
       
    <div class="chat-windows"></div>
    


    <script src="<?=base_url();?>assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?=base_url();?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?=base_url();?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="<?=base_url();?>dist/js/app.min.js"></script>
    <script src="<?=base_url();?>dist/js/app.init.horizontal.js"></script>
    <!--<script src="<?=base_url();?>dist/js/app-style-switcher.horizontal.js"></script>-->
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?=base_url();?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="<?=base_url();?>dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?=base_url();?>dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?=base_url();?>dist/js/custom.js"></script>

    
    <!--This page plugins -->
    <script src="<?=base_url();?>assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="<?=base_url();?>dist/js/pages/datatable/datatable-basic.init.js"></script>

    <script src="<?=base_url();?>assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>    
    <script src="<?=base_url();?>assets/libs/jquery-validation/dist/additional-methods.js"></script>  

    <script src="<?=base_url();?>dist/js/fileinput.js" type="text/javascript"></script>
    <script src="<?=base_url();?>dist/js/es.js" type="text/javascript"></script>

    <script src="<?=base_url();?>assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="<?=base_url();?>assets/libs/sweetalert2/sweet-alert.init.js"></script>
    
    <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>

<script>
           CKEDITOR.replace( 'editor' );
                </script>
    <script type="text/javascript">
    
    
        function capturarCuestionario(id){
            window.location.href = '<?=base_url()?>capturar_cuest?cuestid='+id;
        }
        
        function goToResponder(id){
            window.location.href = '<?=base_url()?>responder?cuestid='+id;
        }
        
        function goToDashboard(id){
            window.location.href = '<?=base_url()?>dashboard?cuestid='+id;
        }

        function carga_modal() {
            //$('#modalNotificacion').modal('show');
        }
        
        function enviaRecordatorio() {
            var cuestid = $("#cuesthiddenid").val();
            var message = CKEDITOR.instances['editor'].getData();
            var resp = $("#check_resp").is(":checked") ? 1 : 0;
            var noresp = $("#check_noresp").is(":checked") ? 1 : 0;
            
            
            //$('.preloader').show();
            
            if(resp == 0 && noresp == 0){
                Swal.fire({
                              type: 'error',
                              title: 'No Enviado',
                              text: 'Es necesario seleccionar al menos una opcion para el enviar el correo.'
                            });
            }else{
                $('#btnrecordatorio').attr('disabled', 'disabled');
            $('#btnrecordatorio').text("Enviando...");
            
                $.post('<?=base_url();?>C_cuestionario/notificar', {cuestid: cuestid, message: message, resp: resp, noresp: noresp }, function(resp) {
                    let env = JSON.parse(resp);
                    if(env.error == 0){
                        Swal.fire({
                              type: 'success',
                              title: 'Enviado',
                              text: 'Recordatorios enviados.'
                            });
                            
                        $('#modalConfirmaNotificacion').modal('hide');
                        
                        $('#btnrecordatorio').removeAttr('disabled');
                        $('#btnrecordatorio').text("Enviar Mensaje");
                    }
                    else
                        Swal.fire({
                              type: 'error',
                              title: 'No Enviado',
                              text: 'Error al enviar recordatorios, intente mas tarde.'
                            });
                            
                    
                    //$('.preloader').hide();
                });
            }
        }

        function eliminarCuestionario(id){
            //var arch = $('#archivo').val();
            if(confirm('¿Realmente desea eliminar este cuestionario?')){
                $.post('<?=base_url();?>C_cuestionario/eliminar_cuestionario', 'id='+id, function(resp){
                    if(resp == 1){
                        location.reload();
                    } else {
                        Swal.fire({
                          type: 'error',
                          title: 'Error',
                          text: resp
                        });
                    }
                });
            }
        }
        
        function cargarPersonasPorFiltro(id){

            $('#btnrecordatorio').removeAttr("disabled");

             $('#usuarios-respondieron').html("");
             $('#usuarios-norespondieron').html("");
             
            $.post('<?=base_url();?>C_cuestionario/cargarusuariosemail', 'id='+id, function(resp){
                
                var data = $.parseJSON(resp);
                
                $("#cuesthiddenid").val(id);
                //open modal;
                $("#modalConfirmaNotificacion").modal("show");
 
            });
        }

        function duplicar(id){
            $('.preloader').show();
            $.post('<?=base_url();?>C_cuestionario/duplicar_cuestionario', 'cuestId='+id, function(resp){
                //$('.preloader').hide();
                location.reload();
            });
        }

        function pegar_pregunta(id){
            //idcuestionario
            var idcuestionario = id;
            //idpregunta
            var idpregunta = localStorage.getItem('idPregunta');

            if(idpregunta == undefined || idpregunta == null || idpregunta === ""){
                Swal.fire({
                          type: 'error',
                          title: 'Sin Pregunta',
                          text: 'No hay ninguna pregunta copiada para pegar.'
                        });
                return false;
            }

            console.log(idpregunta);

            $('.preloader').show();
            $.post('<?=base_url();?>C_cuestionario/pegar_pregunta', 'cuestId='+idcuestionario+'&pregId='+idpregunta, function(resp){
                $('.preloader').hide();
                localStorage.removeItem("idPregunta");
                //location.reload();
                Swal.fire({
                          type: 'success',
                          title: 'Pregunta Almacenada!!!',
                          text: 'La pregunta fue exitosamente copiada en el cuestionario seleccionado.'
                        });
            });
        }

    </script>
</body>

</html>